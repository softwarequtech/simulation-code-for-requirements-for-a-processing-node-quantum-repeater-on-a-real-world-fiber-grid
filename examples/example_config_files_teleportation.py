"""Example of elementary link with asymmetry using abstract nodes as end nodes."""
from simulations.unified_simulation_script_state import run_unified_simulation_state
from netsquid_simulationtools.repchain_data_process import process_repchain_dataframe_holder, process_data_duration, \
    process_data_teleportation_fidelity
import os

example_configs = [
    ("abstract_config.yaml", "abstract_sim_params.yaml"),
    ("abstract_config_vary_t2.yaml", "abstract_sim_params.yaml"),
    ("abstract_config_asym.yaml", "abstract_sim_params.yaml"),
    ("ti_config.yaml", "ti_sim_params.yaml"),
    ("ti_config_vary_coherence.yaml", "ti_sim_params.yaml"),
    ("nv_config.yaml", "nv_sim_params.yaml"),
    ("nv_config_vary_t2.yaml", "nv_sim_params.yaml"),
]


def run_config_teleportation(configfile, paramfile=None, suppress_output=False):
    """Run entanglement-distribution experiment and estimate achievable quantum-teleportation fidelity.

    The simulation performs entanglement distribution and collects the end-to-end state at the time when
    both nodes have confirmed that they hold an end-to-end entangled state.
    This state is used in post-processing to determine the fidelity and rate of quantum teleportation if the produced
    states would be used for teleportation, and teleportation is performed in a fully noiseless way.

    Parameters
    ----------
    configfile : str
        Name of configuration file.
        Must be relative to the folder that holds this python file.
    paramfile : str or None (optional
        Name of parameter file.
        Must be relative to the folder that holds this python file.
    suppress_output : bool (optional)
        If True, print statements are suppressed.

    """
    dir = os.path.dirname(os.path.realpath(__file__)) + "/"
    configfile = dir + configfile
    paramfile = dir + paramfile
    repchain_dataframe, varied_param = run_unified_simulation_state(configfile=configfile, paramfile=paramfile,
                                                                    n_runs=10, suppress_output=suppress_output)
    if varied_param is not None:
        assert varied_param in repchain_dataframe.dataframe.columns
    processed_data = process_repchain_dataframe_holder(repchain_dataframe_holder=repchain_dataframe,
                                                       processing_functions=[process_data_duration,
                                                                             process_data_teleportation_fidelity])
    if not suppress_output:
        print(f"\n\nSimulated for \nconfigfile = {configfile}, \nparamfile = {paramfile}.")
        print(f"\nTeleportation fidelities: \n"
              f"{processed_data['teleportation_fidelity_average_optimized_local_unitaries']}.")
        print(f"\nEntanglement-generation duration (in {processed_data['generation_duration_unit'][0]}): \n"
              f"{processed_data['duration_per_success']}  per success.\n\n")


def main(suppress_output=False):
    for (configfile, paramfile) in example_configs:
        run_config_teleportation(configfile=configfile, paramfile=paramfile, suppress_output=suppress_output)


if __name__ == "__main__":
    main(suppress_output=False)
