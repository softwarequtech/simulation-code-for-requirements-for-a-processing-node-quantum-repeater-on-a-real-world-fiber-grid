from netsquid.protocols.protocol import Protocol
import netsquid as ns
from netsquid.qubits.ketstates import BellIndex, bell_states
from netsquid_magic.magic_distributor import DoubleClickMagicDistributor
from netsquid_physlayer.heralded_connection import MiddleHeraldedConnection, HeraldedConnection
from netsquid_physlayer.classical_connection import ClassicalConnectionWithLength
from qlink_interface import ReqCreateAndKeep, ResCreateAndKeep, ReqMeasureDirectly, ResMeasureDirectly, \
    MeasurementBasis
import netsquid.qubits.qubitapi as qapi

from netsquid_netconf.netconf import netconf_generator
from netsquid_netconf.builder import ComponentBuilder
from nlblueprint.processing_nodes.nodes_with_drivers import AbstractNodeWithDriver, TINodeWithDriver, NVNodeWithDriver
from nlblueprint.control_layer.swapasap_egp import SwapAsapEndNodeLinkLayerProtocol
from netsquid_driver.EGP import EGPService
import random

ns.set_qstate_formalism(ns.QFormalism.DM)


class LinkLayerDemo(Protocol):
    """Base protocol for demonstrating entanglement generation by two nodes using link-layer protocols.

    Parameters
    ----------
    nodes : list of two :class:`netsquid.nodes.node.Node`s.
        Nodes that run the link layer protocols.
    suppress_output : bool
        Set to True to suppress print statements.

    """

    def __init__(self, nodes, suppress_output=False):
        super().__init__(name="link_layer_demo_protocol")
        assert len(nodes) == 2
        self.EGPs = [node.driver[EGPService] for node in nodes]
        self.downstream_node = None
        self.upstream_node = None
        self.suppress_output = suppress_output

    def start(self):
        # determine upstream node randomly
        nodes = [egp.node for egp in self.EGPs]
        self.upstream_node = random.choice(nodes)
        [self.downstream_node] = [node for node in nodes if node != self.upstream_node]

        # send receive request for downstream node so it will be available
        # TODO uncomment two lines below if ReqReceive is implemented in SwapASAPEGP
        #  currently the request triggers an error, the EGP is always available

        # receive_req = ReqReceive(remote_node_id=self.upstream_node.ID, purpose_id=0)
        # self.downstream_node.driver[EGPService].put(receive_req)

        super().start()


class SingleCreateAndKeep(LinkLayerDemo):
    """Send a single create-and-keep request.

    This protocol collects the created qubits and calculates the fidelity of their quantum state to the expected
    Bell state (i.e. the one that is indicated in the response of the link layer).
    Both the expected Bell state and the fidelity are printed.

    Parameters
    ----------
    nodes : list of two :class:`netsquid.nodes.node.Node`s.
        Nodes that run the link layer protocols.
    suppress_output : bool
        Set to True to suppress print statements.

    """

    def __init__(self, nodes, suppress_output):
        super().__init__(nodes, suppress_output)
        self.fidelity = None

    def run(self):

        create_and_keep_req = ReqCreateAndKeep(remote_node_id=self.downstream_node.ID, number=1)
        self.upstream_node.driver[EGPService].put(create_and_keep_req)

        evts = [self.await_signal(sender=egp, signal_label=ResCreateAndKeep.__name__) for egp in self.EGPs]
        yield evts[0] & evts[1]
        res = [egp.get_signal_result(label=ResCreateAndKeep.__name__) for egp in self.EGPs]
        assert res[0].bell_state == res[1].bell_state  # check that EGPs agree on created Bell state
        bell_index = BellIndex(res[0].bell_state)
        bell_state = bell_states[bell_index]
        qubits = [egp.node.qmemory.peek(positions=res.logical_qubit_id)[0] for egp, res in zip(self.EGPs, res)]
        self.fidelity = qapi.fidelity(qubits=qubits, reference_state=bell_state)
        if not self.suppress_output:
            print(f"\n\nExpected Bell state: {bell_index} \n\nFidelity: {self.fidelity}\n\n")


class SingleMeasureDirectly(LinkLayerDemo):
    """Send a single measure-directly request.

    The measurements are (currently) both always performed in the Z basis.
    The outcomes, measurement bases and expected Bell state are printed.

    Parameters
    ----------
    nodes : list of two :class:`netsquid.nodes.node.Node`s.
        Nodes that run the link layer protocols.
    suppress_output : bool
        Set to True to suppress print statements.

    """

    def __init__(self, nodes, suppress_output):
        super().__init__(nodes, suppress_output)
        self.error = None

    def run(self):

        measure_req = ReqMeasureDirectly(remote_node_id=self.downstream_node.ID, number=1)
        self.upstream_node.driver[EGPService].put(measure_req)

        evts = [self.await_signal(sender=egp, signal_label=ResMeasureDirectly.__name__) for egp in self.EGPs]
        yield evts[0] & evts[1]
        res = [egp.get_signal_result(label=ResMeasureDirectly.__name__) for egp in self.EGPs]
        assert res[0].bell_state == res[1].bell_state  # check that EGPs agree on created Bell state
        bell_index = BellIndex(res[0].bell_state)

        assert res[0].measurement_basis == MeasurementBasis.Z
        assert res[1].measurement_basis == MeasurementBasis.Z

        same_outcomes = res[0].measurement_outcome == res[1].measurement_outcome
        if bell_index in [BellIndex.PHI_PLUS, BellIndex.PHI_MINUS]:
            self.error = False if same_outcomes else True
        else:
            self.error = True if same_outcomes else False

        if not self.suppress_output:
            print(f"\n\nBell index: {bell_index}"
                  f"\n\nMeasurement bases: {res[0].measurement_basis} and {res[1].measurement_basis}"
                  f"\n\nMeasurement outcomes: {res[0].measurement_outcome} and {res[1].measurement_outcome}.")


def implement_magic(network):
    """Add magic distributors to a physical network.

    To every connection in the network, a magic distributor is added as attribute.
    Currently, this is always a :class:`netsquid_magic.magic_distributor.DoubleClickMagicDistributor`.
    The magic distributor is given the connection as argument upon construction (so that parameters to be used in magic
    can be read from the connection).

    Parameters
    ----------
    network : :class:`netsquid.nodes.network.Network`
        Network that the network distributors should be added to.

    """

    for connection in network.connections.values():
        if isinstance(connection, HeraldedConnection):
            nodes = [port.connected_port.component for port in connection.ports.values()]
            magic_distributor = DoubleClickMagicDistributor(nodes, connection)
            connection.magic_distributor = magic_distributor


def setup_protocols(network):
    """Setup any protocols that are not set up using netconf by :func:`setup_network`. Also starts all services.

    Most protocols should already be set in the driver of the nodes in the network.
    It is here assumed that the only protocols that are not yet set up, are the link-layer protocols
    (since these need global information about the network when constructed).
    All services known to the driver of each node in the network are started afterwards.

    Parameters
    ----------
    network : :class:`netsquid.nodes.network.Network`
        Network on which protocols should be set up.

    """

    # repeaters have 4 ports: two communication ports, two entanglement ports
    repeaters = [node for node in network.nodes.values() if len(node.ports) == 4]

    # end nodes have 2 ports: one communication port, one entanglement port
    end_nodes = [node for node in network.nodes.values() if len(node.ports) == 2]

    # check that there are only 2 end nodes
    assert len(end_nodes) == 2

    # check that each node is either a repeater node or an end node
    assert len(repeaters) + len(end_nodes) == len(network.nodes)

    # start all protocols on repeaters
    for repeater in repeaters:
        repeater.driver.start_all_services()

    # add link-layer protocol to end nodes and start all protocols
    # TODO perhaps better to define link-layer implementation in the preconfigured node (but needs num repeaters...)
    for end_node in end_nodes:
        end_node.driver.add_service(EGPService, SwapAsapEndNodeLinkLayerProtocol(node=end_node))
        end_node.driver.start_all_services()


def setup_network():
    """Set up network according to configuration file.

    The configuration file "example_swap_asap_link_layer.yaml" is used by `netsquid_netconf` to build a network.
    The functions :func:`implement_magic` and :func:`setup_protocols` are used to further set up the network.

    """

    # add required components to ComponentBuilder
    ComponentBuilder.add_type(name="abstract_node", new_type=AbstractNodeWithDriver)
    ComponentBuilder.add_type(name="nv", new_type=NVNodeWithDriver)
    ComponentBuilder.add_type(name="ti", new_type=TINodeWithDriver)
    ComponentBuilder.add_type(name="mid_heralded_connection", new_type=MiddleHeraldedConnection)
    ComponentBuilder.add_type(name="classical_connection", new_type=ClassicalConnectionWithLength)

    # obtain the network from config file using netconf
    generator = netconf_generator("examples/example_swap_asap_link_layer.yaml")
    objects, config = next(generator)
    network = objects["network"]

    implement_magic(network)
    setup_protocols(network)

    return network


def single_create_and_keep(suppress_output=False):
    """Send a single create-and-keep request to link layer and print expected Bell state, bases and outcomes."""

    ns.sim_reset()
    network = setup_network()
    end_nodes = [node for node in network.nodes.values() if len(node.ports) == 2]
    single_create_and_keep = SingleCreateAndKeep(nodes=end_nodes, suppress_output=suppress_output)
    single_create_and_keep.start()
    ns.sim_run()
    return single_create_and_keep.fidelity


def single_measure_directly(suppress_output=False):
    """Send a single measure-directly request to link layer and print fidelity to expected Bell state."""

    ns.sim_reset()
    network = setup_network()
    end_nodes = [node for node in network.nodes.values() if len(node.ports) == 2]
    single_measure_directly = SingleMeasureDirectly(nodes=end_nodes, suppress_output=suppress_output)
    single_measure_directly.start()
    ns.sim_run()
    return single_measure_directly.error


def main(suppress_output=False):

    if not suppress_output:
        print("\n\nPerforming Create and Keep\n\n")
    fidelities = []
    for _ in range(10):
        fidelity = single_create_and_keep(suppress_output=suppress_output)
        assert 0.5 < fidelity < 1.
        fidelities.append(fidelity)
    if not suppress_output:
        avg_fid = sum(fidelities) / len(fidelities)
        print(f"\n\nAverage fidelity: {avg_fid}\n\n")

    if not suppress_output:
        print("\n\nPerforming Measure Directly\n\n")
    errors = []
    for _ in range(19):
        error = single_measure_directly(suppress_output=suppress_output)
        errors.append(error)
    qber = sum(errors) / len(errors)
    if not suppress_output:
        print(f"\n\nQuantum Bit Error Rate (QBER): {qber}\n\n")
    assert 0 <= qber < 0.5


if __name__ == "__main__":
    main()
