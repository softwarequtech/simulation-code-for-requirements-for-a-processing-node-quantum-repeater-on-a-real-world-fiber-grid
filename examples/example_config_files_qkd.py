"""Example of elementary link with asymmetry using abstract nodes as end nodes."""
from simulations.unified_simulation_script_qkd import run_unified_simulation_qkd
from netsquid_simulationtools.repchain_data_process import process_repchain_dataframe_holder, process_data_duration, \
    process_data_bb84
import os

example_configs = [
    ("abstract_config.yaml", "abstract_sim_params.yaml"),
    ("abstract_config_vary_t2.yaml", "abstract_sim_params.yaml"),
    ("abstract_config_asym.yaml", "abstract_sim_params.yaml"),
    ("ti_config.yaml", "ti_sim_params.yaml"),
    ("ti_config_vary_coherence.yaml", "ti_sim_params.yaml"),
    ("nv_config.yaml", "nv_sim_params.yaml"),
    ("nv_config_vary_t2.yaml", "nv_sim_params.yaml"),
]


def run_config_qkd(configfile, paramfile=None, suppress_output=False):
    """Run QKD-like simulation for single configuration file as example.

    The simulation estimates quantum-bit error rate in different measurement bases,
    and entanglement-distribution duration.
    This is then used to estimate the achievable secret-key rate if BB84 would be performed.

    Parameters
    ----------
    configfile : str
        Name of configuration file.
        Must be relative to the folder that holds this python file.
    paramfile : str or None (optional
        Name of parameter file.
        Must be relative to the folder that holds this python file.
    suppress_output : bool (optional)
        If True, print statements are suppressed.

    """
    dir = os.path.dirname(os.path.realpath(__file__)) + "/"
    configfile = dir + configfile
    paramfile = dir + paramfile
    repchain_dataframe, varied_param = run_unified_simulation_qkd(configfile=configfile, paramfile=paramfile,
                                                                  n_runs=10, suppress_output=suppress_output)
    if varied_param is not None:
        assert varied_param in repchain_dataframe.dataframe.columns
    processed_data = process_repchain_dataframe_holder(repchain_dataframe_holder=repchain_dataframe,
                                                       processing_functions=[process_data_duration,
                                                                             process_data_bb84])
    if not suppress_output:
        print(f"\n\nSimulated for \nconfigfile = {configfile}, \nparamfile = {paramfile}.")
        print(f"\nBB84 secret-key rate (secret bits per {processed_data['generation_duration_unit'][0]}):\n "
              f"{processed_data['sk_rate']}\n\n")


def main(suppress_output=False):
    for (configfile, paramfile) in example_configs:
        run_config_qkd(configfile=configfile, paramfile=paramfile, suppress_output=suppress_output)


if __name__ == "__main__":
    main(suppress_output=False)
