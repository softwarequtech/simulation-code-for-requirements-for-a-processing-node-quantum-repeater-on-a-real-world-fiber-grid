"""
This script is used to plot data produced with unified_simulation_script.py.
"""

from unified_simulation_script_state import plot_data
from argparse import ArgumentParser
import pickle


parser = ArgumentParser()
parser.add_argument("datafile", type=str, help="Name of the data file.")
args = parser.parse_args()
with open(args.datafile, "rb") as datafile:
    repchain_dataframe_holder = pickle.load(datafile)
    plot_data(repchain_dataframe_holder)
