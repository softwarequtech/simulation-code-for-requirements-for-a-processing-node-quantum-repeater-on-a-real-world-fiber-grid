import os
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import yaml
from math import pi
from netsquid_netconf.netconf import Loader
from argparse import ArgumentParser
from netsquid_nv.nv_parameter_set import compute_dephasing_prob_from_nodephasing_number, _gaussian_dephasing_fn

font = {'size': 12}

matplotlib.rc('font', **font)

NAME2DISPLAY = {"detector_efficiency": "detection probability \n (excluding transmission)",
                "electron_T2": r"electron T$_2$",
                "carbon_T2": r"carbon $T_2$",
                "ec_gate_depolar_prob": "two-qubit \n gate noise",
                "visibility": "visibility",
                "n1e": "induced \n storage qubit \n noise",
                "p_double_exc": "probability \n double excitation",
                "std_electron_electron_phase_drift": "phase drift",
                "collection_efficiency": "detection probability \n (excluding transmission)",
                "coherence_time": "coherence time",
                "emission_fidelity": "emission fidelity",
                "improvement_factor_visibility": "visibility",
                "improvement_factor_coin_prob_ph_ph": "coincidence probability"}

TO_PROB_NO_ERROR_FUNCTION = {"detector_efficiency": lambda x: x,
                             "collection_efficiency": lambda x: x,
                             "p_double_exc": lambda x: 1 - x,
                             "ec_gate_depolar_prob": lambda x: 1 - x,
                             "n1e": lambda x: 1 - compute_dephasing_prob_from_nodephasing_number(x),
                             "electron_T1": lambda x: np.exp(-1 / x),
                             "electron_T2": lambda x: np.exp(-1 / x),
                             "carbon_T1": lambda x: np.exp(-1 / x),
                             "carbon_T2": lambda x: np.exp(-1 / x),
                             "std_electron_electron_phase_drift": lambda x: _gaussian_dephasing_fn(x),
                             "T1": lambda x: np.exp(-1 / x),
                             "T2": lambda x: np.exp(-1 / x),
                             "coherence_time": lambda x: np.exp(-1 * (1 / x)**2),
                             "emission_fidelity": lambda x: x,
                             "swap_quality": lambda x: x,
                             "visibility": lambda x: x,
                             "dark_count_probability": lambda x: 1 - x,
                             }


def plot_circular(improvement_dictionary, max_radius=None, baseline_name="baseline",
                  solution_name="improved", path="circular_plot", log=False):
    angles = [n / float(len(improvement_dictionary)) * 2 * pi for n in range(len(improvement_dictionary))]
    r = list(improvement_dictionary.values())

    ax = plt.subplot(111, polar=True)
    ax.set_axisbelow(False)
    ax.plot(angles + angles[0:], r + r[0:], linewidth=2.5)

    if max_radius is not None:
        ax.set_rmax(max_radius)
        if max_radius > 10:
            ticks = [round(tick / 5) * 5 for tick in np.arange(0, max_radius, max_radius / 4)]
            ax.set_rticks(ticks)
        else:
            ax.set_rticks(np.arange(1, max_radius, 2))
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)
    plt.xticks(angles)
    angle_labels = [NAME2DISPLAY[param_name] for param_name in improvement_dictionary.keys()]
    ax.set_xticklabels(angle_labels)
    ax.xaxis.set_tick_params(pad=25)
    ax.set_rlabel_position(0)
    ax.grid(True)
    if log:
        ax.set_rscale('symlog')
    ax.text(0.5, ax.get_rmax(), 'Imp. factor',
            rotation=0, ha='center', va='center', size=12)

    # Save
    filename = 'circular_plot_baseline_{}_improved_{}'.format(baseline_name, solution_name)
    my_path = os.path.abspath(".")
    path_to_save = os.path.join(my_path, path)
    if not os.path.exists(path_to_save):
        os.mkdir(path_to_save)
    # plt.show()
    plt.savefig(os.path.join(path_to_save, filename + '.pdf'), dpi=300, bbox_inches='tight')
    plt.savefig(os.path.join(path_to_save, filename + '.png'), dpi=300, bbox_inches='tight')


def compute_improvement_factor_dict(baseline_dict, solution_dict):
    improvement_factor_dict = {}

    for parameter, value in baseline_dict.items():
        # Some TI parameters are passed as improvement factors, hence they can just directly be added to the cost
        if "improvement" in parameter:
            improvement_factor_dict[parameter] = solution_dict[parameter]
            continue
        baseline_prob_no_error = TO_PROB_NO_ERROR_FUNCTION[parameter](value)
        prob_no_error = TO_PROB_NO_ERROR_FUNCTION[parameter](solution_dict[parameter])

        improvement_factor_dict[parameter] = 1 / (np.log(prob_no_error) / np.log(baseline_prob_no_error))
    return improvement_factor_dict


def read_param_values(param_names, parameter_file):
    with open(parameter_file, "r") as stream:
        parameter_dict = yaml.load(stream, Loader=Loader)

    return {param_name: parameter_dict[param_name] for param_name in param_names}


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--parameters", nargs="+", default=None, required=True,
                        help="Parameters to include in circular plot.")
    parser.add_argument("--baseline_param_file", type=str, required=True, help="Name of baseline parameter file.")
    parser.add_argument("--solution_param_file", type=str, required=True, help="Name of 1st solution parameter file.")
    parser.add_argument("--output_path", type=str, required=False, default="circular_plot", help="Path to save plot.")
    parser.add_argument("--log", type=str, required=False, default=False, help="Whether or not to use log scale.")

    args, unknown = parser.parse_known_args()

    baseline_param_dict = read_param_values(args.parameters, args.baseline_param_file)
    solution_param_dict = read_param_values(args.parameters, args.solution_param_file)

    imp_factor_dict = compute_improvement_factor_dict(baseline_param_dict, solution_param_dict)

    max_radius = max(imp_factor_dict.values())
    plot_circular(improvement_dictionary=imp_factor_dict,
                  max_radius=max_radius,
                  baseline_name=args.baseline_param_file.split(".yaml")[0],
                  solution_name=args.solution_param_file.split(".yaml")[0],
                  path=args.output_path,
                  log=args.log)
