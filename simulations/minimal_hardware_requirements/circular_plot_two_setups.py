import os
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import yaml
from math import pi
from netsquid_netconf.netconf import Loader
from argparse import ArgumentParser
from netsquid_nv.nv_parameter_set import compute_dephasing_prob_from_nodephasing_number, _gaussian_dephasing_fn

font = {'size': 12}

matplotlib.rc('font', **font)

NAME2DISPLAY = {"detector_efficiency": "detection probability \n (excluding transmission)",
                "electron_T2": r"electron T$_2$",
                "carbon_T2": r"carbon $T_2$",
                "ec_gate_depolar_prob": "two-qubit \n gate noise",
                "visibility": "visibility",
                "n1e": "induced \n storage qubit \n noise",
                "p_double_exc": "probability \n double excitation",
                "std_electron_electron_phase_drift": "phase drift"}

TO_PROB_NO_ERROR_FUNCTION = {"detector_efficiency": lambda x: x,
                             "collection_efficiency": lambda x: x,
                             "p_double_exc": lambda x: 1 - x,
                             "ec_gate_depolar_prob": lambda x: 1 - x,
                             "n1e": lambda x: 1 - compute_dephasing_prob_from_nodephasing_number(x),
                             "electron_T1": lambda x: np.exp(-1 / x),
                             "electron_T2": lambda x: np.exp(-1 / x),
                             "carbon_T1": lambda x: np.exp(-1 / x),
                             "carbon_T2": lambda x: np.exp(-1 / x),
                             "std_electron_electron_phase_drift": lambda x: _gaussian_dephasing_fn(x),
                             "T1": lambda x: np.exp(-1 / x),
                             "T2": lambda x: np.exp(-1 / x),
                             "coherence_time": lambda x: np.exp(-1 * (1 / x)**2),
                             "emission_fidelity": lambda x: x,
                             "swap_quality": lambda x: x,
                             "visibility": lambda x: x,
                             "dark_count_probability": lambda x: 1 - x,
                             }


def plot_circular(improvement_dictionary_1, improvement_dictionary_2, max_radius=None, baseline_name="baseline",
                  solution_name_1="improved_1", solution_name_2="improved_2", label_1=None, label_2=None,
                  path="circular_plot", log=False):
    angles_1 = [n / float(len(improvement_dictionary_1)) * 2 * pi for n in range(len(improvement_dictionary_1))]
    r_1 = list(improvement_dictionary_1.values())
    angles_2 = [n / float(len(improvement_dictionary_2)) * 2 * pi for n in range(len(improvement_dictionary_2))]
    r_2 = list(improvement_dictionary_2.values())

    ax = plt.subplot(111, polar=True)
    ax.set_axisbelow(False)
    if label_1 is None:
        label_1 = solution_name_1
    if label_2 is None:
        label_2 = solution_name_2
    ax.plot(angles_1 + angles_1[0:], r_1 + r_1[0:], linewidth=2.5, label=label_1)
    ax.plot(angles_2 + angles_2[0:], r_2 + r_2[0:], linewidth=2.5, label=label_2)

    if max_radius is not None:
        ax.set_rmax(max_radius)
        if max_radius > 10:
            ticks = [round(tick / 5) * 5 for tick in np.arange(0, max_radius, max_radius / 4)]
            ax.set_rticks(ticks)
        else:
            ax.set_rticks(np.arange(1, max_radius, 2))
    ax.set_theta_offset(pi / 2)
    ax.set_theta_direction(-1)
    plt.xticks(angles_1)
    angle_labels = [NAME2DISPLAY[param_name] for param_name in improvement_dictionary_1.keys()]
    ax.set_xticklabels(angle_labels)
    ax.xaxis.set_tick_params(pad=25)
    ax.set_rlabel_position(0)
    ax.grid(True)
    if log:
        ax.set_rscale('symlog')
    ax.text(0.5, ax.get_rmax(), 'Imp. factor',
            rotation=0, ha='center', va='center', size=12)
    plt.legend(loc=(-0.2, -0.2))

    # Save
    filename = 'circular_plot_baseline_{}_improved_{}_{}'.format(baseline_name, solution_name_1, solution_name_2)
    my_path = os.path.abspath(".")
    path_to_save = os.path.join(my_path, path)
    if not os.path.exists(path_to_save):
        os.mkdir(path_to_save)
    # plt.show()
    plt.savefig(os.path.join(path_to_save, filename + '.pdf'), dpi=300, bbox_inches='tight')
    plt.savefig(os.path.join(path_to_save, filename + '.png'), dpi=300, bbox_inches='tight')


def compute_improvement_factor_dict(baseline_dict, solution_dict):
    improvement_factor_dict = {}

    for parameter, value in baseline_dict.items():
        # Some TI parameters are passed as improvement factors, hence they can just directly be added to the cost
        if "improvement" in parameter:
            improvement_factor_dict[parameter] = solution_dict[parameter]
            continue
        baseline_prob_no_error = TO_PROB_NO_ERROR_FUNCTION[parameter](value)
        prob_no_error = TO_PROB_NO_ERROR_FUNCTION[parameter](solution_dict[parameter])

        improvement_factor_dict[parameter] = 1 / (np.log(prob_no_error) / np.log(baseline_prob_no_error))
    return improvement_factor_dict


def read_param_values(param_names, parameter_file):
    with open(parameter_file, "r") as stream:
        parameter_dict = yaml.load(stream, Loader=Loader)

    return {param_name: parameter_dict[param_name] for param_name in param_names}


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--parameters", nargs="+", default=None, required=True,
                        help="Parameters to include in circular plot.")
    parser.add_argument("--baseline_param_file", type=str, required=True, help="Name of baseline parameter file.")
    parser.add_argument("--solution_param_file_1", type=str, required=True, help="Name of 1st solution parameter file.")
    parser.add_argument("--solution_param_file_2", type=str, required=True, help="Name of 2nd solution parameter file.")
    parser.add_argument("--label_1", type=str, required=False, default=None, help="Label for 1st solution.")
    parser.add_argument("--label_2", type=str, required=False, default=None, help="Label for 2nd solution.")
    parser.add_argument("--output_path", type=str, required=False, default="circular_plot", help="Path to save plot.")
    parser.add_argument("--log", type=str, required=False, default=False, help="Whether or not to use log scale.")

    args, unknown = parser.parse_known_args()

    baseline_param_dict = read_param_values(args.parameters, args.baseline_param_file)
    solution_param_dict_1 = read_param_values(args.parameters, args.solution_param_file_1)
    solution_param_dict_2 = read_param_values(args.parameters, args.solution_param_file_2)

    imp_factor_dict_1 = compute_improvement_factor_dict(baseline_param_dict, solution_param_dict_1)
    imp_factor_dict_2 = compute_improvement_factor_dict(baseline_param_dict, solution_param_dict_2)

    max_radius = max([max(imp_factor_dict_1.values()), max(imp_factor_dict_2.values())])
    plot_circular(improvement_dictionary_1=imp_factor_dict_1,
                  improvement_dictionary_2=imp_factor_dict_2,
                  max_radius=max_radius,
                  baseline_name=args.baseline_param_file.split(".yaml")[0],
                  solution_name_1=args.solution_param_file_1.split(".yaml")[0],
                  solution_name_2=args.solution_param_file_2.split(".yaml")[0],
                  label_1=args.label_1,
                  label_2=args.label_2,
                  path=args.output_path,
                  log=args.log)
