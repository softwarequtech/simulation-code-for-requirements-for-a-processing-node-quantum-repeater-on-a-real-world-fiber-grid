import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import yaml
from netsquid_netconf.netconf import Loader
from argparse import ArgumentParser

font = {'size': 24}

matplotlib.rc('font', **font)

NAME2DISPLAY = {"detector_efficiency": "detection probability \n (excluding transmission)",
                "electron_T2": r"electron T$_2$ (s)",
                "carbon_T2": r"carbon $T_2$ (s)",
                "ec_gate_depolar_prob": "two-qubit gate noise",
                "ec_gate_fidelity": "two-qubit gate fidelity",
                "visibility": "visibility",
                "n1e": "induced storage qubit noise \n (tolerated attempts)",
                "p_double_exc": "probability double excitation",
                "std_electron_electron_phase_drift": "phase drift",
                "T2": r"T$_2$ (s)",
                "swap_quality": "swap quality",
                "dark_count_probability": "dark count probability",
                "emission_fidelity": "emission fidelity"}

DISPLAY2NAME = {v: k for k, v in NAME2DISPLAY.items()}


def bar_plot(file_1, file_2, parameters, label_1="sol_1", label_2="sol_2"):
    """Produces a bar plot with the values of `parameters` read out from `file_1` and `file_2`, which are labelled
    `label_1` and `label_2`.
    """
    solution_param_dict_1 = make_param_dictionary(parameters, args.solution_param_file_1)
    solution_param_dict_2 = make_param_dictionary(parameters, args.solution_param_file_2)

    bar_lengths_1 = []
    bar_lengths_2 = []
    labels = []

    for param, value in solution_param_dict_1.items():
        # bar length is 1 for the biggest value, and value_1 / value_2 for the smallest value
        if value >= solution_param_dict_2[param]:
            bar_lengths_1.append(1)
            bar_lengths_2.append(solution_param_dict_2[param] / value)
        else:
            bar_lengths_2.append(1)
            bar_lengths_1.append(value / solution_param_dict_2[param])
        labels.append(NAME2DISPLAY[param])

    index = np.arange(len(solution_param_dict_1))
    plt.figure(figsize=(7.5, 7.5))
    plt.barh(index + 0.2, bar_lengths_2, height=0.25, hatch="/", label=label_2)
    plt.barh(index - 0.2, bar_lengths_1, height=0.25, label=label_1)

    # label the bars
    plt.yticks(index, labels, rotation="horizontal")

    # put the actual numbers on the bars
    for i, l in enumerate(bar_lengths_1):
        value_1 = solution_param_dict_1[DISPLAY2NAME[labels[i]]]
        formatted_value_1 = "{0:.2g}".format(value_1)

        value_2 = solution_param_dict_2[DISPLAY2NAME[labels[i]]]
        formatted_value_2 = "{0:.2g}".format(value_2)

        plt.text(l + 0.01, i - 0.25, formatted_value_1, verticalalignment="center")
        plt.text(bar_lengths_2[i] + 0.01, i + 0.25, formatted_value_2, verticalalignment="center")
    # plt.xlim([-1, 1.05])

    plt.gca().axes.xaxis.set_ticklabels([])
    plt.tick_params(bottom=False, left=False)
    ax = plt.subplot(111)
    ax.legend(bbox_to_anchor=(0., 1.4))
    plt.box(False)
    plt.savefig("bar_plot_" + str(file_1) + "_" + str(file_2) + ".png",
                bbox_inches="tight", dpi=300)
    plt.savefig("bar_plot_" + str(file_1) + "_" + str(file_2) + ".pdf",
                bbox_inches="tight", dpi=300)


def make_param_dictionary(param_names, parameter_file):
    """Takes list of `param_names`, reads out their values from a `parameter_file` and returns a dictionary where
    the keys are the names and the values are the parameter values.
    """
    with open(parameter_file, "r") as stream:
        parameter_dict = yaml.load(stream, Loader=Loader)

    # display fidelity instead of depolarizing probability
    if "ec_gate_depolar_prob" in parameter_dict:
        parameter_dict["ec_gate_fidelity"] = ((4 - 3 * parameter_dict["ec_gate_depolar_prob"]) / 4) ** 2

    # convert coherence times to second
    for coherence_time in ["T2", "coherence_time", "carbon_T2", "electron_T2"]:
        if coherence_time in parameter_dict:
            parameter_dict[coherence_time] /= 1.e+9

    return {param_name: parameter_dict[param_name] for param_name in param_names}


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--parameters", nargs="+", default=None, required=True,
                        help="Parameters to include in bar plot.")
    parser.add_argument("--solution_param_file_1", type=str, required=True,
                        help="Name of first solution parameter file.")
    parser.add_argument("--solution_param_file_2", type=str, required=True,
                        help="Name of second solution parameter file.")
    parser.add_argument("--label_1", type=str, required=False,
                        help="Label for first set of parameters.")
    parser.add_argument("--label_2", type=str, required=False,
                        help="Label for second set of parameters.")

    args, unknown = parser.parse_known_args()
    bar_plot(file_1=args.solution_param_file_1, file_2=args.solution_param_file_2, parameters=args.parameters,
             label_1=args.label_1, label_2=args.label_2)
