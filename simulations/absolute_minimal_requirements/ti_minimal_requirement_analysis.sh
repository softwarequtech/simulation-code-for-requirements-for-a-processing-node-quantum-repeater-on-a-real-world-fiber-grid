# !/bin/bash

parameter_file_name="ti_parameters.txt"
while read parameter
do
    echo "Performing absolute minimal requirement analysis for parameter $parameter"
    python find_absolute_minimal_requirements.py ti_surf_config.yaml -bpf ti_baseline_params.yaml -ppf ti_perfect_params.yaml \
    -pn $parameter -pltf ti -if 1 2 -n 200 -rt 0.1
done < "$parameter_file_name"
