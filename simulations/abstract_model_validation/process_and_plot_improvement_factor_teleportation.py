"""
This script is used to combine and plot teleportation fidelity against improvement factor data of two platforms.
It takes files from two given directories, combines them and plots the result on the same canvas.
"""

from argparse import ArgumentParser
from netsquid_simulationtools.repchain_data_combine import combine_data
from netsquid_simulationtools.repchain_data_process import process_repchain_dataframe_holder, \
    process_data_teleportation_fidelity, process_data_duration
from plot_improvement_factor_teleportation import plot_two_platforms_teleportation_fidelity

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--directory_pltf_1", type=str, help="1st directory holding files to combine.")
    parser.add_argument("--directory_pltf_2", type=str, help="2nd directory holding files to combine.")
    parser.add_argument("--label_1", type=str, required=False, default="platform_1",
                        help="Label for data of directory 1.")
    parser.add_argument("--label_2", type=str, required=False, default="platform_2",
                        help="Label for data of directory 2.")
    args = parser.parse_args()

    combined_data_1 = combine_data(raw_data_dir=args.directory_pltf_1,
                                   output='combined_' + args.label_1 + '_data.pickle')
    combined_data_2 = combine_data(raw_data_dir=args.directory_pltf_2,
                                   output='combined_' + args.label_2 + '_data.pickle')

    for df_holder, platform in zip([combined_data_1, combined_data_2], [args.label_1, args.label_2]):
        for key in df_holder.dataframe.columns:
            if key not in ["improvement_factor", "state", "generation_duration", "midpoint_outcome_0"]:
                df_holder.dataframe.drop(key, axis=1, inplace=True)

        processed_data = process_repchain_dataframe_holder(df_holder,
                                                           [process_data_teleportation_fidelity, process_data_duration])
        # sort data by first scan_param
        processed_data.sort_values(by=df_holder.varied_parameters[0], inplace=True)
        # save processed data
        processed_data.to_csv("processed_data_" + platform + ".csv", index=False)

    plot_two_platforms_teleportation_fidelity(file_name_1="processed_data_" + args.label_1 + ".csv",
                                              file_name_2="processed_data_" + args.label_2 + ".csv",
                                              label_1=args.label_1,
                                              label_2=args.label_2)
