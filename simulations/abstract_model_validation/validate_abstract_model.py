import sys
import yaml
import copy
from types import SimpleNamespace
from ruamel.yaml import YAML
from argparse import ArgumentParser
import netsquid as ns
from simulations.unified_simulation_script_state import collect_state_data, save_data, setup_networks, \
    innsbruck_visibility_and_coin_probs
from nlblueprint.ion_trap.ti_parameter_set import TIParameterSet
from nlblueprint.abstract_node_mapping.nv_abstract_mapping_delft_eindhoven import NVParameters, NVParametersDoubleClick
from nlblueprint.abstract_node_mapping.ion_trap_abstract_mapping_delft_eindhoven import IonTrapParametersDoubleClick
from netsquid_netconf.netconf import Loader
from netsquid_nv.nv_parameter_set import NVParameterSet
from netsquid_simulationtools.parameter_set import rootbased_improvement_fn
from netsquid_abstractmodel.abstract_parameter_set import AbstractParameterSet

"""This script can be used to validate the abstract model defined in
:class:`~nlblueprint.abstract_node_mapping.abstract_model_delft_eindhoven.AbstractParameters` against the NV center and
trapped ion models. This is done through the following workflow:

1. Take a set of baseline hardware parameters for NV centers or trapped ions. Improve these hardware parameters
according to the desired values of the improvement factor.
2. Perform a simulation of end-to-end entanglement generation for each of the resulting sets of hardware parameters.
3. Convert the baseline hardware values to abstract model parameters. Improve these baseline abstract hardware
parameters according to the same values of the improvement factor.
4. Perform a simulation of end-to-end entanglement generation for each of the resulting sets of hardware parameters.

The resulting data can be processed and plotted  using the accompanying script,
`process_and_plot_improvement_factor_teleportation.py`.
"""

PARAMETER_SETS = {
    "abstract_from_ti_double_click": IonTrapParametersDoubleClick,
    "abstract_from_nv_double_click": NVParametersDoubleClick,
    "abstract_from_nv_single_click": NVParameters,
    "nv": NVParameterSet,
    "ti": TIParameterSet,
    "abstract": AbstractParameterSet
}


def read_from_file(file_name):
    """Read parameters from yaml file.

    Parameters
    ----------
    file_name : str
        Name of yaml file to be read.

    Returns
    -------
    sim_params : dict
        Dictionary where keys are names of parameters and values are their values.

    """
    with open(file_name, "r") as stream:
        sim_params = yaml.load(stream, Loader=Loader)

    return sim_params


def setup_simulation(config_file, param_file):
    """
    Set up simulation according to configuration file and parameter file.

    Parameters
    ----------
    config_file_name : str
        Name of configuration file.
    param_file_name : str
        Name of parameter file.

    Returns
    -------
    generator : generator
        Generator yielding network configurations
    ae : bool
        True if simulating atomic ensemble hardware, False otherwise
    number_nodes : int
        Number of nodes in the network
    sim_params : dict
        Dictionary of simulation parameters.
    """

    ns.sim_reset()
    generator, ae, number_nodes = setup_networks(config_file_name=config_file, param_file_name=param_file)
    sim_params = read_from_file(param_file)

    return generator, ae, number_nodes, sim_params


def update_config_file(config_file, param_file, platform):
    """Update network configuration to have it include a given parameter file. Assumes identifier of parameter file in
    configuration file is of the form platform_sim_params.

    Parameters
    ----------
    config_file_name : str
        Name of configuration file.
    param_file_name : str
        Name of parameter file.
    platform : str
        Hardware platform. Either ti or nv.

    """

    params_str_identifier = str(platform) + '_sim_params'

    yaml = YAML()
    with open(config_file, 'r') as stream:
        code = yaml.load(stream)

    code[params_str_identifier]["INCLUDE"].value = param_file
    with open(config_file, "w") as stream:
        yaml.dump(code, stream)


def _remove_parameters_not_in_original_parameter_set(baseline_sim_params, ParameterSetClass):
    """Some parameters (e.g. gate durations) are included in the abstract model parameter set derived from NV/TI
    parameter set, but not in the NV/TI parameter set. Therefore, if we try to improve them we get an error. This
    function should remove them from the dictionary and return a new one without them.

    Parameters
    ----------
    baseline_sim_params : dict
        Dictionary of baseline parameters.
    ParameterSetClass : class
        Platform-specific parameter set class.

    Returns
    -------
    new_dict : dict
        Dictionary of baseline parameters without parameters not in ParameterSetClass.
    """
    new_dict = copy.deepcopy(baseline_sim_params)
    for key in [key for key in baseline_sim_params if key not in ParameterSetClass.parameter_names()]:
        new_dict.pop(key)

    return new_dict


def _add_parameter_not_in_original_parameter_set(baseline_sim_params, improved_parameter_dict):
    """Reverts action of the `_remove_parameters_not_in_original_parameter_set` method.

    Parameters
    ----------
    baseline_sim_params : dict
        Dictionary of baseline parameters.
    improved_parameter_dict : dict
        Dictionary of improved parameters.

    Returns
    -------
    improved_parameter_dict : dict
        Dictionary of improved parameters with parameters not in ParameterSetClass.
    """
    for key in [key for key in baseline_sim_params if key not in improved_parameter_dict]:
        improved_parameter_dict[key] = baseline_sim_params[key]

    return improved_parameter_dict


def improve_parameter_file(param_file, improvement_factor, hardware_platform):
    """Take parameter file, improve parameter by improvement factor according to root based improvement and dump result
    to new parameter file.

    Parameters
    ----------
    param_file : str
        Name of parameter file to be improved.
    improvement_factor : int
        Value of improvement factor by which parameters should be improved.
    hardware_platform : str
        Hardware platform. Either ti or nv.

    Returns
    -------
    str
        Name of improved parameter file.
    """

    try:
        ParameterSetClass = PARAMETER_SETS[hardware_platform]
    except KeyError:
        print(hardware_platform, "not recognized. Currently supported hardware platforms are", list(PARAMETER_SETS))
        sys.exit(1)

    if improvement_factor > 0:
        baseline_sim_params = read_from_file(file_name=param_file)
        baseline_sim_params_cleaned = \
            _remove_parameters_not_in_original_parameter_set(baseline_sim_params, ParameterSetClass)

        improvement_dictionary = {}
        improvement_dictionary.update(dict.fromkeys(list(baseline_sim_params_cleaned.keys()), improvement_factor))
        improved_parameter_dict = ParameterSetClass.to_improved_dict(baseline_sim_params_cleaned,
                                                                     improvement_dictionary,
                                                                     rootbased_improvement_fn)
        # needed because sometimes the values returned from ParameterSet are np.float, which yaml does not like
        for k, v in improved_parameter_dict.items():
            if ParameterSetClass._get_parameter_by_name(k).type is float:
                improved_parameter_dict[k] = float(v)
            if ParameterSetClass._get_parameter_by_name(k).type is int:
                improved_parameter_dict[k] = int(v)

        improved_parameter_dict_with_non_improved_parameters = \
            _add_parameter_not_in_original_parameter_set(baseline_sim_params, improved_parameter_dict)

        # workaround for different parameter namings
        if hardware_platform == "nv":
            improved_parameter_dict_with_non_improved_parameters["detector_efficiency"] = \
                improved_parameter_dict_with_non_improved_parameters[
                    "prob_detect_excl_transmission_no_conversion_no_cavities"]
        # dump to yaml
        improved_param_file = param_file.split('.yaml')[0] + '_improvement_factor_' + \
            str(improvement_factor) + '.yaml'
        with open(improved_param_file, "w") as stream:
            yaml.dump(improved_parameter_dict_with_non_improved_parameters, stream)

        return improved_param_file
    elif improvement_factor == 0:
        return param_file
    else:
        raise ValueError("Improvement factor is {} but must be greater than or equal to 0.".
              format(improvement_factor))


def _prepare_namespace_for_saving_data(config_file, param_file, platform,
                                       entanglement_generation_protocol, n_runs, output_path):
    """Prepare arguments in a Namespace to be passed to the `save_data` method.

    Parameters
    ----------
    config_file : str
        Name of configuration file used.
    param_file : str
        Name of parameter file used.
    platform : str
        Name of hardware platform simulated.
    entanglement_generation_protocol : str
        Name of entanglement generation protocol employed.
    n_runs : int
        Number of simulation runs per data point.
    output_path : str
        Path to where simulation data was stored.

    Returns
    -------
    namespace_saving_data : :obj:`~types.SimpleNamespace`
        Namespace with information about simulation ran.
    """
    namespace_saving_data = SimpleNamespace()
    namespace_saving_data.configfile = config_file
    namespace_saving_data.paramfile = param_file
    namespace_saving_data.platform = platform
    namespace_saving_data.entanglement_generation_protocol = entanglement_generation_protocol
    namespace_saving_data.n_runs = n_runs
    namespace_saving_data.output_path = output_path
    namespace_saving_data.plot = False

    return namespace_saving_data


def perform_scan_over_improvement_factor(config_file_pltf_specific, config_file_abs, param_file, platform,
                                         entanglement_generation_protocol, improvement_factors,
                                         n_runs, output_path):
    """Performs platform-specific and corresponding abstract simulation for hardware parameters improved by different
    factors.

    Parameters
    ----------
    config_file_pltf_specific : str
        Name of platform specific configuration file.
    config_file_abs : str
        Name of abstract configuration file.
    param_file : str
        Name of platform specific baseline parameter file.
    entanglement_generation_protocol : str
        Name of entanglement generation protocol to be simulated.
    improvement_factors : int or list of int
        Improvement factors for which to improve baseline parameters and perform simulation.
    n_runs : int
        Number of simulation runs per data point.
    output_path : str
        Path to where simulation data was stored.
    """
    if platform == "ti":
        with open(param_file, "r") as stream:
            sim_params = yaml.load(stream)
        v, p_c_p_p, p_c_p_d, p_c_d_d = \
            innsbruck_visibility_and_coin_probs(t_coin=sim_params["coincidence_time_window"],
                                                improvement_factor_visibility=1,
                                                improvement_factor_coin_prob_ph_ph=1)

        # Convert to float because these are np.float, which yaml does not like
        sim_params["visibility"] = float(v)
        sim_params["coin_prob_ph_ph"] = float(p_c_p_p)
        sim_params["coin_prob_ph_dc"] = float(p_c_p_d)
        sim_params["coin_prob_dc_dc"] = float(p_c_d_d)

        # dump to yaml to be picked up by netconf
        with open(param_file, "w") as stream:
            yaml.dump(sim_params, stream)

    update_config_file(config_file=config_file_pltf_specific,
                       param_file=param_file,
                       platform=platform)

    platform_egp = 'abstract_from_' + platform + '_' + entanglement_generation_protocol
    baseline_hardware_platform_param_file = param_file
    baseline_abstract_hardware_platform_parameter_set = \
        PARAMETER_SETS[platform_egp](
            baseline_hardware_platform_param_file)
    baseline_abstract_hardware_platform_parameter_set.dump_to_yaml()
    baseline_abstract_param_file = baseline_abstract_hardware_platform_parameter_set.output_param_file

    for param_file, config_file, platform in zip([baseline_hardware_platform_param_file, baseline_abstract_param_file],
                                                 [config_file_pltf_specific, config_file_abs],
                                                 [platform, platform_egp]):
        for improvement_factor in improvement_factors:
            print(
                'Preparing to run {} simulation with an improvement factor of {}.'.format(platform, improvement_factor))
            new_param_file = improve_parameter_file(param_file=param_file,
                                                    improvement_factor=improvement_factor,
                                                    hardware_platform=platform)

            update_config_file(config_file=config_file,
                               param_file=new_param_file,
                               platform=platform)

            generator, ae, number_nodes, sim_params = setup_simulation(config_file=config_file,
                                                                       param_file=new_param_file)
            sim_params["improvement_factor"] = improvement_factor

            rdfh = collect_state_data(generator=generator, ae=ae, n_runs=n_runs, sim_params=sim_params,
                                      varied_param=None, varied_object=None, number_nodes=number_nodes)
            output_path_this_pltf = output_path + "_" + platform
            args_for_saving_data = _prepare_namespace_for_saving_data(config_file, param_file, platform,
                                                                      entanglement_generation_protocol,
                                                                      n_runs, output_path_this_pltf)

            save_data(meas_holder=rdfh,
                      args=args_for_saving_data)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('config_file_pltf_specific', type=str,
                        help="Name of the config file with realistic hardware components.")
    parser.add_argument('configfile_abs', type=str, help="Name of the config file with abstract hardware components.")
    parser.add_argument('-pf', '--paramfile', required=True, type=str,
                        help="Name of the realistic hardware baseline parameter file.")
    parser.add_argument('-pltf', '--platform', required=True, type=str,
                        help="Hardware platform. Either ti or nv.")
    parser.add_argument('-egp', '--entanglement_generation_protocol', required=True, type=str,
                        help="Entanglement generation protocol. Either single_click or double_click.")
    parser.add_argument('-if', '--improvement_factors', nargs='+', required=True, type=int,
                        help="Improvement factors for which to perform sensitivity analysis.")
    parser.add_argument('-n', '--n_runs', required=False, type=int, default=500,
                        help="Number of runs per configuration. If none is provided, defaults to 500.")
    parser.add_argument('--output_path', required=False, type=str, default="raw_data",
                        help="Path relative to local directory where simulation results should be saved.")

    args, unknown = parser.parse_known_args()
    perform_scan_over_improvement_factor(config_file_pltf_specific=args.config_file_pltf_specific,
                                         config_file_abs=args.configfile_abs,
                                         param_file=args.paramfile,
                                         platform=args.platform,
                                         entanglement_generation_protocol=args.entanglement_generation_protocol,
                                         improvement_factors=args.improvement_factors,
                                         n_runs=args.n_runs,
                                         output_path=args.output_path)
