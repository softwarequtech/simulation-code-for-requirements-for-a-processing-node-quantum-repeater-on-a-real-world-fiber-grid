from simulations.processing_function import parse_from_input_file


def test_parameter_name_parsing():
    test_file_name = "test_input_file.ini"

    test_parameter_names = ["donald", "duck", "likes", "to", "quack"]

    assert parse_from_input_file(test_file_name) == test_parameter_names
