from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from simulations.unified_simulation_script_state import setup_networks
import netsquid as ns
from netsquid.components import QuantumProgram, INSTR_MEASURE, INSTR_H
from netsquid.qubits.qubitapi import fidelity
from netsquid.qubits import ketstates
from netsquid_physlayer.heralded_connection import HeraldedConnection
from netsquid_magic.magic_distributor import DoubleClickMagicDistributor

rc('text', usetex=True)
font = {'family': 'sans-serif',
        'size': 20}
rc('font', **font)


class NodeMeasureProgram(QuantumProgram):
    """Implements arbitrary-basis single-qubit measurement circuit on a quantum processor.

    The measurement is done in any desired basis by first performing a rotation according to the
    specified Euler angles.
    The operations that need to be supported by the quantum processor are
    `netsquid.components.instructions.INSTR_ROT` and `netsquid.components.instructions.INSTR_MEASURE`.

    """

    def program(self, basis="z"):
        qubit_to_meas = self.get_qubit_indices(1)
        if basis == "x":
            self.apply(instruction=INSTR_H, qubit_indices=qubit_to_meas)
        self.apply(instruction=INSTR_MEASURE, qubit_indices=qubit_to_meas, output_key="outcome")
        yield self.run()


def _determine_delivered_state(network):
    [qubit_A] = network.nodes["node_A"].qmemory.peek(positions=0)
    [qubit_B] = network.nodes["node_B"].qmemory.peek(positions=0)
    fidelity_el_b11 = fidelity([qubit_A, qubit_B], ketstates.b11)
    fidelity_el_b01 = fidelity([qubit_A, qubit_B], ketstates.b01)

    if fidelity_el_b11 > fidelity_el_b01:
        return "psi_minus"
    else:
        return "psi_plus"


def process_and_plot(upup_count, updown_count, downup_count, downdown_count, n_runs,
                     upup, updown, downup, downdown, basis, state):
    upup_count_error = np.sqrt(upup_count)
    upup_count /= n_runs
    upup_count_error /= n_runs

    updown_count_error = np.sqrt(updown_count)
    updown_count /= n_runs
    updown_count_error /= n_runs

    downup_count_error = np.sqrt(downup_count)
    downup_count /= n_runs
    downup_count_error /= n_runs

    downdown_count_error = np.sqrt(downdown_count)
    downdown_count /= n_runs
    downdown_count_error /= n_runs

    tot_events_paper = upup + updown + downup + downdown

    upup_count_paper = float(upup / tot_events_paper)
    upup_count_paper_error = np.sqrt(upup) / tot_events_paper
    updown_count_paper = float(updown / tot_events_paper)
    updown_count_paper_error = np.sqrt(updown) / tot_events_paper
    downup_count_paper = float(downup / tot_events_paper)
    downup_count_paper_error = np.sqrt(downup) / tot_events_paper
    downdown_count_paper = float(downdown / tot_events_paper)
    downdown_count_paper_error = np.sqrt(downdown) / tot_events_paper

    # Position of bars on x-axis
    ind = np.arange(4)

    # Figure size
    plt.figure(figsize=(10, 5))

    # Width of a bar
    width = 0.3
    plt.clf()
    plt.bar(x=ind, height=[upup_count, updown_count, downup_count, downdown_count], color="blue", width=width,
            label="Simulation", yerr=[upup_count_error, updown_count_error, downup_count_error, downdown_count_error],
            capsize=3)
    plt.bar(x=ind + width, height=[upup_count_paper, updown_count_paper, downup_count_paper, downdown_count_paper],
            color="orange", width=width, label="Experiment",
            yerr=[upup_count_paper_error, updown_count_paper_error,
            downup_count_paper_error, downdown_count_paper_error], capsize=3)
    plt.xticks(ind + width / 2, (r'$\uparrow\uparrow$', r'$\uparrow\downarrow$',
                                 r'$\downarrow\uparrow$', r'$\downarrow\downarrow$'))
    plt.xlabel("State")
    plt.ylabel("Normalized occurrence")
    plt.legend(loc='best')
    plt.savefig("correlator_" + basis + "_" + state + ".pdf", dpi=300, bbox_inches="tight")
    plt.savefig("correlator_" + basis + "_" + state + ".png", dpi=300, bbox_inches="tight")


def run_simulation(configfile, paramfile, n_runs):
    upup_count_minus_paper = 9
    updown_count_minus_paper = 35
    downup_count_minus_paper = 37
    downdown_count_minus_paper = 18

    upup_count_plus_paper = 31
    updown_count_plus_paper = 22
    downup_count_plus_paper = 17
    downdown_count_plus_paper = 47

    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.DM)
    generator, _, _ = setup_networks(configfile, paramfile)
    measurement_program_A = NodeMeasureProgram()
    measurement_program_B = NodeMeasureProgram()

    for objects, config in generator:
        network = objects["network"]
        for connection in network.connections.values():
            if isinstance(connection, HeraldedConnection):
                nodes = [port.connected_port.component for port in connection.ports.values()]
                magic_distributor = DoubleClickMagicDistributor(nodes, connection)
                connection.magic_distributor = magic_distributor

    for basis in ["z", "x"]:
        upup_count_plus = 0
        updown_count_plus = 0
        downup_count_plus = 0
        downdown_count_plus = 0
        n_runs_plus = 0

        upup_count_minus = 0
        updown_count_minus = 0
        downup_count_minus = 0
        downdown_count_minus = 0
        n_runs_minus = 0

        if basis == "z":
            upup_count_minus_paper = 11
            updown_count_minus_paper = 75
            downup_count_minus_paper = 65
            downdown_count_minus_paper = 18

            upup_count_plus_paper = 4
            updown_count_plus_paper = 65
            downup_count_plus_paper = 45
            downdown_count_plus_paper = 30
        else:
            upup_count_minus_paper = 9
            updown_count_minus_paper = 35
            downup_count_minus_paper = 37
            downdown_count_minus_paper = 18

            upup_count_plus_paper = 31
            updown_count_plus_paper = 22
            downup_count_plus_paper = 17
            downdown_count_plus_paper = 47

        for run in range(n_runs):
            ns.sim_run()

            state = _determine_delivered_state(network)

            network.nodes["node_A"].qmemory.execute_program(measurement_program_A,
                                                            qubit_mapping=[0],
                                                            basis=basis)
            network.nodes["node_B"].qmemory.execute_program(measurement_program_B,
                                                            qubit_mapping=[0],
                                                            basis=basis)
            ns.sim_run()

            # 0 is positive eigenstate in NetSquid
            if state == "psi_plus":
                n_runs_plus += 1
                if not (measurement_program_A.output["outcome"][0] or measurement_program_B.output["outcome"][0]):
                    upup_count_plus += 1
                if not measurement_program_A.output["outcome"][0] and measurement_program_B.output["outcome"][0]:
                    updown_count_plus += 1
                if measurement_program_A.output["outcome"][0] and not measurement_program_B.output["outcome"][0]:
                    downup_count_plus += 1
                if measurement_program_A.output["outcome"][0] and measurement_program_B.output["outcome"][0]:
                    downdown_count_plus += 1
            else:
                n_runs_minus += 1
                if not (measurement_program_A.output["outcome"][0] or measurement_program_B.output["outcome"][0]):
                    upup_count_minus += 1
                if not measurement_program_A.output["outcome"][0] and measurement_program_B.output["outcome"][0]:
                    updown_count_minus += 1
                if measurement_program_A.output["outcome"][0] and not measurement_program_B.output["outcome"][0]:
                    downup_count_minus += 1
                if measurement_program_A.output["outcome"][0] and measurement_program_B.output["outcome"][0]:
                    downdown_count_minus += 1

        process_and_plot(upup_count_plus, updown_count_plus, downup_count_plus, downdown_count_plus, n_runs_plus,
                         upup_count_plus_paper, updown_count_plus_paper, downup_count_plus_paper,
                         downdown_count_plus_paper, basis=basis, state="plus")
        process_and_plot(upup_count_minus, updown_count_minus, downup_count_minus, downdown_count_minus, n_runs_minus,
                         upup_count_minus_paper, updown_count_minus_paper, downup_count_minus_paper,
                         downdown_count_minus_paper, basis=basis, state="minus")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('configfile', type=str, help="Name of the config file.")
    parser.add_argument('-pf', '--paramfile', required=False, type=str,
                        help="Name of the parameter file. Only required as a command line argument for AE simulations.")
    parser.add_argument('-n', '--n_runs', required=False, type=int, default=10,
                        help="Number of runs per configuration. If none is provided, defaults to 10.")

    args, unknown = parser.parse_known_args()

    run_simulation(configfile=args.configfile,
                   paramfile=args.paramfile,
                   n_runs=args.n_runs)
