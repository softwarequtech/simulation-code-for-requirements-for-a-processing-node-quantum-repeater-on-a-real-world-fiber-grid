import csv
import time
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from simulations.unified_simulation_script_state import run_unified_simulation_state

font = {'size': 16}

matplotlib.rc('font', **font)


def plot_runtime_data(runtime_dict, filename):
    run_arr = np.array(list(runtime_dict.keys()))
    runtime_list = list(runtime_dict.values())
    runtime_av_arr = np.array([runtime_list[i][1] for i in range(len(runtime_list))])
    runtime_std_arr = np.array([runtime_list[i][2] for i in range(len(runtime_list))])

    plt.errorbar(run_arr, runtime_av_arr, yerr=runtime_std_arr / np.sqrt(500),
                 fmt='o', label="Measured runtime (s)", markersize=1)
    m, b = np.polyfit(run_arr, runtime_av_arr, 1)
    plt.plot(run_arr, m * run_arr + b, label="Linear fit")
    plt.legend()
    plt.grid(True)
    plt.xlabel("Number of entangled pairs distributed")
    plt.ylabel("Runtime (s)")
    # plt.show()

    plt.savefig(filename + '.pdf', dpi=300, bbox_inches='tight')
    plt.savefig(filename + '.png', dpi=300, bbox_inches='tight')


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('configfile', type=str, help="Name of the config file.")
    parser.add_argument('-pf', '--paramfile', required=False, type=str, help="Name of the parameter file.")
    parser.add_argument('-n', '--n_runs', type=int, nargs="+", default=None, required=True, help="Number of runs.")
    parser.add_argument('--output_file_name', type=str, required=False, default="runtime_analysis",
                        help="Name of file.")

    args, unknown = parser.parse_known_args()
    elapsed_time_per_number_runs = {}
    number_of_repetitions_per_data_point = 500
    for number_of_runs in args.n_runs:
        elapsed_time = []
        for number_of_reps in range(number_of_repetitions_per_data_point):
            number_of_runs = int(number_of_runs)
            start_time = time.time()
            run_unified_simulation_state(configfile=args.configfile,
                                         paramfile=args.paramfile,
                                         n_runs=number_of_runs)
            end_time = time.time()
            elapsed_time_one_run = end_time - start_time
            elapsed_time.append(elapsed_time_one_run)
        elapsed_time_av = np.average(elapsed_time)
        elapsed_time_std = np.std(elapsed_time)
        elapsed_time_per_number_runs[number_of_runs] = [elapsed_time, elapsed_time_av, elapsed_time_std]

    timestr = time.strftime("%Y%m%d_%H%M%S")
    filename = args.output_file_name + '_' + timestr

    raw_data_filename = args.output_file_name + '_raw_' + timestr

    with open(raw_data_filename + '.csv', 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["number_runs", "elapsed_time"])
        for key, val in elapsed_time_per_number_runs.items():
            for run in val[0]:
                writer.writerow([key, run])

    with open(filename + '.csv', 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["number_runs", "elapsed_time_av(s)", "elapsed_time_std(s)"])
        for key, val in elapsed_time_per_number_runs.items():
            writer.writerow([key, val[1], val[2]])

    plot_runtime_data(runtime_dict=elapsed_time_per_number_runs,
                      filename=filename)
