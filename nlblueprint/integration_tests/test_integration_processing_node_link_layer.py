import pytest
import numpy as np
import netsquid as ns
from netsquid.qubits.qubitapi import reduced_dm
from netsquid.util import DataCollector
from pydynaa.core import EventExpression
from qlink_interface import MeasurementBasis, ReqMeasureDirectly, ReqCreateAndKeep, ResMeasureDirectly, ResCreateAndKeep
from netsquid_magic.magic_distributor import DoubleClickMagicDistributor
from netsquid_netconf.netconf import ComponentBuilder, netconf_generator
from netsquid_physlayer.heralded_connection import MiddleHeraldedConnection, HeraldedConnection
from nlblueprint.processing_nodes.nodes_with_drivers import AbstractNodeWithDriver, TINodeWithDriver, \
    NVNodeWithDriver, DepolarizingNodeWithDriver
from nlblueprint.egp_datacollector import EGPDataCollector, EGPDataCollectorState
from netsquid_driver.EGP import EGPService
from nlblueprint.control_layer.swapasap_egp import SwapAsapEndNodeLinkLayerProtocol
from simulations.unified_simulation_script_state import collect_state_data, run_simulation
from netsquid_physlayer.classical_connection import ClassicalConnectionWithLength as ClassicalConnection


class EGPDataCollectorBothNodes:
    """EGP data collector that collects state data from both end nodes. Used to test that qubit ordering is as expected.
    """
    def __init__(self, start_time=ns.sim_time()):
        # probably this in not necessary
        self.sim_time = start_time

    def __call__(self, evexpr):
        """Picks up responses from the EGP when they are signalled by both Protocols.
        """
        generation_time = ns.sim_time() - self.sim_time
        self.sim_time = ns.sim_time()

        response_alice = evexpr.first_term.triggered_events[0]. \
            source.get_signal_by_event(evexpr.first_term.triggered_events[0])
        response_bob = evexpr.second_term.triggered_events[0]. \
            source.get_signal_by_event(evexpr.second_term.triggered_events[0])

        assert evexpr.first_term.triggered_events[0].source != evexpr.second_term.triggered_events[0].source

        [alice_qubit] = evexpr.first_term.triggered_events[0].source.node. \
            qmemory.peek(positions=response_alice.result.logical_qubit_id, skip_noise=False)
        [bob_qubit] = evexpr.second_term.triggered_events[0].source.node. \
            qmemory.peek(positions=response_bob.result.logical_qubit_id, skip_noise=False)
        state_alice = reduced_dm([alice_qubit, bob_qubit])
        state_bob = reduced_dm([bob_qubit, alice_qubit])

        data = {"state_alice": state_alice,
                "state_bob": state_bob,
                "generation_duration": generation_time * 1E-9,
                "midpoint_outcome_0_alice": response_alice.result.bell_state,
                "midpoint_outcome_0_bob": response_bob.result.bell_state
                }

        return data


def reset(network):
    """Stop protocols and reset components to get ready for next simulation run."""

    for node in network.nodes.values():
        for service_instantiation in node.driver._services.values():
            service_instantiation.stop()
        node.reset()
    for connection in network.connections.values():
        connection.reset()


def setup_protocols(network):
    """Setup any protocols that are not set up using netconf by :func:`setup_network`. Also starts all services.

    Most protocols should already be set in the driver of the nodes in the network.
    It is here assumed that the only protocols that are not yet set up, are the link-layer protocols
    (since these need global information about the network when constructed).
    All services known to the driver of each node in the network are started afterwards.

    Parameters
    ----------
    network : :class:`netsquid.nodes.network.Network`
        Network on which protocols should be set up.

    """

    # repeaters have 4 ports: two communication ports, two entanglement ports
    repeaters = [node for node in network.nodes.values() if len(node.ports) == 4]

    # end nodes have 2 ports: one communication port, one entanglement port
    end_nodes = [node for node in network.nodes.values() if len(node.ports) == 2]

    # check that there are only 2 end nodes
    assert len(end_nodes) == 2

    # check that each node is either a repeater node or an end node
    assert len(repeaters) + len(end_nodes) == len(network.nodes)

    # start all protocols on repeaters
    for repeater in repeaters:
        repeater.driver.start_all_services()

    # add link-layer protocol to end nodes and start all protocols
    for end_node in end_nodes:
        end_node.driver.add_service(EGPService, SwapAsapEndNodeLinkLayerProtocol(node=end_node))
        end_node.driver.start_all_services()


def implement_magic(network):
    """Add magic distributors to a physical network.

    To every connection in the network, a magic distributor is added as attribute.
    Currently, this is always a :class:`netsquid_magic.magic_distributor.DoubleClickMagicDistributor`.
    The magic distributor is given the connection as argument upon construction (so that parameters to be used in magic
    can be read from the connection).

    Parameters
    ----------
    network : :class:`netsquid.nodes.network.Network`
        Network that the network distributors should be added to.

    """

    for connection in network.connections.values():
        if isinstance(connection, HeraldedConnection):
            nodes = [port.connected_port.component for port in connection.ports.values()]
            magic_distributor = DoubleClickMagicDistributor(nodes, connection)
            connection.magic_distributor = magic_distributor


def setup_networks(config_file_name):
    """
    Set up network(s) according to configuration file

    Parameters
    ----------
    config_file_name : str
        Name of configuration file

    Returns
    -------
    generator : generator
        Generator yielding network configurations
    """

    # add required components to ComponentBuilder
    ComponentBuilder.add_type(name="abstract_node", new_type=AbstractNodeWithDriver)
    ComponentBuilder.add_type(name="ti_node", new_type=TINodeWithDriver)
    ComponentBuilder.add_type(name="nv_node", new_type=NVNodeWithDriver)
    ComponentBuilder.add_type(name="depolarizing_node", new_type=DepolarizingNodeWithDriver)
    ComponentBuilder.add_type(name="mid_heralded_connection", new_type=MiddleHeraldedConnection)
    ComponentBuilder.add_type(name="classical_connection", new_type=ClassicalConnection)

    generator = netconf_generator(config_file_name)
    return generator


def magic_and_protocols(objects):
    network = objects["network"]
    implement_magic(network)
    setup_protocols(network)
    end_nodes = [node for node in network.nodes.values() if len(node.ports) == 2]
    egp_services = [end_node.driver[EGPService] for end_node in end_nodes]
    return egp_services, network


def run_simulation_data_both_nodes(egp_services, request_type, response_type, n_runs, experiment_specific_arguments={}):
    """
    Places a request for a given number of pairs to be created and kept, runs the simulation and
    collects the resulting state data from both nodes.

    Parameters
    ----------
    egp_services : list
        List holding EGP end node services
    request_type : :class:`qlink_interface.ReqCreateBase`
        Type of request to be submitted.
    response_type : :class:`qlink_interface.ResCreateBase`
        Type of response corresponding to the submitted request
    n_runs : int
        Number of pairs to generate and measure
    experiment_specific_arguments : dict
        Dictionary holding extra experiment-specific arguments e.g. measurement bases

    Returns
    -------
    :class:`pandas.DataFrame`
        Dataframe holding simulation results
    """
    ns.set_qstate_formalism(ns.qubits.qformalism.QFormalism.DM)
    alice_responds = EventExpression(source=egp_services[0],
                                     event_type=egp_services[0].signals.get(response_type.__name__))
    bob_responds = EventExpression(source=egp_services[1],
                                   event_type=egp_services[1].signals.get(response_type.__name__))

    both_nodes_respond = alice_responds & bob_responds
    egp_datacollector = DataCollector(EGPDataCollectorState(start_time=ns.sim_time()))
    egp_datacollector_both_nodes = DataCollector(EGPDataCollectorBothNodes(start_time=ns.sim_time()))
    egp_datacollector.collect_on(both_nodes_respond)
    egp_datacollector_both_nodes.collect_on(both_nodes_respond)

    local = egp_services[0]
    remote = egp_services[1]
    request = request_type(remote_node_id=remote.node.ID,
                           number=n_runs,
                           **experiment_specific_arguments)

    local.put(request)

    # run simulation
    ns.sim_run()

    return egp_datacollector.dataframe, egp_datacollector_both_nodes.dataframe


def measure_directly_10_pairs(config_file_name):
    """
    Sets up network, protocols and submits a single request to a processing node link layer for 10 pairs to be
    measured in the X basis. Checks that expected amount of data was collected and that it is in the expected format.
    """
    ns.sim_reset()
    generator = setup_networks(config_file_name)
    objects, config = next(generator)
    egp_services, network = magic_and_protocols(objects=objects)
    for measurement_basis in [MeasurementBasis.X, MeasurementBasis.Z]:
        if measurement_basis == MeasurementBasis.X:
            exp_spec_args = {"y_rotation_angle_local": np.pi / 2, "y_rotation_angle_remote": np.pi / 2}
        else:
            exp_spec_args = {"y_rotation_angle_local": 0, "y_rotation_angle_remote": 0}

        data_single_run = run_simulation(egp_services=egp_services,
                                         request_type=ReqMeasureDirectly,
                                         response_type=ResMeasureDirectly,
                                         data_collector=EGPDataCollector,
                                         n_runs=10,
                                         experiment_specific_arguments=exp_spec_args)
        assert len(data_single_run) == 10
        for time_stamp in data_single_run["time_stamp"].to_list():
            assert type(time_stamp) == float
        for basis_A in data_single_run["basis_A"].to_list():
            assert basis_A == str(measurement_basis).split('.')[1]
        for basis_B in data_single_run["basis_B"].to_list():
            assert basis_B == str(measurement_basis).split('.')[1]
        for outcome_A in data_single_run["outcome_A"].to_list():
            assert type(outcome_A) == float
        for outcome_B in data_single_run["outcome_B"].to_list():
            assert type(outcome_B) == float
        for generation_duration in data_single_run["generation_duration"].to_list():
            assert type(generation_duration) == float
        for midpoint_outcome_0 in data_single_run["midpoint_outcome_0"].to_list():
            assert type(midpoint_outcome_0) == float


def create_and_keep(config_file_name):
    ns.sim_reset()
    generator = setup_networks(config_file_name)
    objects, config = next(generator)
    egp_services, network = magic_and_protocols(objects=objects)
    data, data_both_nodes = run_simulation_data_both_nodes(egp_services=egp_services,
                                                           request_type=ReqCreateAndKeep,
                                                           response_type=ResCreateAndKeep,
                                                           n_runs=1)
    # we expect Alice and Bob's state to be the same up to a reordering of the qubits
    # so we apply SWAP and assert equality to verify this
    SWAP = np.array([[1, 0, 0, 0], [0, 0, 1, 0], [0, 1, 0, 0], [0, 0, 0, 1]])
    assert len(data) == 1
    assert data_both_nodes.midpoint_outcome_0_alice[0] == data_both_nodes.midpoint_outcome_0_bob[0]
    assert np.isclose(data.state[0], SWAP @ data_both_nodes.state_bob[0] @ np.conj(SWAP).T).all()
    assert (data_both_nodes.state_alice[0] == data.state[0]).all()


@pytest.mark.integtest
def test_measure_directly_10_pairs_abstract_2_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/abstract_config_2_nodes.yaml"
    measure_directly_10_pairs(config_file_name)


@pytest.mark.integtest
def test_create_and_keep_abstract_2_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/abstract_config_2_nodes.yaml"
    create_and_keep(config_file_name)


@pytest.mark.integtest
def test_measure_directly_10_pairs_abstract_3_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/abstract_config_3_nodes.yaml"
    measure_directly_10_pairs(config_file_name)


@pytest.mark.integtest
def test_create_and_keep_abstract_3_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/abstract_config_3_nodes.yaml"
    create_and_keep(config_file_name)


@pytest.mark.integtest
def test_measure_directly_10_pairs_abstract_6_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/abstract_config_6_nodes.yaml"
    measure_directly_10_pairs(config_file_name)


@pytest.mark.integtest
def test_create_and_keep_abstract_6_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/abstract_config_6_nodes.yaml"
    create_and_keep(config_file_name)


@pytest.mark.integtest
def test_measure_directly_10_pairs_nv_3_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/nv_config_3_nodes.yaml"
    measure_directly_10_pairs(config_file_name)


@pytest.mark.integtest
def test_create_and_keep_nv_3_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/nv_config_3_nodes.yaml" 
    create_and_keep(config_file_name)


@pytest.mark.integtest
def test_measure_directly_10_pairs_nv_6_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/nv_config_6_nodes.yaml"
    measure_directly_10_pairs(config_file_name)


@pytest.mark.integtest
def test_create_and_keep_nv_6_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/nv_config_6_nodes.yaml"
    create_and_keep(config_file_name)


@pytest.mark.integtest
def test_measure_directly_10_pairs_ti_3_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/ti_config_3_nodes.yaml"
    measure_directly_10_pairs(config_file_name)


@pytest.mark.integtest
def test_create_and_keep_ti_3_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/ti_config_3_nodes.yaml"
    create_and_keep(config_file_name)


@pytest.mark.integtest
def test_create_and_keep_ti_6_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/ti_config_6_nodes.yaml"
    create_and_keep(config_file_name)


@pytest.mark.integtest
def test_measure_directly_10_pairs_ti_6_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/ti_config_6_nodes.yaml"
    measure_directly_10_pairs(config_file_name)


@pytest.mark.integtest
def test_measure_directly_10_pairs_depolarizing_3_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/depolarizing_config_3_nodes.yaml"
    measure_directly_10_pairs(config_file_name)


@pytest.mark.integtest
def test_measure_directly_10_pairs_depolarizing_6_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/depolarizing_config_6_nodes.yaml"
    measure_directly_10_pairs(config_file_name)


@pytest.mark.integtest
def test_create_and_keep_depolarizing_3_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/depolarizing_config_3_nodes.yaml"
    create_and_keep(config_file_name)


@pytest.mark.integtest
def test_create_and_keep_depolarizing_6_node_chain():
    config_file_name = "./nlblueprint/integration_tests/test_configs/depolarizing_config_6_nodes.yaml"
    create_and_keep(config_file_name)


@pytest.mark.integtest
def test_unified_simulation_script_data_collection():
    config_file_name = "./nlblueprint/integration_tests/test_configs/abstract_config_3_nodes.yaml"
    generator = setup_networks(config_file_name)
    n_runs = 2
    sim_params = None
    varied_param = None
    varied_object = None
    number_nodes = 3
    ns.sim_reset()
    meas_holder = collect_state_data(generator=generator,
                                     n_runs=n_runs,
                                     sim_params=sim_params,
                                     varied_object=varied_object,
                                     varied_param=varied_param,
                                     number_nodes=number_nodes)
    assert len(meas_holder.dataframe) == n_runs
