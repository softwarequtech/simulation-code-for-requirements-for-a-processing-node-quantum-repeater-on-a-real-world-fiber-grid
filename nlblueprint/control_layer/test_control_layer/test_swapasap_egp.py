import itertools
import numpy as np
import netsquid as ns
import logging
from nlblueprint.processing_nodes.nodes_with_drivers import AbstractNodeWithDriver, abstract_node_standard_params
from netsquid.qubits.ketstates import BellIndex
from netsquid.protocols import NodeProtocol
import netsquid.qubits.qubitapi as qapi

from netsquid_entanglementtracker.bell_state_tracker import BellStateTracker
from nlblueprint.control_layer.swapasap_egp import (
    SwapAsapEndNodeLinkLayerProtocol,
    ResMeasureDirectly)
from qlink_interface import ReqMeasureDirectly
from netsquid_driver.entanglement_service import (
    EntanglementService,
    ResEntanglementSuccess, EntanglementServiceWithSingleNodePerPort, ResStateReady,
    ResEntanglementFail)
from netsquid_entanglementtracker.entanglement_tracker_service import EntanglementTrackerService
from netsquid_driver.message_handler_protocol import MessageHandlerProtocol
from netsquid_physlayer.classical_connection import ClassicalConnection
from netsquid.components.component import Message
from nlblueprint.control_layer.swap_asap import SwapASAP
from netsquid_driver.swap_asap_service import SwapASAPService
from netsquid_driver.measurement_services import SwapService, MeasureService, ResMeasure, ResSwap


class MockEntanglementService(EntanglementService):

    def generate_entanglement(self, req):
        self.send_response(ResEntanglementSuccess(port_name='test', bell_index=BellIndex(3)))

    def abort(self, req):
        super().abort(req)


class MockMeasureService(MeasureService):

    def measure(self, req):
        super().measure(req=req)
        self.send_response(ResMeasure(outcome=True))


class MockSwapService(SwapService):

    def swap(self, req):
        super().swap(req)
        bell_index = BellIndex.B11
        self.send_response(ResSwap(outcome=True))
        self.node.driver[EntanglementTrackerService].register_local_swap_mem_pos(mem_pos_1=req.mem_pos_1,
                                                                                 mem_pos_2=req.mem_pos_2,
                                                                                 bell_index=bell_index)


class MockNetworkLayer(NodeProtocol):

    def __init__(self, node, egp):
        super().__init__(node=node, name="mock_network_layer")
        self.add_subprotocol(egp, "EGP")
        self.finished = False

    def run(self):
        egp = self.subprotocols["EGP"]
        egp.start()
        egp.put(ReqMeasureDirectly())

        create_id = 0
        message = Message(["CONFIRM", create_id], **{"forward": "chain"})
        self.node.ports["A"].tx_input(message)

        yield self.await_signal(sender=egp,
                                signal_label=ResMeasureDirectly.__name__)
        response = egp.get_signal_result(label=ResMeasureDirectly.__name__, receiver=self)
        assert response.measurement_outcome
        assert not egp._should_keep
        self.finished = True


class SimpleEntanglementServiceWithConnections(EntanglementServiceWithSingleNodePerPort):
    """Minimal implementation of :class:`EntanglementService` for testing purposes.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    interval_duration : float
        Time between all signals (ready - fail - ready - success).
        Total duration of entanglement generation on the first port is `4 * interval_duration_0`.

    """
    class SimpleEntanglementSubprotocol(NodeProtocol):
        """Perform mock entanglement generation successfully once, after one failure.

        Parameters
        ----------
        node : :class:`~netsquid.nodes.node.Node`
            The node this protocol is running on.
        superprotocol : :class:`SimpleEntanglement`
            Protocol this is a subprotocol of.
        port_name : str
            Name of port to generate entanglement over in a mock way.
        mem_pos : int
            Memory position to deliver the qubit.
        interval_duration : float
            Time between all signals (ready - fail - ready - success).
            Total duration of entanglement generation on the first port is `4 * interval_duration_0`.

        """

        def __init__(self, node, superprotocol, port_name, mem_pos, interval_duration):
            super().__init__(node=node, name="simple_entanglement_subprotocol")
            self.superprotocol = superprotocol
            self.port_name = port_name
            self.mem_pos = mem_pos
            self.interval_duration = interval_duration

        def run(self):
            # state ready
            yield self.await_timer(duration=self.interval_duration)
            self.superprotocol.send_response(ResStateReady(port_name=self.port_name,
                                                           mem_pos=self.mem_pos))
            # this attempt failed
            yield self.await_timer(duration=self.interval_duration)
            self.superprotocol.send_response(ResEntanglementFail(port_name=self.port_name,
                                                                 mem_pos=self.mem_pos))
            # state ready of next attempt
            yield self.await_timer(duration=self.interval_duration)
            self.superprotocol.send_response(ResStateReady(port_name=self.port_name,
                                                           mem_pos=self.mem_pos))
            # this attempt succeeded!
            yield self.await_timer(duration=self.interval_duration)
            # bell_index depends on port name to check if we identified "upstream" correctly
            remote_node_id = self.superprotocol.get_remote_node_id(self.port_name)
            self.superprotocol.send_response(ResEntanglementSuccess(remote_node_id=remote_node_id,
                                                                    port_name=self.port_name,
                                                                    mem_pos=self.mem_pos,
                                                                    bell_index=BellIndex(0)))
            qubit = qapi.create_qubits(1)
            self.node.qmemory.put(qubit, positions=self.mem_pos)

    def __init__(self, node, interval_duration=400):
        super().__init__(node=node)
        self.interval_duration = interval_duration
        self._add_subprotocols()

    def _add_subprotocols(self):
        """Add subprotocol for every possible port_name - mem_pos combination."""
        for port_name, mem_pos in itertools.product(self.node.ports, range(self.node.qmemory.num_positions)):
            subprot_name = self.get_subprot_name(port_name=port_name, mem_pos=mem_pos)
            subprotocol = self.SimpleEntanglementSubprotocol(node=self.node,
                                                             superprotocol=self,
                                                             port_name=port_name,
                                                             mem_pos=mem_pos,
                                                             interval_duration=self.interval_duration)
            self.add_subprotocol(subprotocol, name=subprot_name)

    def generate_entanglement(self, req):
        """Start subprotocol corresponding to this port_name - mem_pos combination."""
        subprot_name = self.get_subprot_name(port_name=req.port_name, mem_pos=req.mem_pos)
        subprotocol = self.subprotocols[subprot_name]
        subprotocol.start()

    @staticmethod
    def get_subprot_name(port_name, mem_pos):
        return str(port_name) + str(mem_pos)

    def get_remote_node_id(self, port_name):
        """We are just hardcoding the mapping between port names and node IDs as it happens to be in this example."""
        if port_name == "ENT":  # this must be an end node, so the remote node is node_M
            return 1
        if port_name == "ENT_A":  # this must be the middle node; side "A" is connected to node_L
            return 0
        if port_name == "ENT_B":  # this must be the middle node; side "B" is connected to node_R
            return 2
        raise ValueError(f"{port_name} not a known port name")

    def start(self):
        pass  # to avoid automatically starting all subprotocols

    def abort(self, req):
        pass


abstract_node_params = dict(abstract_node_standard_params)
del abstract_node_params["ID"]


def setup_3_node_network():
    ns.sim_reset()

    # physical
    # Need to add ports A and B on end nodes otherwise forwarding doesn't work
    node_L = AbstractNodeWithDriver(name="test_node_L", ID=0, end_node=True, **abstract_node_params)
    node_M = AbstractNodeWithDriver(name="test_node_M", ID=1, end_node=False, **abstract_node_params)
    node_R = AbstractNodeWithDriver(name="test_node_R", ID=2, end_node=True, **abstract_node_params)
    node_L.add_ports(["A", "ENT"])
    node_M.add_ports(["A", "B", "ENT_A", "ENT_B"])
    node_R.add_ports(["A", "ENT"])

    connection_LM = ClassicalConnection(name="test_connection_LM")
    connection_MR = ClassicalConnection(name="test_connection_MR")

    connection_LM.ports["A"].connect(node_L.ports["A"])
    connection_LM.ports["B"].connect(node_M.ports["A"])
    connection_MR.ports["A"].connect(node_M.ports["B"])
    connection_MR.ports["B"].connect(node_R.ports["A"])

    # add protocols
    bell_state_tracker_L = BellStateTracker(node=node_L)
    local_entanglement_protocol_L = SimpleEntanglementServiceWithConnections(node=node_L)
    message_handler_protocol_L = MessageHandlerProtocol(node=node_L)
    measure_protocol_L = MockMeasureService(node=node_L)

    bell_state_tracker_R = BellStateTracker(node=node_R)
    local_entanglement_protocol_R = SimpleEntanglementServiceWithConnections(node=node_R)
    message_handler_protocol_R = MessageHandlerProtocol(node=node_R)
    measure_protocol_R = MockMeasureService(node=node_R)

    bell_state_tracker_M = BellStateTracker(node=node_M)
    message_handler_protocol_M = MessageHandlerProtocol(node=node_M)
    swap_asap_protocol_M = SwapASAP(node=node_M)
    local_entanglement_protocol_M = SimpleEntanglementServiceWithConnections(node=node_M)
    swap_service_M = MockSwapService(node=node_M)

    node_L.driver.add_service(EntanglementTrackerService, bell_state_tracker_L)
    node_L.driver.add_service(EntanglementService, local_entanglement_protocol_L)
    node_L.driver.add_service(MessageHandlerProtocol, message_handler_protocol_L)
    node_L.driver.add_service(MeasureService, measure_protocol_L)
    local_entanglement_protocol_L.start()
    message_handler_protocol_L.start()
    measure_protocol_L.start()

    node_R.driver.add_service(EntanglementTrackerService, bell_state_tracker_R)
    node_R.driver.add_service(EntanglementService, local_entanglement_protocol_R)
    node_R.driver.add_service(MessageHandlerProtocol, message_handler_protocol_R)
    node_R.driver.add_service(MeasureService, measure_protocol_R)
    local_entanglement_protocol_R.start()
    message_handler_protocol_R.start()
    measure_protocol_R.start()

    node_M.driver.add_service(EntanglementTrackerService, bell_state_tracker_M)
    node_M.driver.add_service(SwapASAPService, swap_asap_protocol_M)
    node_M.driver.add_service(EntanglementService, local_entanglement_protocol_M)
    node_M.driver.add_service(SwapService, swap_service_M)
    local_entanglement_protocol_M.start()
    message_handler_protocol_M.start()
    swap_service_M.start()

    egp_L = SwapAsapEndNodeLinkLayerProtocol(node=node_L)
    egp_R = SwapAsapEndNodeLinkLayerProtocol(node=node_R)

    # start protocols
    egp_L.start()
    egp_R.start()
    message_handler_protocol_M.start()

    return egp_L, egp_R


def test_measure_directly_chain_1_repeater():
    ns.logger.setLevel(logging.DEBUG)

    egp_L, egp_R = setup_3_node_network()
    egp_L.put(ReqMeasureDirectly(remote_node_id=egp_R.node.ID))
    ns.sim_run()

    response = egp_L.get_signal_result(label=ResMeasureDirectly.__name__)
    assert response.measurement_outcome
    assert not egp_L._should_keep


def test_measure_directly_chain_1_repeater_custom_basis():
    egp_L, egp_R = setup_3_node_network()
    egp_L.put(ReqMeasureDirectly(remote_node_id=egp_R.node.ID,
                                 x_rotation_angle_local_1=0,
                                 y_rotation_angle_local=np.pi / 2,
                                 x_rotation_angle_local_2=0,
                                 x_rotation_angle_remote_1=0,
                                 y_rotation_angle_remote=np.pi / 2,
                                 x_rotation_angle_remote_2=0))
    ns.sim_run()

    response = egp_L.get_signal_result(label=ResMeasureDirectly.__name__)
    assert response.measurement_outcome
    assert not egp_L._should_keep
    assert egp_L._local_measurement_angles == egp_R._local_measurement_angles
    assert egp_L._local_measurement_angles["x_1"] == 0
    assert egp_L._local_measurement_angles["y"] == np.pi / 2
    assert egp_L._local_measurement_angles["x_2"] == 0


def test_is_connected():

    node = AbstractNodeWithDriver("test_node", end_node=False, **abstract_node_standard_params)
    egp = SwapAsapEndNodeLinkLayerProtocol(node=node)
    assert not egp.is_connected
    node.add_ports("A")
    assert egp.is_connected
    node.add_ports("B")
    assert not egp.is_connected
