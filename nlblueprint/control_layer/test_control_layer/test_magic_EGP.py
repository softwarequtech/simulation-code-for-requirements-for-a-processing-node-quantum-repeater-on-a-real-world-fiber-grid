import netsquid as ns
import numpy as np
import abc
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.protocols.protocol import Protocol
from netsquid_magic.link_layer import TranslationUnit, MagicLinkLayerProtocolWithSignaling
from netsquid_magic.magic_distributor import MagicDistributor
from netsquid_magic.state_delivery_sampler import IStateDeliverySamplerFactory, StateDeliverySampler

from unittest import TestCase
import unittest

from netsquid.nodes import Node
from nlblueprint.control_layer.magic_EGP import MagicEGP
from netsquid.qubits import StateSampler

from qlink_interface import (
    ReqCreateAndKeep,
    ReqMeasureDirectly,
    ReqReceive,
    ReqStopReceive,
    ResCreateAndKeep,
    ResMeasureDirectly,
    ResError,
    MeasurementBasis,
    RandomBasis,
    ErrorCode,
)


class MockMagicLinkLayerProtocol(MagicLinkLayerProtocolWithSignaling):

    class MockStateDeliverySamplerFactory(IStateDeliverySamplerFactory):
        """Always successfull, always delivers the state |00>."""

        def create_state_delivery_sampler(self, **kwargs):
            return StateDeliverySampler(state_sampler=StateSampler(qreprs=[np.array([1, 0, 0, 0])],
                                                                   labels=[("success", 0)]),
                                        cycle_time=1000)

    class MockTranslationUnit(TranslationUnit):

        def request_to_parameters(self, request, **fixed_parameters):
            return {}

    def __init__(self, nodes):
        magic_distributor = MagicDistributor(delivery_sampler_factory=self.MockStateDeliverySamplerFactory(),
                                             nodes=nodes,
                                             state_delay=0,
                                             label_delay=100)
        super().__init__(nodes=nodes, magic_distributor=magic_distributor, translation_unit=self.MockTranslationUnit())


class TestMagicEGP(TestCase):

    class MockNetworkLayer(Protocol, metaclass=abc.ABCMeta):

        def __init__(self, egp1, egp2):
            super().__init__(name="magic_link_layer_test_mock_network_layer")
            self.add_subprotocol(egp1, "EGP1")
            self.add_subprotocol(egp2, "EGP2")
            self.finished = False

        def start(self):
            super().start()
            self.subprotocols["EGP1"].start()
            self.subprotocols["EGP2"].start()

        def stop(self):
            super().stop()
            self.subprotocols["EGP1"].stop()
            self.subprotocols["EGP2"].stop()

        def run(self):
            # put requests
            self.subprotocols["EGP2"].put(
                request=ReqReceive(remote_node_id=self.subprotocols["EGP1"].node.ID))
            success_signal_label = self.make_request()
            # wait for responses: fail and success from both nodes
            evts_success = [self.await_signal(sender=egp, signal_label=success_signal_label)
                            for egp in self.subprotocols.values()]
            evts_fail = [self.await_signal(sender=egp, signal_label=ResError.__name__)
                         for egp in self.subprotocols.values()]
            responses = []
            for _ in range(2):  # we expect one response for each of two nodes
                evt_expr = yield (evts_success[0] | evts_fail[0]) | (evts_success[1] | evts_fail[1])
                if evt_expr.first_term.value:  # if response comes from EGP1
                    evt_expr_one_egp = evt_expr.first_term
                    egp = self.subprotocols["EGP1"]
                else:
                    evt_expr_one_egp = evt_expr.second_term
                    egp = self.subprotocols["EGP2"]
                if evt_expr_one_egp.second_term.value:  # if response indicates failure
                    res = egp.get_signal_result(label=ResError.__name__, receiver=self)
                else:
                    res = egp.get_signal_result(label=success_signal_label, receiver=self)
                responses.append(res)

            self.check(responses)
            self.finished = True

        @abc.abstractmethod
        def make_request(self):
            return []

        @abc.abstractmethod
        def check(self, responses):
            # check if both EGP's gave a response
            assert {res.remote_node_id for res in responses} == {self.subprotocols["EGP1"].node.ID,
                                                                 self.subprotocols["EGP2"].node.ID}
            # check if directionality flag is correct
            assert ([res.remote_node_id == self.subprotocols["EGP1"].node.ID for res in responses] ==
                    [res.directionality_flag for res in responses])

    @classmethod
    def setUp(self):
        ns.sim_reset()
        qmem1 = QuantumProcessor(name="magic_link_layer_test_memory_1", num_positions=1)
        qmem2 = QuantumProcessor(name="magic_link_layer_test_memory_2", num_positions=1)
        self.node1 = Node(name="magic_link_layer_test_node_1", ID=1, qmemory=qmem1, port_names=["1.a"])
        self.node2 = Node(name="magic_link_layer_test_node_2", ID=2, qmemory=qmem2, port_names=["2.a"])

        self.NetworkLayer = None

        magic_link_layer_protocol = MockMagicLinkLayerProtocol(nodes=[self.node1, self.node2])
        self.egp1 = MagicEGP(node=self.node1,
                             magic_link_layer_protocol=magic_link_layer_protocol,
                             name="magic_test_egp_1")
        self.egp2 = MagicEGP(node=self.node2,
                             magic_link_layer_protocol=magic_link_layer_protocol,
                             name="magic_test_egp_2")

    def tearDown(self):
        network_layer = self.NetworkLayer(egp1=self.egp1, egp2=self.egp2)
        network_layer.start()
        ns.sim_run()
        assert network_layer.finished
        network_layer.stop()

    def test_create_and_keep(self):

        class NetworkLayer(self.MockNetworkLayer):

            def make_request(self):
                create_id = self.subprotocols["EGP1"].put(
                    request=ReqCreateAndKeep(remote_node_id=self.subprotocols["EGP2"].node.ID))
                assert create_id == 0
                return ResCreateAndKeep.__name__

            def check(self, responses):
                super().check(responses)
                for res in responses:
                    assert isinstance(res, ResCreateAndKeep)
                qubits = [egp.node.qmemory.peek(positions=0)[0] for egp in self.subprotocols.values()]
                assert ns.qubits.qubitapi.fidelity(
                    qubits, np.array([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])) == 1

        self.NetworkLayer = NetworkLayer

    def test_measure_directly_fixed(self):

        class NetworkLayer(self.MockNetworkLayer):

            def make_request(self):
                create_id = self.subprotocols["EGP1"].put(
                    request=ReqMeasureDirectly(remote_node_id=self.subprotocols["EGP2"].node.ID))
                assert create_id == 0
                return ResMeasureDirectly.__name__

            def check(self, responses):
                super().check(responses)
                for res in responses:
                    assert isinstance(res, ResMeasureDirectly)
                    assert res.measurement_outcome == 0
                    assert res.measurement_basis == MeasurementBasis.Z

        self.NetworkLayer = NetworkLayer

    def test_measure_directly_xz(self):

        class NetworkLayer(self.MockNetworkLayer):

            def make_request(self):
                create_id = self.subprotocols["EGP1"].put(
                    request=ReqMeasureDirectly(random_basis_local=RandomBasis.XZ,
                                               random_basis_remote=RandomBasis.XZ,
                                               remote_node_id=self.subprotocols["EGP2"].node.ID,
                                               probability_distribution_parameter_local_1=1,
                                               probability_distribution_parameter_remote_1=0))
                assert create_id == 0
                return ResMeasureDirectly.__name__

            def check(self, responses):
                super().check(responses)
                for res in responses:
                    assert isinstance(res, ResMeasureDirectly)
                    if res.remote_node_id == self.subprotocols["EGP1"].node.ID:
                        assert res.measurement_basis == MeasurementBasis.Z
                        assert res.measurement_outcome == 0
                    else:
                        assert res.measurement_basis == MeasurementBasis.X

        self.NetworkLayer = NetworkLayer

    def test_measure_directly_xyz(self):

        class NetworkLayer(self.MockNetworkLayer):

            def make_request(self):
                self.subprotocols["EGP1"].put(
                    request=ReqMeasureDirectly(random_basis_local=RandomBasis.XYZ,
                                               random_basis_remote=RandomBasis.XYZ,
                                               remote_node_id=self.subprotocols["EGP2"].node.ID,
                                               probability_distribution_parameter_local_1=1,
                                               probability_distribution_parameter_local_2=0,
                                               probability_distribution_parameter_remote_1=0,
                                               probability_distribution_parameter_remote_2=0))
                return ResMeasureDirectly.__name__

            def check(self, responses):
                super().check(responses)
                for res in responses:
                    assert isinstance(res, ResMeasureDirectly)
                    if res.remote_node_id == self.subprotocols["EGP1"].node.ID:
                        assert res.measurement_basis == MeasurementBasis.Z
                        assert res.measurement_outcome == 0
                    else:
                        assert res.measurement_basis == MeasurementBasis.X

        self.NetworkLayer = NetworkLayer

    def test_measure_directly_chsh(self):

        class NetworkLayer(self.MockNetworkLayer):

            def make_request(self):
                self.subprotocols["EGP1"].put(
                    request=ReqMeasureDirectly(random_basis_local=RandomBasis.CHSH,
                                               random_basis_remote=RandomBasis.CHSH,
                                               remote_node_id=self.subprotocols["EGP2"].node.ID,
                                               probability_distribution_parameter_local_1=1,
                                               probability_distribution_parameter_remote_1=0))
                return ResMeasureDirectly.__name__

            def check(self, responses):
                super().check(responses)
                for res in responses:
                    assert isinstance(res, ResMeasureDirectly)
                    if res.remote_node_id == self.subprotocols["EGP1"].node.ID:
                        assert res.measurement_basis == MeasurementBasis.ZMINUSX
                    else:
                        assert res.measurement_basis == MeasurementBasis.ZPLUSX

        self.NetworkLayer = NetworkLayer

    def test_not_receiving(self):

        class NetworkLayer(self.MockNetworkLayer):

            def make_request(self):
                create_ids = []
                self.subprotocols["EGP2"].put(request=ReqStopReceive(remote_node_id=self.subprotocols["EGP1"].node.ID))
                create_id = self.subprotocols["EGP1"].put(
                    request=ReqCreateAndKeep(remote_node_id=self.subprotocols["EGP2"].node.ID))
                create_ids.append(create_id)
                create_id = self.subprotocols["EGP2"].put(
                    request=ReqCreateAndKeep(remote_node_id=self.subprotocols["EGP1"].node.ID))
                create_ids.append(create_id)
                assert create_ids == [0, 0]
                return ResCreateAndKeep.__name__

            def check(self, responses):
                for res in responses:
                    assert isinstance(res, ResError)
                    assert res.error_code == ErrorCode.REJECTED

        self.NetworkLayer = NetworkLayer


if __name__ == "__main__":
    # logger.setLevel("WARNING")
    unittest.main()
