import unittest

import netsquid as ns
from netsquid.protocols.nodeprotocols import NodeProtocol
from netsquid.nodes.node import Node
from netsquid_physlayer.classical_connection import ClassicalConnection
from netsquid_driver.driver import Driver
from nlblueprint.control_layer.initiative_based_agreement_service import \
    InitiativeBasedAgreementServiceWithClassicalConnections, InitiativeBasedAgreementServiceMode, \
    InitiativeTakingAgreementServiceWithClassicalConnections, RespondingAgreementServiceWithClassicalConnections

from netsquid_driver.entanglement_agreement_service import EntanglementAgreementService, \
    ResEntanglementAgreementReached, ResEntanglementAgreementRejected, ReqEntanglementAgreement, \
    ReqEntanglementAgreementAbort
from netsquid_driver.message_handler_protocol import MessageHandlerProtocol


class ListenForAgreement(NodeProtocol):

    def __init__(self, node):
        super().__init__(node=node, name="listen_for_agreement")
        self.responses = []
        self.times = []

    def run(self):
        agreement_service = self.node.driver[EntanglementAgreementService]
        while True:
            reached = self.await_signal(sender=agreement_service,
                                        signal_label=ResEntanglementAgreementReached.__name__)
            rejected = self.await_signal(sender=agreement_service,
                                         signal_label=ResEntanglementAgreementRejected.__name__)
            evt_expr = reached | rejected
            yield evt_expr
            [evt] = evt_expr.triggered_events
            response = agreement_service.get_signal_by_event(evt)
            self.responses.append(response.result)
            self.times.append(ns.sim_time())


def configure_node(node):
    node.driver = Driver(f"driver_of_{node.name}")
    node.add_subcomponent(node.driver)
    message_handler_protocol = MessageHandlerProtocol(node=node,
                                                      name=f"message_handler_protocol_{node.name}")
    node.driver.add_service(MessageHandlerProtocol, message_handler_protocol)
    listen_for_agreement = ListenForAgreement(node)

    return listen_for_agreement


class NodeWithDriver(Node):
    def __init__(self, name):
        super().__init__(name)
        self.driver = None


class TestInitiativeBasedAgreementServiceTwoNodes(unittest.TestCase):

    def setUp(self):
        ns.sim_reset()
        self.node_initiative = NodeWithDriver("node_initiative")
        self.listen_for_agreement_initiative = configure_node(self.node_initiative)
        self.node_respond = NodeWithDriver("node_respond")
        self.listen_for_agreement_respond = configure_node(self.node_respond)
        self.node_initiative.connect_to(remote_node=self.node_respond,
                                        connection=ClassicalConnection(name="test_classical_connection",
                                                                       delay=50))

    def setup_without_modes(self):
        initiative_agreement_service = \
            InitiativeTakingAgreementServiceWithClassicalConnections(node=self.node_initiative,
                                                                     name=f"agreement_service_"
                                                                          f"{self.node_initiative.name}")
        self.node_initiative.driver.add_service(EntanglementAgreementService, initiative_agreement_service)
        respond_agreement_service = \
            RespondingAgreementServiceWithClassicalConnections(node=self.node_respond,
                                                               name=f"agreement_service_"
                                                                    f"{self.node_respond.name}")
        self.node_respond.driver.add_service(EntanglementAgreementService, respond_agreement_service)

        for node in [self.node_initiative, self.node_respond]:
            node.driver.start_all_services()
        for listen_for_agreement in [self.listen_for_agreement_initiative, self.listen_for_agreement_respond]:
            listen_for_agreement.start()

    def setup_with_modes(self):
        for node in [self.node_initiative, self.node_respond]:
            agreement_service = \
                InitiativeBasedAgreementServiceWithClassicalConnections(node=node,
                                                                        name=f"agreement_service_{node.name}")
            node.driver.add_service(EntanglementAgreementService, agreement_service)
        self.node_initiative.driver[EntanglementAgreementService].mode = \
            InitiativeBasedAgreementServiceMode.InitiativeTaking
        assert (self.node_initiative.driver[EntanglementAgreementService].mode
                == InitiativeBasedAgreementServiceMode.InitiativeTaking)
        self.node_respond.driver[EntanglementAgreementService].mode = InitiativeBasedAgreementServiceMode.Responding
        assert (self.node_respond.driver[EntanglementAgreementService].mode
               == InitiativeBasedAgreementServiceMode.Responding)

        for node in [self.node_initiative, self.node_respond]:
            node.driver.start_all_services()
        for listen_for_agreement in [self.listen_for_agreement_initiative, self.listen_for_agreement_respond]:
            listen_for_agreement.start()

    def test_get_agreement_without_modes(self):
        self.setup_without_modes()
        self.try_to_get_agreement()

    def test_get_agreement_with_modes(self):
        self.setup_with_modes()
        self.try_to_get_agreement()

    def try_to_get_agreement(self, connection_ids=[0]):
        # request for responder to agree with initiative node
        # should be denied, since there has been no initiative yet
        requests = [ReqEntanglementAgreement(remote_node_id=self.node_initiative.ID, connection_id=connection_id)
                    for connection_id in connection_ids]
        for req in requests:
            self.node_respond.driver[EntanglementAgreementService].put(req)
        ns.sim_run()
        assert ns.sim_time() == 0
        assert self.listen_for_agreement_respond.responses == \
               [ResEntanglementAgreementRejected(remote_node_id=self.node_initiative.ID,
                                                 connection_id=connection_id) for connection_id in
                reversed(connection_ids)]

        # now we request the node with initiative to take the initiative
        # this will have no result until the responder confirms the agreement,
        # which it only does when a request for agreement is issued at the responder
        requests = [ReqEntanglementAgreement(remote_node_id=self.node_respond.ID, connection_id=connection_id)
                    for connection_id in connection_ids]
        for req in requests:
            self.node_initiative.driver[EntanglementAgreementService].put(req)
        ns.sim_run()
        assert ns.sim_time() == 50
        assert not self.listen_for_agreement_initiative.responses
        assert len(self.listen_for_agreement_respond.responses) == len(connection_ids)

        # check that if the wrong request is put at responder, this has no effect
        nonsense_node_id = self.node_initiative.ID + 100
        requests = [ReqEntanglementAgreement(remote_node_id=nonsense_node_id, connection_id=connection_id)
                    for connection_id in connection_ids]
        for req in requests:
            self.node_respond.driver[EntanglementAgreementService].put(req)
        ns.sim_run()
        assert ns.sim_time() == 50
        assert not self.listen_for_agreement_initiative.responses
        expected_responses = [ResEntanglementAgreementRejected(remote_node_id=self.node_initiative.ID,
                                                               connection_id=cid)
                              for cid in reversed(connection_ids)] + \
                             [ResEntanglementAgreementRejected(remote_node_id=nonsense_node_id,
                                                               connection_id=cid) for cid in reversed(connection_ids)]
        assert self.listen_for_agreement_respond.responses == expected_responses
        assert self.listen_for_agreement_respond.times == [0 for _ in connection_ids] + [50 for _ in connection_ids]

        # now put the proper request at the responder, which should lead to confirming
        # both should now give a response indicating that agreement has been reached
        # it is important that these responses are issued simultaneously!
        requests = [ReqEntanglementAgreement(remote_node_id=self.node_initiative.ID, connection_id=connection_id)
                    for connection_id in connection_ids]
        for req in requests:
            self.node_respond.driver[EntanglementAgreementService].put(req)
        ns.sim_run()
        assert ns.sim_time() == 100
        assert self.listen_for_agreement_initiative.responses == \
               [ResEntanglementAgreementReached(remote_node_id=self.node_respond.ID, connection_id=connection_id)
                for connection_id in reversed(connection_ids)]
        assert self.listen_for_agreement_initiative.times == [100 for _ in reversed(connection_ids)]
        assert self.listen_for_agreement_respond.responses == \
               [ResEntanglementAgreementRejected(remote_node_id=self.node_initiative.ID,
                                                 connection_id=connection_id) for
                connection_id in reversed(connection_ids)] + \
               [ResEntanglementAgreementRejected(remote_node_id=nonsense_node_id,
                                                 connection_id=connection_id) for
                connection_id in reversed(connection_ids)] + \
               [ResEntanglementAgreementReached(remote_node_id=self.node_initiative.ID,
                                                connection_id=connection_id) for
                connection_id in reversed(connection_ids)]
        assert self.listen_for_agreement_respond.times == [0 for _ in reversed(connection_ids)] +\
               [50 for _ in reversed(connection_ids)] + [100 for _ in reversed(connection_ids)]

        # check that it also works twice in a row
        for connection_id in connection_ids:
            self.node_initiative.driver[EntanglementAgreementService].put(
                ReqEntanglementAgreement(self.node_respond.ID, connection_id=connection_id))
        ns.sim_run(duration=1000)
        for connection_id in connection_ids:
            self.node_respond.driver[EntanglementAgreementService].put(
                ReqEntanglementAgreement(self.node_initiative.ID, connection_id=connection_id))
        ns.sim_run()
        assert len(self.listen_for_agreement_initiative.responses) == 2 * len(connection_ids)
        assert (self.listen_for_agreement_initiative.responses[-1] ==
                ResEntanglementAgreementReached(self.node_respond.ID, connection_id=connection_ids[0]))
        assert self.listen_for_agreement_initiative.times[-1] == 1150
        assert len(self.listen_for_agreement_respond.responses) == 4 * len(connection_ids)
        assert (self.listen_for_agreement_respond.responses[-1] ==
                ResEntanglementAgreementReached(self.node_initiative.ID, connection_id=connection_ids[0]))
        assert self.listen_for_agreement_respond.times[-1] == 1150

    def test_requests_over_multiple_connections(self):
        self.setup_without_modes()
        self.try_to_get_agreement(connection_ids=[1, 2, 3, 5, 42])

    def test_two_simultaneous_requests_initiative(self):
        self.setup_without_modes()
        self.node_initiative.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_respond.ID,
                                                                                               connection_id=0))
        with self.assertRaises(RuntimeError):
            self.node_initiative.driver[EntanglementAgreementService].put(
                ReqEntanglementAgreement(self.node_respond.ID, connection_id=0))
        ns.sim_run(duration=25)
        with self.assertRaises(RuntimeError):
            self.node_initiative.driver[EntanglementAgreementService].put(
                ReqEntanglementAgreement(self.node_respond.ID, connection_id=0))

    def test_two_simultaneous_requests_responder(self):
        self.setup_without_modes()
        self.node_initiative.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_respond.ID,
                                                                                               connection_id=0))
        ns.sim_run()
        self.node_respond.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_initiative.ID,
                                                                                            connection_id=0))
        ns.sim_run(duration=20)
        assert not self.listen_for_agreement_initiative.responses
        assert not self.listen_for_agreement_respond.responses
        with self.assertRaises(RuntimeError):
            self.node_respond.driver[EntanglementAgreementService].put(
                ReqEntanglementAgreement(self.node_initiative.ID, connection_id=0))

    def test_abort_responder(self):
        """Test that aborting at responder while confirmation is underway, no AgreementReached response is sent.
        It is sent at the initiative node though, since it will receive the confirmation and it cannot be helped.
        """
        self.setup_without_modes()
        self.node_initiative.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_respond.ID,
                                                                                               connection_id=0))
        ns.sim_run()
        self.node_respond.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_initiative.ID,
                                                                                            connection_id=0))
        ns.sim_run(duration=20)
        self.node_respond.driver[EntanglementAgreementService]\
            .put(ReqEntanglementAgreementAbort(self.node_initiative.ID, connection_id=0))
        ns.sim_run()
        assert not self.listen_for_agreement_respond.responses
        assert self.listen_for_agreement_initiative.responses == [ResEntanglementAgreementReached(self.node_respond.ID,
                                                                                                  connection_id=0)]

    def test_abort_initiative_in_time_to_stop_confirmation(self):
        """Test that aborting at initiative node such that the RETRACT_ASK_AGREEMENT message is received
         before a confirmation message is sent by responder completely undoes the original request."""
        self.setup_without_modes()
        self.node_initiative.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_respond.ID,
                                                                                               connection_id=0))
        ns.sim_run()
        self.node_initiative.driver[EntanglementAgreementService]\
            .put(ReqEntanglementAgreementAbort(self.node_respond.ID, connection_id=0))
        ns.sim_run()
        self.node_respond.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_initiative.ID,
                                                                                            connection_id=0))
        ns.sim_run()
        assert not self.listen_for_agreement_initiative.responses
        assert (self.listen_for_agreement_respond.responses ==
                [ResEntanglementAgreementRejected(self.node_initiative.ID, connection_id=0)])

    def test_abort_initiative_while_confirmation_under_way(self):
        """Test that aborting at initiative node after a confirmation message is sent (but before it is received)
        results in no AgreementReached response at the initiative node. There is such a response at the responder
        node though, as it's too late to let it know about the abortion."""
        self.setup_without_modes()
        self.node_initiative.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_respond.ID,
                                                                                               connection_id=0))
        ns.sim_run()
        self.node_respond.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_initiative.ID,
                                                                                            connection_id=0))
        ns.sim_run(duration=20)
        self.node_initiative.driver[EntanglementAgreementService] \
            .put(ReqEntanglementAgreementAbort(self.node_respond.ID, connection_id=0))
        ns.sim_run()
        assert not self.listen_for_agreement_initiative.responses
        assert self.listen_for_agreement_respond.responses == [ResEntanglementAgreementReached(self.node_initiative.ID,
                                                                                               connection_id=0)]

    def test_abort_initiative_in_time_to_stop_agreement_but_too_late_to_stop_confirmation(self):
        """Test that aborting the initiative node at such a time that the RETRACT_ASK_AGREEMENT message is received
        by the responder node after it has sent a confirmation message, but before the confirmation message arrives
        at the initiative node (and the responder node sends an EntanglementAgreementReached response),
        results in no response at either node. Also check that subsequent requests at responder node are rejected."""
        self.setup_without_modes()
        self.node_initiative.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_respond.ID,
                                                                                               connection_id=0))
        ns.sim_run()
        self.node_initiative.driver[EntanglementAgreementService]\
            .put(ReqEntanglementAgreementAbort(self.node_respond.ID, connection_id=0))
        ns.sim_run(duration=20)
        self.node_respond.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_initiative.ID,
                                                                                            connection_id=0))
        ns.sim_run()  # now, the confirmation and retract_ask messages cross each other
        assert not self.listen_for_agreement_initiative.responses
        assert not self.listen_for_agreement_respond.responses
        self.node_respond.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.node_initiative.ID,
                                                                                            connection_id=0))
        ns.sim_run()
        assert (self.listen_for_agreement_respond.responses ==
                [ResEntanglementAgreementRejected(self.node_initiative.ID, connection_id=0)])
        assert not self.listen_for_agreement_initiative.responses


class TestInitiativeBasedAgreementServiceThreeNodes(unittest.TestCase):

    delay = 50

    def setUp(self):
        ns.sim_reset()
        self.nodes = [NodeWithDriver(f"node_{i}") for i in range(3)]
        self.listening_protocols = [configure_node(node) for node in self.nodes]
        for i in range(2):
            self.nodes[i].connect_to(remote_node=self.nodes[i + 1],
                                     connection=ClassicalConnection(name=f"classical_connection_{i}", delay=self.delay))

    def start_protocols(self):
        for node in self.nodes:
            node.driver.start_all_services()
        for listening_protocol in self.listening_protocols:
            listening_protocol.start()

    def test_respond_initiative_respond(self):
        # setup
        for node in [self.nodes[0], self.nodes[2]]:
            node.driver.add_service(EntanglementAgreementService,
                                    RespondingAgreementServiceWithClassicalConnections(node=node))
        self.nodes[1].driver.add_service(EntanglementAgreementService,
                                         InitiativeTakingAgreementServiceWithClassicalConnections(node=self.nodes[1]))
        self.start_protocols()

        # check requests are rejected at responders
        for node in [self.nodes[0], self.nodes[2]]:
            node.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.nodes[1].ID, connection_id=0))
        ns.sim_run()
        for listening_protocol in [self.listening_protocols[0], self.listening_protocols[2]]:
            assert listening_protocol.responses == [ResEntanglementAgreementRejected(self.nodes[1].ID, connection_id=0)]
            assert listening_protocol.times == [0]

        # now try to get agreement with both simultaneously
        for node in [self.nodes[0], self.nodes[2]]:
            self.nodes[1].driver[EntanglementAgreementService].put(ReqEntanglementAgreement(node.ID, connection_id=0))
        ns.sim_run(duration=1000)
        assert not self.listening_protocols[1].responses
        for node in [self.nodes[0], self.nodes[2]]:
            node.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.nodes[1].ID, connection_id=0))
        ns.sim_run()
        for listening_protocol in [self.listening_protocols[0], self.listening_protocols[2]]:
            assert len(listening_protocol.responses) == 2
            assert listening_protocol.responses[-1] == ResEntanglementAgreementReached(self.nodes[1].ID,
                                                                                       connection_id=0)
            assert listening_protocol.times[-1] == 1000 + self.delay
        assert len(self.listening_protocols[1].responses) == 2
        assert ResEntanglementAgreementReached(self.nodes[0].ID, 0) in self.listening_protocols[1].responses
        assert ResEntanglementAgreementReached(self.nodes[2].ID, 0) in self.listening_protocols[1].responses
        assert self.listening_protocols[1].times == [1000 + self.delay] * 2

        # now try to get agreement with both sequentially
        self.nodes[1].driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.nodes[0].ID,
                                                                                        connection_id=0))
        ns.sim_run()
        self.nodes[0].driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.nodes[1].ID,
                                                                                        connection_id=0))
        ns.sim_run()
        assert len(self.listening_protocols[1].responses) == 3
        assert len(self.listening_protocols[0].responses) == 3
        assert len(self.listening_protocols[2].responses) == 2
        self.nodes[1].driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.nodes[2].ID,
                                                                                        connection_id=0))
        ns.sim_run()
        self.nodes[2].driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.nodes[1].ID,
                                                                                        connection_id=0))
        ns.sim_run()
        assert len(self.listening_protocols[1].responses) == 4
        assert len(self.listening_protocols[0].responses) == 3
        assert len(self.listening_protocols[2].responses) == 3

    def test_initiative_respond_initiative(self):
        # setup
        for node in [self.nodes[0], self.nodes[2]]:
            node.driver.add_service(EntanglementAgreementService,
                                    InitiativeTakingAgreementServiceWithClassicalConnections(node=node))
        self.nodes[1].driver.add_service(EntanglementAgreementService,
                                         RespondingAgreementServiceWithClassicalConnections(node=self.nodes[1]))
        self.start_protocols()

        # take initiative on both sides at the same time
        for node in [self.nodes[0], self.nodes[2]]:
            node.driver[EntanglementAgreementService].put(ReqEntanglementAgreement(self.nodes[1].ID, connection_id=0))
        ns.sim_run()
        for node in [self.nodes[0], self.nodes[2]]:
            self.nodes[1].driver[EntanglementAgreementService].put(ReqEntanglementAgreement(node.ID, connection_id=0))
        ns.sim_run()
        for listening_protocol in [self.listening_protocols[0], self.listening_protocols[2]]:
            assert listening_protocol.responses == [ResEntanglementAgreementReached(self.nodes[1].ID, connection_id=0)]
            assert listening_protocol.times[0] == self.delay * 2
        assert len(self.listening_protocols[1].responses) == 2
        assert ResEntanglementAgreementReached(self.nodes[0].ID, 0) in self.listening_protocols[1].responses
        assert ResEntanglementAgreementReached(self.nodes[2].ID, 0) in self.listening_protocols[1].responses
        assert self.listening_protocols[1].times == [self.delay * 2] * 2
