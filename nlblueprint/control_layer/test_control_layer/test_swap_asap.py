from netsquid.protocols.nodeprotocols import NodeProtocol
from netsquid import BellIndex
from netsquid.components.instructions import INSTR_MEASURE_BELL
from netsquid.components.qprocessor import QuantumProcessor
import netsquid as ns
from netsquid.nodes import Node
from netsquid_driver.driver import Driver
from netsquid_entanglementtracker.local_link_tracker import LocalLinkTracker
from netsquid_entanglementtracker.cutoff_service import CutoffService
from netsquid_entanglementtracker.cutoff_timer import CutoffTimer
from netsquid_driver.entanglement_service import EntanglementService, ResStateReady, \
    ResEntanglementFail, ResEntanglementSuccess
from netsquid_entanglementtracker.entanglement_tracker_service import EntanglementTrackerService
from netsquid_driver.measurement_services import SwapService, ResSwap
from netsquid_driver.swap_asap_service import ReqSwapASAP, ReqSwapASAPAbort, ResSwapASAPFinished
from nlblueprint.control_layer.swap_asap import SwapASAP, SwapASAPOneSequentialRepeater
from netsquid_physlayer.heralded_connection import HeraldedConnection
import netsquid.qubits.qubitapi as qapi
import logging


class SimpleEntanglement(EntanglementService):
    """Minimal implementation of :class:`EntanglementService` for testing purposes.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    interval_duration_0 : float
        Time between all signals (ready - fail - ready - success) on first port.
        Total duration of entanglement generation on the first port is `4 * interval_duration_0`.
    interval_duration_1 : float
        Time between all signals (ready - fail - ready - success) on second port.
        Total duration of entanglement generation on the second port is `4 * interval_duration_1`.

    """

    class SimpleEntanglementSubprotocol(NodeProtocol):
        """Perform mock entanglement generation successfully once, after one failure.

        Parameters
        ----------
        node : :class:`~netsquid.nodes.node.Node`
            The node this protocol is running on.
        superprotocol : :class:`SimpleEntanglement`
            Protocol this is a subprotocol of.
        port_name : str
            Name of port to generate entanglement over in a mock way.
        mem_pos : int
            Memory position to deliver the qubit.
        interval_duration : float
            Time between all signals (ready - fail - ready - success) on port `port_name`.
            Total duration of entanglement generation on the second port is `4 * interval_duration`.

        """

        def __init__(self, node, superprotocol, port_name, mem_pos, interval_duration):
            super().__init__(node=node, name="simple_entanglement_subprotocol")
            self.superprotocol = superprotocol
            self.port_name = port_name
            self.mem_pos = mem_pos
            self.interval_duration = interval_duration

        def run(self):
            # state ready
            yield self.await_timer(duration=self.interval_duration)
            self.superprotocol.send_response(ResStateReady(port_name=self.port_name,
                                                           mem_pos=self.mem_pos))
            # this attempt failed
            yield self.await_timer(duration=self.interval_duration)
            self.superprotocol.send_response(ResEntanglementFail(port_name=self.port_name,
                                                                 mem_pos=self.mem_pos))
            # state ready of next attempt
            yield self.await_timer(duration=self.interval_duration)
            self.superprotocol.send_response(ResStateReady(port_name=self.port_name,
                                             mem_pos=self.mem_pos))
            # this attempt succeeded!
            yield self.await_timer(duration=self.interval_duration)
            # bell_index depends on port name to check if we identified "upstream" correctly
            bell_index = BellIndex(1) if self.port_name == SwapASAP.ENT_PORT_NAMES[0] else BellIndex(2)
            self.superprotocol.send_response(ResEntanglementSuccess(remote_node_id=100,
                                                                    port_name=self.port_name,
                                                                    mem_pos=self.mem_pos,
                                                                    bell_index=bell_index))
            qubit = qapi.create_qubits(1)
            self.node.qmemory.put(qubit, positions=self.mem_pos)

    def __init__(self, node, interval_duration_0=100, interval_duration_1=160):
        super().__init__(node=node, name="simple_entanglement_protocol")
        self.aborted = []
        self.interval_duration_0 = interval_duration_0
        self.interval_duration_1 = interval_duration_1
        self._add_subprotocols()

    def _add_subprotocols(self):
        mem_pos = 0
        self.port_name_by_mem_pos = {}
        for port_name, interval_duration in zip(SwapASAP.ENT_PORT_NAMES, [self.interval_duration_0,
                                                                          self.interval_duration_1]):
            self.add_subprotocol(self.SimpleEntanglementSubprotocol(node=self.node,
                                                                    superprotocol=self,
                                                                    port_name=port_name,
                                                                    mem_pos=mem_pos,
                                                                    interval_duration=interval_duration),
                                 name=port_name)
            self.port_name_by_mem_pos[mem_pos] = port_name
            mem_pos += 1

    def generate_entanglement(self, req):
        super().generate_entanglement(req)
        subprotocol = self.subprotocols[self.port_name_by_mem_pos[req.mem_pos]]
        subprotocol.start()

    def abort(self, req):
        port_name = req.port_name
        self.aborted.append(port_name)
        self.subprotocols[port_name].stop()


class SimpleEntanglementOneParallelLink(SimpleEntanglement):
    """Minimal implementation of :class:`EntanglementService` for testing purposes.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    interval_duration_0 : float
        Time between all signals (ready - fail - ready - success) on first port.
        Total duration of entanglement generation on the first port is `4 * interval_duration_0`.
    interval_duration_1 : float
        Time between all signals (ready - fail - ready - success) on second port.
        Total duration of entanglement generation on the second port is `4 * interval_duration_1`.

    """
    def __init__(self, node, interval_duration_0=100, interval_duration_1=160):
        super().__init__(node=node, interval_duration_0=interval_duration_0, interval_duration_1=interval_duration_1)
        self.num_parallel_attempts = 1

    class SimpleEntanglementSubprotocol(NodeProtocol):
        """Perform mock entanglement generation successfully once, after one failure.

        Parameters
        ----------
        node : :class:`~netsquid.nodes.node.Node`
            The node this protocol is running on.
        superprotocol : :class:`SimpleEntanglement`
            Protocol this is a subprotocol of.
        port_name : str
            Name of port to generate entanglement over in a mock way.
        mem_pos : int
            Memory position to deliver the qubit.
        interval_duration : float
            Time between all signals (ready - fail - ready - success) on port `port_name`.
            Total duration of entanglement generation on the second port is `4 * interval_duration`.

        """

        def __init__(self, node, superprotocol, port_name, mem_pos, interval_duration):
            super().__init__(node=node, name="simple_entanglement_subprotocol")
            self.superprotocol = superprotocol
            self.port_name = port_name
            self.mem_pos = mem_pos
            self.interval_duration = interval_duration

        def run(self):
            yield self.await_timer(duration=self.interval_duration)
            # bell_index depends on port name to check if we identified "upstream" correctly
            bell_index = BellIndex(1) if self.port_name == SwapASAP.ENT_PORT_NAMES[0] else BellIndex(2)
            self.superprotocol.send_response(ResEntanglementSuccess(remote_node_id=100,
                                                                    port_name=self.port_name,
                                                                    mem_pos=self.mem_pos,
                                                                    bell_index=bell_index))
            qubit = qapi.create_qubits(1)
            self.node.qmemory.put(qubit, positions=self.mem_pos)


class SimpleSwap(SwapService):
    """Minimal implementation of :class:`SwapService` for testing purposes."""

    def swap(self, req):
        super().swap(req)
        results = self.node.qmemory.execute_instruction(INSTR_MEASURE_BELL)
        res = ResSwap(outcome=results[0]["instr"][0])
        self.send_response(res)
        self.node.driver[EntanglementTrackerService].register_local_swap_mem_pos(mem_pos_1=req.mem_pos_1,
                                                                                 mem_pos_2=req.mem_pos_2,
                                                                                 bell_index=res.outcome)


class SwapASAPTestProtocol(NodeProtocol):
    """Protocol meant for testing :class:`SwapASAP`."""

    def __init__(self, node, swap_asap, interval_duration_0=100, interval_duration_1=160):
        super().__init__(node=node, name="swap_asap_test_protocol")
        self.swap_asap = swap_asap
        self.finished = False
        self.finish_messages = 0
        self.req_id = 10
        self.num = 4
        self.interval_duration_0 = interval_duration_0
        self.interval_duration_1 = interval_duration_1
        self.finished_swap_asap_4_times = False
        self.finished_discard = False
        self.finished_abort = False

    def run(self):
        for port in self.node.ports.values():
            port.bind_output_handler(self.listen_at_ports)
        yield from self.test_swap_asap_4_times()
        self.finish_messages = 0
        yield from self.test_discard()
        self.finish_messages = 0
        yield from self.test_abort()  # Note: any test that takes place after test_abort fails, but not sure why
        self.finished = True

    def test_swap_asap_4_times(self):
        """Test if SwapASAP generates and swaps entanglement as expected in case 4 swaps are requested."""
        start_time = ns.sim_time()
        request = ReqSwapASAP(request_id=self.req_id, num=self.num, cutoff_time=0)
        request.port_name_where_request_arrived = SwapASAP.COMM_PORT_NAMES[0]
        self.swap_asap.put(request)
        yield self.await_signal(sender=self.swap_asap,
                                signal_label=ResSwapASAPFinished.__name__)
        res = self.swap_asap.get_signal_result(ResSwapASAPFinished.__name__)
        assert res.request_id == self.req_id
        assert len(res.downstream_bell_indices) == self.num
        assert len(res.swap_bell_indices) == self.num
        assert res.upstream_bell_indices[0] == BellIndex(1)
        assert res.downstream_bell_indices[0] == BellIndex(2)
        assert self.finish_messages == 2  # one for each port
        assert ns.sim_time() == start_time + max(self.interval_duration_0, self.interval_duration_1) * 4 * 4
        self.finished_swap_asap_4_times = True
        # 4 responses per entanglement, interval_duration per response, 4 x generated, takes as long as the slowest one

    def test_abort(self):
        """Test what happens if SwapASAP is aborted."""
        request = ReqSwapASAP(request_id=self.req_id + 1, num=self.num, cutoff_time=0)
        request.port_name_where_request_arrived = SwapASAP.COMM_PORT_NAMES[1]
        self.swap_asap.put(request)
        yield self.await_timer(duration=(max(self.interval_duration_0, self.interval_duration_1) + 1) * 4 * 2)
        # enough time for one entanglement to finish 2 times, the other 3)
        self.swap_asap.put(ReqSwapASAPAbort(request_id=self.req_id + 1))
        # check that everything was properly reset
        assert self.swap_asap.request_id is None
        assert self.swap_asap.num is None
        assert self.swap_asap.cutoff_time is None
        assert len(self.swap_asap.swap_bell_indices) == 0
        assert len(self.swap_asap.goodness) == 0
        assert self.swap_asap.entanglement_bell_indices == {direction: [] for direction in self.swap_asap.DIRECTIONS}
        assert self.node.driver[EntanglementService].aborted == SwapASAP.ENT_PORT_NAMES
        self.finished_abort = True

    def test_discard(self):
        """Test what happens if entanglement is discarded while running swap-asap."""
        start_time = ns.sim_time()
        # test entanglement being first generated, then discarded
        request = ReqSwapASAP(request_id=self.req_id + 2, num=1, cutoff_time=0)
        request.port_name_where_request_arrived = SwapASAP.COMM_PORT_NAMES[0]
        self.swap_asap.put(request)
        # wait for entanglement to be generated on the faster link
        yield self.await_timer(duration=4 * max(self.interval_duration_0, self.interval_duration_1) - 1)
        # discard entanglement
        ent_tracker = self.node.driver[EntanglementTrackerService]
        num_finished_links = 0
        for mem_pos in range(2):
            if ent_tracker.get_link(mem_pos) is not None:
                num_finished_links += 1
                ent_tracker.register_local_discard_mem_pos(mem_pos=mem_pos)
        assert num_finished_links == 1
        # check protocol is still able to finish
        yield self.await_signal(sender=self.swap_asap,
                                signal_label=ResSwapASAPFinished.__name__)
        res = self.swap_asap.get_signal_result(ResSwapASAPFinished.__name__)
        assert res.request_id == self.req_id + 2
        assert len(res.downstream_bell_indices) == 1
        assert len(res.swap_bell_indices) == 1
        assert res.upstream_bell_indices[0] == BellIndex(1)
        assert res.downstream_bell_indices[0] == BellIndex(2)
        assert self.finish_messages == 2  # one for one port
        assert ns.sim_time() == start_time + 4 * self.interval_duration_0 + 4 * self.interval_duration_1 - 1
        # 4 * max(intervals) - 1 + 4 * min(intervals) (due to regenerating the shorter one) (4 intervals = 1 link)
        self.finished_discard = True

    def listen_at_ports(self, message):
        assert isinstance(message.items[0], ResSwapASAPFinished)
        assert message.meta["type"] == "repeater_response"
        assert message.meta["sender_id"] == self.node.ID
        assert message.meta["forward"] == "chain"
        self.finish_messages += 1


class SwapASAPOneRepeaterSequentialTestProtocol(NodeProtocol):
    """Protocol meant for testing :class:`SwapASAPOneSequentialRepeater`."""

    def __init__(self, node, swap_asap, interval_duration_0, interval_duration_1,
                 length_connection_1=25, length_connection_2=45):
        super().__init__(node=node, name="swap_asap_test_protocol")
        self.swap_asap = swap_asap
        self.finished = False
        self.finish_messages = 0
        self.req_id = 10
        self.num = 4
        self.length_connection_1 = length_connection_1
        self.length_connection_2 = length_connection_2
        self.interval_duration_0 = interval_duration_0
        self.interval_duration_1 = interval_duration_1
        self.finished_start_on_long_side = False
        self.finished_generate_on_short_side_after_long = False
        self.finished_discard_goes_back_to_longest = False

    def run(self):
        yield from self.test_start_on_long_side()
        yield from self.test_shortest_generated_after()
        yield from self.test_discard_goes_back_to_longest()
        yield from self.test_swap_asap_4_times()
        self.finished = True

    def test_start_on_long_side(self):
        """Test that entanglement generation begins on the longest side."""
        request = ReqSwapASAP(request_id=self.req_id, num=1, cutoff_time=0)
        request.port_name_where_request_arrived = SwapASAP.COMM_PORT_NAMES[0]
        self.swap_asap.put(request)
        yield self.await_timer(duration=1)
        assert self.swap_asap._port_of_longest_side == "ENT_B"
        assert self.swap_asap._get_subprot_by_port_name("ENT_B").generating_entanglement
        self.finished_start_on_long_side = True

    def test_shortest_generated_after(self):
        """Test that after enough time elapsed for entanglement generation on the long side to finish, entanglement is
        being generated on the shortest side."""
        yield self.await_timer(duration=self.interval_duration_1)
        assert self.swap_asap._get_subprot_by_port_name(self.swap_asap._port_of_shortest_side).generating_entanglement
        request = ReqSwapASAPAbort(request_id=self.req_id)
        self.swap_asap.put(request)
        self.swap_asap._set_memory_positions_unused()
        self.finished_generate_on_short_side_after_long = True

    def test_discard_goes_back_to_longest(self):
        """Test after we discard entanglement on the long side, we stop generating on short side and begin on long."""
        yield self.await_timer(duration=self.interval_duration_0)
        request = ReqSwapASAP(request_id=self.req_id + 1, num=1)
        request.port_name_where_request_arrived = SwapASAP.COMM_PORT_NAMES[0]
        self.swap_asap.put(request)
        yield self.await_timer(duration=self.interval_duration_1 + 1)
        assert self.swap_asap._get_subprot_by_port_name(self.swap_asap._port_of_shortest_side).generating_entanglement
        # 26 = cutoff + 1 so we should have discarded
        yield self.await_timer(duration=26)
        assert not self.swap_asap._get_subprot_by_port_name(
            self.swap_asap._port_of_shortest_side).generating_entanglement
        assert self.swap_asap._get_subprot_by_port_name(self.swap_asap._port_of_longest_side).generating_entanglement
        request = ReqSwapASAPAbort(request_id=self.req_id + 1)
        self.swap_asap.put(request)
        self.finished_discard_goes_back_to_longest = True

    def test_swap_asap_4_times(self):
        """Test if SwapASAP generates and swaps entanglement as expected in case 4 swaps are requested."""
        # Change cutoff time so that this can actually conclude
        self.node.driver[CutoffService]._cutoff_time = 100
        start_time = ns.sim_time()
        request = ReqSwapASAP(request_id=self.req_id + 2, num=self.num, cutoff_time=0)
        request.port_name_where_request_arrived = SwapASAP.COMM_PORT_NAMES[0]
        self.swap_asap.put(request)
        yield self.await_signal(sender=self.swap_asap,
                                signal_label=ResSwapASAPFinished.__name__)
        res = self.swap_asap.get_signal_result(ResSwapASAPFinished.__name__)
        assert res.request_id == self.req_id + 2
        assert len(res.downstream_bell_indices) == self.num
        assert len(res.swap_bell_indices) == self.num
        assert res.upstream_bell_indices[0] == BellIndex(1)
        assert res.downstream_bell_indices[0] == BellIndex(2)
        assert ns.sim_time() == start_time + (self.interval_duration_0 + self.interval_duration_1) * 4
        self.finished_swap_asap_4_times = True


def test_swap_asap(interval_duration_0=100, interval_duration_1=160):

    ns.logger.setLevel(logging.DEBUG)
    ns.sim_reset()
    port_names = SwapASAP.COMM_PORT_NAMES + SwapASAP.ENT_PORT_NAMES
    node = Node("test_swap_asap_node", port_names=port_names)
    node.driver = Driver("test_swap_asap_driver")
    node.add_subcomponent(node.driver)
    node.driver.add_service(EntanglementService, SimpleEntanglement(node=node,
                                                                    interval_duration_0=interval_duration_0,
                                                                    interval_duration_1=interval_duration_1))
    node.driver.add_service(EntanglementTrackerService, LocalLinkTracker(node=node))
    node.driver.add_service(SwapService, SimpleSwap(node=node))
    node.qmemory = QuantumProcessor(name="test_swap_asap_processor", num_positions=2, fallback_to_nonphysical=True)
    swap_asap = SwapASAP(node=node, name="test_swap_asap", broadcast_responses=True)
    test_protocol = SwapASAPTestProtocol(node=node,
                                         swap_asap=swap_asap,
                                         interval_duration_0=interval_duration_0,
                                         interval_duration_1=interval_duration_1)
    test_protocol.start()
    ns.sim_run()
    assert test_protocol.finished_swap_asap_4_times
    assert test_protocol.finished_discard
    assert test_protocol.finished_abort
    assert test_protocol.finished


def test_swap_asap_sequential_one_repeater():
    ns.logger.setLevel(logging.DEBUG)
    ns.sim_reset()
    port_names = SwapASAP.COMM_PORT_NAMES + SwapASAP.ENT_PORT_NAMES

    node_A = Node("mock_node_A", port_names=["B, ENT_B"])
    node_B = Node("mock_node_B", port_names=["A, ENT_A"])

    interval_duration_0 = 30
    interval_duration_1 = 20

    connection_AR = HeraldedConnection(name="mock_connection_AR", length_A=10, length_B=15)
    connection_RB = HeraldedConnection(name="mock_connection_RB", length_A=20, length_B=25)

    repeater = Node("test_swap_asap_node", port_names=port_names)
    repeater.driver = Driver("test_swap_asap_driver")
    repeater.add_subcomponent(repeater.driver)
    repeater.driver.add_service(
        EntanglementService, SimpleEntanglementOneParallelLink(node=repeater,
                                                               interval_duration_0=interval_duration_0,
                                                               interval_duration_1=interval_duration_1))
    repeater.driver.add_service(EntanglementTrackerService, LocalLinkTracker(node=repeater))
    repeater.driver.add_service(CutoffService, CutoffTimer(node=repeater, cutoff_time=25))
    repeater.driver.add_service(SwapService, SimpleSwap(node=repeater))
    repeater.qmemory = QuantumProcessor(name="test_swap_asap_processor", num_positions=2, fallback_to_nonphysical=True)
    swap_asap = SwapASAPOneSequentialRepeater(node=repeater, name="test_swap_asap", broadcast_responses=True)
    repeater.driver.start_all_services()
    node_A.connect_to(remote_node=repeater,
                      connection=connection_AR,
                      label='entanglement_connection',
                      local_port_name='ENT_B',
                      remote_port_name='ENT_A')
    node_B.connect_to(remote_node=repeater,
                      connection=connection_RB,
                      label='entanglement_connection',
                      local_port_name='ENT_A',
                      remote_port_name='ENT_B')

    test_protocol = SwapASAPOneRepeaterSequentialTestProtocol(node=repeater,
                                                              swap_asap=swap_asap,
                                                              interval_duration_0=interval_duration_0,
                                                              interval_duration_1=interval_duration_1)
    test_protocol.start()
    ns.sim_run()

    assert test_protocol.finished_start_on_long_side
    assert test_protocol.finished_generate_on_short_side_after_long
    assert test_protocol.finished_discard_goes_back_to_longest
    assert test_protocol.finished_swap_asap_4_times
    assert test_protocol.finished
