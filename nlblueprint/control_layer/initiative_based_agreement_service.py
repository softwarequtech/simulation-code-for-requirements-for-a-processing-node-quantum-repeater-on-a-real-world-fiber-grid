from netsquid.protocols import NodeProtocol
from abc import abstractmethod, ABCMeta
from netsquid_driver.entanglement_agreement_service import EntanglementAgreementService, \
    EntanglementAgreementServiceWithMessages, ResEntanglementAgreementReached, ResEntanglementAgreementRejected, \
    EntanglementAgreementServiceWithClassicalConnections, signal_entanglement_agreement_status_change, \
    ReqEntanglementAgreementAbort
from enum import Enum
from netsquid.components.component import Message


ASK_AGREEMENT = "We are going to generate entanglement, or else..."

CONFIRM_AGREEMENT = "Yes I will generate entanglement with you, please don't hurt my kids!"

RETRACT_ASK_AGREEMENT = "Keep your filthy entanglement to yourself!"


class InitiativeTakingAgreementService(EntanglementAgreementServiceWithMessages,
                                       metaclass=ABCMeta):
    """Tries to reach agreement about entanglement generation by asking for it and waiting for confirmation.

    When agreement is requested, a message is sent to the remote node to ask if it wants to generate entanglement.
    The request is never rejected, i.e. response
    :class:`entanglement_agreement_service.ResEntanglementAgreementRejected` is never sent.
    The service will keep waiting until it receives a confirmation message from the remote node.
    At the time at which the message is received, a
    :class:`entanglement_agreement_service.ResEntanglementAgreementReached` is sent.

    This implementation of :class:`EntanglementAgreementService` is designed to reach agreement about
    entanglement generation with a remote node running :class:`RespondingAgreementService`.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    name : str, optional

    """

    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self._nodes_and_connections_with_outstanding_agreement_requests = []

    def try_to_reach_agreement(self, req):
        """Send a message asking for agreement and wait for a confirmation.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreement`
            Request holding requirements on the agreement that must be reached.

        """
        if (req.remote_node_id, req.connection_id) in self._nodes_and_connections_with_outstanding_agreement_requests:
            raise RuntimeError(f"Already trying to get agreement with node {req.remote_node_id} through connection "
                               f"{req.connection_id}, cannot handle two of the same requests at once.")
        message = Message(items=ASK_AGREEMENT)
        self._send_message(message=message, remote_node_id=req.remote_node_id, connection_id=req.connection_id)
        self._nodes_and_connections_with_outstanding_agreement_requests.append((req.remote_node_id, req.connection_id))

    def abort(self, req):
        """Send message to retract asking for agreement, and stop listening for confirmation messages.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreementAbort`
            Request specifying which attempt at agreement should be aborted.

        """
        message = Message(items=RETRACT_ASK_AGREEMENT)
        self._send_message(message=message, remote_node_id=req.remote_node_id, connection_id=req.connection_id)
        self._nodes_and_connections_with_outstanding_agreement_requests.remove((req.remote_node_id, req.connection_id))

    def _handle_message(self, message):
        """Handle confirmation messages.

        Parameters
        ----------
        message : :class:`netsquid.components.component.Message`
            Received message.

        Notes
        -----
        It is tempting to raise an error when receiving a confirmation message from a node with which there is no
        outstanding agreement request, as in that case the service does not know how to deal with the message.
        However, it may be that agreement was requested with a node, but then aborted afterwards at this node.
        The other node might then still send a confirmation message, as it doesn't know about the abortion.
        Since this is somewhat common, it is better to ignore these confirmation messages.


        """
        if message.items == [CONFIRM_AGREEMENT]:
            id_of_node_that_agreed = message.meta["sender_node_id"]
            id_of_connection = message.meta["connection_id"]
            if (id_of_node_that_agreed, id_of_connection) in \
                    self._nodes_and_connections_with_outstanding_agreement_requests:
                self.send_response(ResEntanglementAgreementReached(remote_node_id=id_of_node_that_agreed,
                                                                   connection_id=id_of_connection))
                self._nodes_and_connections_with_outstanding_agreement_requests.remove((id_of_node_that_agreed,
                                                                                        id_of_connection))
            else:
                # We do not raise an error in this case, because the receiving of an unexpected confirmation
                # message can be the result of a previously issued abort request.
                # See also the Notes section of the docstring of this method.
                # raise RuntimeError("Received confirmation message of node that has not been asked for agreement.")
                pass
        else:
            raise RuntimeError(f"{self} received a message that it does not know how to handle.")


class RespondingAgreementService(EntanglementAgreementServiceWithMessages,
                                 metaclass=ABCMeta):
    """Tries to reach agreement about entanglement generation by responding to other nodes taking the initiative.

    Waits for other nodes to ask for agreement about entanglement generation.
    If a request to reach agreement is made with a node that has not asked for agreement over that specific connection,
    a :class:`entanglement_agreement_service.ResEntanglementAgreementRejected` is sent directly.
    If the node has asked for agreement over that specific connection, a confirmation message is sent to that node.
    A :class:`entanglement_agreement_service.ResEntanglementAgreementReached` response is sent after a possible
    delay (e.g. to make sure entanglement generation is commenced at both nodes at the same time).

    If a message indicating that the remote node no longer wants to reach agreement over a respective connection
    is received, this service will start rejecting requests with that remote node over that connection again.
    If, at the time when the message is received, a confirmation message has already been sent but the
    :class:`ResEntanglementAgreementReached` response is still being delayed, the delay is cancelled and the response
    will no longer be sent.
    The confirmation message has already been sent however, and might lead to the remote node believing there is
    agreement when there is none.

    This implementation of :class:`EntanglementAgreementService` is designed to reach agreement about
    entanglement generation with a remote node running :class:`InitiativeTakingAgreementService`.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    name : str, optional

    """

    class _SendResponseAfterDelay(NodeProtocol):
        """Protocol used by `RespondingAgreementService` to send `ResEntanglementAgreementReached` with a delay.

        Parameters
        ----------
        superprotocol : :class:`RespondingAgreementService`
            Protocol that is using this protocol to send a delayed response.
        delay : float
            Amount of time [ns] the response should be delayed.
        remote_node_id : int
            ID of the remote node with which entanglement agreement should be claimed to have been reached
            after the delay.
        connection_id : int
            ID of the entangling connection about which agreement should be reached.

        """
        def __init__(self, superprotocol, delay, remote_node_id, connection_id):
            super().__init__(node=superprotocol.node, name="send_response_after_delay")
            self.superprotocol = superprotocol
            self.delay = delay
            self.remote_node_id = remote_node_id
            self.connection_id = connection_id

        def run(self):
            yield self.await_timer(duration=self.delay)
            self.superprotocol.send_response(ResEntanglementAgreementReached(remote_node_id=self.remote_node_id,
                                                                             connection_id=self.connection_id))

        def stop(self):
            super().stop()
            self.superprotocol._nodes_and_connections_with_which_agreement_is_being_confirmed.\
                remove((self.remote_node_id, self.connection_id))
            self.superprotocol._send_response_after_delay_protocols.remove(self)

    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self._nodes_and_connections_that_want_agreement = []
        self._nodes_and_connections_with_which_agreement_is_being_confirmed = []
        self._send_response_after_delay_protocols = []

    def try_to_reach_agreement(self, req):
        """Reject request for agreement if not asked for agreement by other node, otherwise confirm to that node.

        If no message asking this node for agreement has been received from the remote node specified in the request,
        it is immediately rejected.
        Otherwise, a confirmation message is sent to the remote node indicating that this node wants to reach agreement.
        This is considered enough to actually reach agreement with the remote node;
        after a time set by :meth:`RespondingAgreementService.determine_delay`, a
        :class:`ResEntanglementAgreementReached` will be sent.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreement`
            Request holding requirements on the agreement that must be reached.

        """
        if (req.remote_node_id, req.connection_id) in \
                self._nodes_and_connections_with_which_agreement_is_being_confirmed:
            raise RuntimeError(f"Already confirming agreement with with node {req.remote_node_id} through connection "
                               f"{req.connection_id}. Cannot get agreement with the same node through the same "
                               f"connection twice simultaneously.")
        elif (req.remote_node_id, req.connection_id) not in self._nodes_and_connections_that_want_agreement:
            self.send_response(ResEntanglementAgreementRejected(remote_node_id=req.remote_node_id,
                                                                connection_id=req.connection_id))
        else:
            self._nodes_and_connections_with_which_agreement_is_being_confirmed.append((req.remote_node_id,
                                                                                        req.connection_id))
            message = Message(items=[CONFIRM_AGREEMENT])
            self._send_message(message=message, remote_node_id=req.remote_node_id, connection_id=req.connection_id)
            send_response_after_delay_protocol = \
                self._SendResponseAfterDelay(superprotocol=self,
                                             delay=self.determine_delay(req.remote_node_id),
                                             remote_node_id=req.remote_node_id, connection_id=req.connection_id)
            send_response_after_delay_protocol.start()
            self._send_response_after_delay_protocols.append(send_response_after_delay_protocol)
            self._nodes_and_connections_that_want_agreement.remove((req.remote_node_id, req.connection_id))

    def _handle_message(self, message):
        """Handle messages asking for agreement or retracting the previous asking for agreement.

        Parameters
        ----------
        message : :class:`netsquid.components.component.Message`
            Received message.

        Notes
        -----
        In case a message is received indicating that a node retracts asking for agreement,
        but that node is not known as a node that wants agreement or with which agreement is currently being
        confirmed, it may sound like a good idea to raise an error.
        However, instead this service does nothing.
        The reason for this is that it can happen that the initiative node aborts trying to achieve
        agreement when the confirmation message is already sent by this service.
        In that case, it is too late to abort at this service, and a retraction message is received
        after the request has been resolved at the responder (thereby removing the initiative node from the
        lists of nodes that want agreement and nodes with which agreement is being confirmed).
        This is a situation that should not lead to an error.

        """
        if message.items == [ASK_AGREEMENT]:
            node_that_wants_agreement = message.meta["sender_node_id"]
            connection_that_wants_agreement = message.meta["connection_id"]
            if (node_that_wants_agreement, connection_that_wants_agreement) in \
                    self._nodes_and_connections_that_want_agreement:
                raise RuntimeError("A node-connection pair has asked for agreement twice.")
            self._nodes_and_connections_that_want_agreement.append((node_that_wants_agreement,
                                                                    connection_that_wants_agreement))
            self._send_update_signal()
        elif message.items == [RETRACT_ASK_AGREEMENT]:
            node_that_aborts_agreement = message.meta["sender_node_id"]
            connection_that_aborts_agreement = message.meta["connection_id"]
            if (node_that_aborts_agreement, connection_that_aborts_agreement) in \
                    self._nodes_and_connections_that_want_agreement:
                self._nodes_and_connections_that_want_agreement.remove((node_that_aborts_agreement,
                                                                        connection_that_aborts_agreement))
            elif (node_that_aborts_agreement, connection_that_aborts_agreement) in \
                    self._nodes_and_connections_with_which_agreement_is_being_confirmed:
                self.abort(req=ReqEntanglementAgreementAbort(remote_node_id=node_that_aborts_agreement,
                                                             connection_id=connection_that_aborts_agreement))
            else:
                # We do not raise an error in this case, since such an "unexpected" message can occur when
                # the other node aborts generating agreement while the confirmation message is already underway.
                # See also the Notes section of the docstring of this method.
                # raise RuntimeError("A node that has not asked for agreement is now retracting it.")
                pass
        else:
            raise RuntimeError(f"{self} received a message that it does not know how to handle.")

    def abort(self, req):
        """If currently waiting for a delayed `ResEntanglementAgreementReached`, stop waiting and don't send response.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreementAbort`
            Request specifying the obtaining of which agreement should be aborted.

        """
        for protocol in self._send_response_after_delay_protocols:
            if protocol.remote_node_id == req.remote_node_id:
                protocol.stop()

    @abstractmethod
    def determine_delay(self, remote_node_id):
        """Determine the delay of a `ResEntanglementAgreementReached` response after sending confirmation message.

        Parameters
        ----------
        remote_node_id : int
            ID of the remote node for which the delay should be calculated.

        Return
        ------
        float
            Delay [ns] before sending :class:`entanglement_agreement_service.ResEntanglementAgreementReached`.

        """
        pass


class InitiativeBasedAgreementServiceMode(Enum):
    InitiativeTaking = 1
    Responding = 2


class InitiativeBasedAgreementService(EntanglementAgreementService, metaclass=ABCMeta):
    """Reach agreement about entanglement generation between initiative-taking nodes and responding nodes.

    This service can switch between an initiative-taking mode and a responding mode.
    The current mode can determined, and a new mode be set using :meth:`InitiativeBasedAgreementService.mode`.

    If the mode is `InitiativeBasedAgreementServiceMode.InitiativeTaking`, this service implements
    :class:`InitiativeTakingAgreementService` under the hood.
    It then sends messages to remote nodes asking for agreement when a :class:`ReqEntanglementAgreement` is issued.
    :class:`ResEntanglementAgreementReached` response is sent if a confirmation message is received
    from a remote node that has been asked for agreement.

    If the mode is `InitiativeBasedAgreementServiceMode.Responding` this service implements
    :class:`RespondingAgreementService` under the hood.
    It then only handles a :class:`ReqEntanglementAgreement` if it has previously received a message asking for
    agreement, and will respond by sending a confirmation message back and giving a
    :class:`ResEntanglementAgreementReached` response after some set delay.

    Only instantiations of this service in different modes can reach agreement with each other.
    The purpose of this service that can switch modes, is that the roles of nodes in a network can be determined
    dynamically, such that it can be ensured that whenever two nodes want to reach agreement, their
    `EntanglementAgreementService`s are compatible.

    To implement this service, `InitiativeBasedAgreementService.INITIATIVE_TAKING_CLASS` (used when the mode
    is `InitiativeBasedAgreementServiceMode.InitiativeTaking`),
    and `InitiativeBasedAgreementService.RESPONDING_CLASS (used under the hood when the mode is
    `InitiativeBasedAgreementServiceMode.Responding`), must be overwritten, since otherwise these protocols
    (that are used under the hood to implement the different modes) are abstract classes.


    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    name : str, optional
    mode : :atr:`InitiativeBasedAgreementService`, optional
        Mode of operation of the service; either initiative taking or responding.
        Initiative taking by default. Can be changed after the fact using :meth:`InitiativeTakingAgreementService.mode`.

    """

    # class to use when in `InitiativeBasedAgreementServiceMode.InitiativeTaking`
    INITIATIVE_TAKING_CLASS = InitiativeTakingAgreementService

    # class to use when in `InitiativeBasedAgreementServiceMode.RESPONDING`
    RESPONDING_CLASS = RespondingAgreementService

    def __init__(self, node, name=None, mode=InitiativeBasedAgreementServiceMode.Responding):
        super().__init__(node=node, name=name)
        if not isinstance(mode, InitiativeBasedAgreementServiceMode):
            raise ValueError(f"{mode} is not an {InitiativeBasedAgreementServiceMode} value")
        self._mode = None
        self.mode = mode

    @property
    def mode(self):
        """The mode of operation of this service. Can be either initiative taking or responding.

        Returns
        -------
        `InitiativeBasedAgreementServiceMode`

        """
        return self._mode

    @mode.setter
    def mode(self, mode):
        """The mode of operation of this service. Can be either initiative taking or responding.

        Note: when changing the mode, all inner state of the protocol is lost.
        Therefore, be careful when changing the mode dynamically.

        Parameters
        -------
        mode : `InitiativeBasedAgreementServiceMode`
            Mode that should be used.

        """
        if not isinstance(mode, InitiativeBasedAgreementServiceMode):
            raise ValueError(f"{mode} is not an {InitiativeBasedAgreementServiceMode} value")
        if self._mode != mode:
            if self.is_running:
                raise RuntimeError("Cannot change mode while running.")
            if mode == InitiativeBasedAgreementServiceMode.InitiativeTaking:
                self._protocol = self.INITIATIVE_TAKING_CLASS(node=self.node,
                                                              name=self.name)
            else:
                self._protocol = self.RESPONDING_CLASS(node=self.node,
                                                       name=self.name)
            self._mode = mode

    def try_to_reach_agreement(self, req):
        """All requests are forwarded to the service implementation used under the hood.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreement`
            Request holding requirements on the agreement that must be reached.

        """
        return self._protocol.try_to_reach_agreement(req=req)

    def abort(self, req):
        """All requests are forwarded to the service implementation used under the hood.

        Parameters
        ----------
        req : :class:`entanglement_agreement_service.ReqEntanglementAgreementAbort`
            Request specifying the obtaining of which agreement should be aborted.

        """
        return self._protocol.abort(req=req)

    def start(self):
        self._protocol.start()
        super().start()

    def run(self):
        """Intercept all signals sent by service implementation used under the hood and resend them."""
        agreement_reached = self.await_signal(sender=self._protocol,
                                              signal_label=ResEntanglementAgreementReached.__name__)
        rejected = self.await_signal(sender=self._protocol,
                                     signal_label=ResEntanglementAgreementRejected.__name__)
        update = self.await_signal(sender=self._protocol,
                                   signal_label=signal_entanglement_agreement_status_change)
        while True:
            evt_expr = agreement_reached | rejected | update
            yield evt_expr
            [evt] = evt_expr.triggered_events
            signal = self._protocol.get_signal_by_event(evt)
            if (isinstance(signal.result, ResEntanglementAgreementReached)
               or isinstance(signal.result, ResEntanglementAgreementRejected)):
                self.send_response(signal.result)
            elif signal.label == signal_entanglement_agreement_status_change:
                self._send_update_signal()
            else:
                raise RuntimeError("Unexpected signal.")


class InitiativeTakingAgreementServiceWithClassicalConnections(InitiativeTakingAgreementService,
                                                               EntanglementAgreementServiceWithClassicalConnections):
    """Implementation of `InitiativeTakingAgreementService` using classical connections to send messages."""
    pass


class RespondingAgreementServiceWithClassicalConnections(RespondingAgreementService,
                                                         EntanglementAgreementServiceWithClassicalConnections):
    """Implementation of `RespondingAgreementService` using classical connections to send messages."""

    def determine_delay(self, remote_node_id):
        """Delay chosen to correspond to communication time through classical connection.

        Delay is chosen to be equal to the time it takes the confirm messages to reach the other node.
        If the other node runs :class:`InitiativeTakingAgreementService`, this results in both protocols sending
        :class:`ResEntanglementAgreementReached` simultaneously.

        Parameters
        ----------
        remote_node_id : int
            ID of the remote node for which the delay should be calculated.

        Return
        ------
        float
            Delay [ns] before sending :class:`entanglement_agreement_service.ResEntanglementAgreementReached`.

        """
        classical_connection = self._classical_connection_to_remote_node(remote_node_id)
        channel_a_to_b = classical_connection.subcomponents["channel_AtoB"]
        delay_a_to_b = channel_a_to_b.compute_delay()
        return delay_a_to_b


class InitiativeBasedAgreementServiceWithClassicalConnections(InitiativeBasedAgreementService):
    """Implementation of `InitiativeBasedAgreementService` using classical connections to send messages."""

    INITIATIVE_TAKING_CLASS = InitiativeTakingAgreementServiceWithClassicalConnections

    RESPONDING_CLASS = RespondingAgreementServiceWithClassicalConnections
