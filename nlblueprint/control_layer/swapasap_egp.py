"""Implementation of a link layer protocol for a repeater chain
of automated nodes.
"""
import numpy as np
from netsquid_driver.EGP import EGPService, ResMeasureDirectly, ResCreateAndKeep
from netsquid_driver.entanglement_service import (EntanglementService, ReqEntanglement,
                                                  ResEntanglementSuccess, ResEntanglementError)
from netsquid_entanglementtracker.entanglement_tracker_service import LinkStatus, EntanglementTrackerService
from netsquid_driver.measurement_services import MeasureService, ReqMeasure, ResMeasure
from netsquid_driver.message_handler_protocol import MessageHandlerProtocol, FORWARD_CHAIN, TYPE_REQUEST
from qlink_interface import MeasurementBasis
from netsquid.components.component import Message
from netsquid_driver.swap_asap_service import SwapASAPService, ReqSwapASAPAbort, ReqSwapASAP
from netsquid_driver.memory_manager_service import QuantumMemoryManager, ReqMove, ReqFreeMemory, \
    ResMoveSuccess


def do_not_forward(message, port):
    """Callback function removing "forward" from message meta.
    """

    message.meta.pop("forward")


def ignore_message(message, port):
    """Callback function altering type of messages to "Ignore".
    """

    message.meta["type"] = "ignore"


class SwapAsapEndNodeLinkLayerProtocol(EGPService):
    """Link layer protocol for end nodes in a repeater chain of automated nodes generating end-to-end entanglement once.

    The process starts when a request is placed on one of the end nodes. This can be a
    :class:`~qlink_interface.ReqMeasureDirectly` request, if the entangled pair should be measured upon generation
    (e.g. for QKD) or a :class:`~qlink_interface.ReqCreateAndKeep` request, if the entangled pair should be kept.

    The end node who received the request then places a message on its neighbour node to be forwarded across the chain
    to the other end node, asking to generate entanglement. Upon receival of this message, the second end node sends
    a confirmation message back and starts entanglement generation with its neighbour. When the original end node gets
    the confirmation message, it sends a message through the chain to activate all (automated) repeater nodes and starts
    entanglement generation with its neighbour.

    When the end node that initiated the process receives all the swap outcomes from the repeater nodes, it sends a
    success response back, which can be :class:`~nlblueprint.services.EGP.ResMeasureDirectly` or
    :class:`~qlink_interface.ResCreateAndKeep`. In the second case, a measurement request
    :class:`~nlblueprint.services.operation_services.ReqMeasure` is placed on the local measurement service and the
    outcome of this measurement is included in the response.


    Parameters
    ----------
        node : :class:`~netsquid.nodes.node.Node`
            The node this protocol is running on
        name : str, optional
            The name of this protocol

    """

    _TYPE_CONFIRM = "confirm"
    _TYPE_ASK = "ask"

    def __init__(self, node, name=None):
        super().__init__(node=node, name=name)
        self._should_keep = None
        self._is_upstream = None
        self._mem_pos = 0
        self._number_pairs_to_generate = 0
        self._number_pairs_generated = 0
        self._start_own_link_layer = "start_own_link_layer_signal"
        self.add_signal(self._start_own_link_layer)
        self._rotation_to_measbasis = {(0, np.pi / 2, 0): MeasurementBasis.X,
                                       (np.pi / 2, 0, 0): MeasurementBasis.Y,
                                       (0, 0, 0): MeasurementBasis.Z}
        self._bell_state = None  # Bell index of expected end-to-end entanglement
        self._remote_node_id = None
        self._unique_id = 0

    def start(self):
        """Starts the protocol and registers necessary callbacks.
        """

        super().start()
        message_handler_protocol = self.node.driver[MessageHandlerProtocol]

        # ignore all messages that should be forwarded (they are either meant for repeater nodes,
        # or they are picked up in the run method of this protocol)
        message_handler_protocol.register_callback(do_not_forward, meta={"forward": FORWARD_CHAIN})

        message_handler_protocol.register_callback(ignore_message, meta={"service": SwapASAPService})

        message_handler_protocol.register_callback(self._handle_ask_message, meta={"type": self._TYPE_ASK})
        message_handler_protocol.register_callback(self._handle_confirm_message, meta={"type": self._TYPE_CONFIRM})

    def run(self):
        """Generates end-to-end entanglement across a repeater chain of automated nodes."""
        while True:
            yield self.await_signal(sender=self, signal_label=self._start_own_link_layer)
            yield from self._generate_end_to_end_entanglement()
            if self._should_keep:
                self.send_response(ResCreateAndKeep(create_id=self._create_id,
                                                    bell_state=self._bell_state,
                                                    logical_qubit_id=self._mem_pos))
                # we add a very short delay to make sure that data collection works as expected
                # (see https://ci.tno.nl/gitlab/QIA/nlblueprint/-/issues/582 for details)
                yield self.await_timer(1e-6)
                entanglement_tracker = self.node.driver[EntanglementTrackerService]
                [link_identifier] = entanglement_tracker.get_links_by_status(LinkStatus.AVAILABLE)
                entanglement_tracker.register_measurement(link_identifier)
            else:
                yield from self._perform_measurement()
            req = ReqFreeMemory()
            self.node.driver[QuantumMemoryManager].put(request=req)
            self._number_pairs_generated += 1
            # WARNING: This only works because we hardcode the communication position to be the first one
            # This should eventually be replaced by asking the memory manager for a free communication position
            self._mem_pos = 0
            if self._number_pairs_generated == self._number_pairs_to_generate:
                if self._is_upstream:
                    req = ReqSwapASAPAbort(request_id=self._create_id)
                    message = Message([req],
                                      **{"type": TYPE_REQUEST, "service": SwapASAPService, "forward": FORWARD_CHAIN})
                    self._assign_header_to_message(message)
                    self.node.ports[self._communication_port_name].tx_output(message)
                self._number_pairs_generated = 0
                continue
            self.send_signal(self._start_own_link_layer)

    def _generate_end_to_end_entanglement(self):
        """Generate entanglement between this node and the other end node (specified in the request).

        End-to-end entanglement is generated by generating entanglement with this end node's neighbour,
        and then waiting for the entanglement tracker to decide that either
        1. the local entangled qubit has become entangled with the other end node, or
        2. the entangled state of the local entangled qubit has been destroyed (e.g. because a qubit has been discarded
        by a repeater node).

        In case the second condition is met, a new attempt is made by starting entanglement generation with the
        neighbour again.

        """
        entanglement_tracker = self.node.driver[EntanglementTrackerService]
        while True:
            yield from self._perform_own_link_layer()
            [link_identifier] = entanglement_tracker.get_links_by_status(LinkStatus.AVAILABLE)
            yield from entanglement_tracker.await_entanglement(node_ids={self.node.ID, self._remote_node_id},
                                                               link_identifiers={link_identifier},
                                                               other_nodes_allowed=False)
            entanglement_info = entanglement_tracker.get_entanglement_info_of_link(link_identifier)
            if entanglement_info.intact:
                self._bell_state = entanglement_info.bell_index
                entanglement_tracker.untrack_entanglement_info(entanglement_info)
                break
            else:
                req = ReqFreeMemory()
                self.node.driver[QuantumMemoryManager].put(request=req)
                # WARNING: This only works because we hardcode the communication position to be the first one
                # This should eventually be replaced by asking the memory manager for a free communication position
                self._mem_pos = 0

    def _perform_measurement(self):
        """Perform a local measurement."""
        request = ReqMeasure(mem_pos=self._mem_pos,
                             x_rotation_angle_1=self._local_measurement_angles["x_1"],
                             y_rotation_angle=self._local_measurement_angles["y"],
                             x_rotation_angle_2=self._local_measurement_angles["x_2"])
        measure_protocol = self.node.driver.services[MeasureService]
        measure_protocol.put(request=request)
        yield self.await_signal(sender=measure_protocol,
                                signal_label=ResMeasure.__name__)
        response = measure_protocol.get_signal_result(label=ResMeasure.__name__, receiver=self)
        outcome = response.outcome
        if isinstance(outcome, list):
            measurement_outcome = outcome[0]
        else:
            measurement_outcome = outcome
        req = ReqFreeMemory(memory_position_id=self._mem_pos)
        self.node.driver[QuantumMemoryManager].put(request=req)
        self.node.qmemory.mem_positions[self._mem_pos].in_use = False
        self.send_response(ResMeasureDirectly(create_id=self._create_id,
                                              measurement_outcome=measurement_outcome,
                                              bell_state=self._bell_state,
                                              measurement_basis=self._rotation_to_measbasis[
                                                  tuple(self._local_measurement_angles.values())]))

    def _handle_ask_message(self, message, port):
        """Handles ask message received from remote end node.

        Parameters
        ----------
        message : :class:`~netsquid.components.component.Message`
            Message from remote end node
        port : :class:`~netsquid.components.component.Port`
            Port this function is bound to

        """
        self._send_confirm_message_to_other_egp(create_id=message.items[0])
        self._is_upstream = False
        self._remote_node_id = message.items[3]
        local_measurement_angles = message.items[2]
        if local_measurement_angles == {}:
            self._should_keep = True  # otherwise, there would have been measurement angles
        else:
            self._should_keep = False
            self._local_measurement_angles = local_measurement_angles
        self._create_id = message.items[0]
        self._number_pairs_to_generate = message.items[1]
        self.send_signal(self._start_own_link_layer)

    def _handle_confirm_message(self, message, port):
        """Handles confirm message received from remote end node.

        Parameters
        ----------
        message : :class:`~netsquid.components.component.Message`
            Message from remote end node
        port : :class:`~netsquid.components.component.Port`
            Port this function is bound to

        """

        self._is_upstream = True
        self.send_repeater_activation_message(create_id=message.items[0], port_name=self._communication_port_name)
        self._create_id = message.items[0]
        self.send_signal(self._start_own_link_layer)

    def _send_confirm_message_to_other_egp(self, create_id):
        """Sends a message to the other end node on the chain confirming that it wants to start generating entanglement.

        Parameters
        ----------
        create_id : int
            Identifier that will also be used in responses corresponding to this request.

        """

        message = Message([create_id], **{"type": self._TYPE_CONFIRM, "forward": FORWARD_CHAIN})
        self._assign_header_to_message(message)

        self.node.ports[self._communication_port_name].tx_output(message)

    def _send_ask_message_to_other_egp(self, create_id, number=1, remote_measurement_angles={}):
        """Sends a message to the other end node on the chain asking if it wants to start generating entanglement.

        Parameters
        ----------
        create_id : int
            Identifier that will also be used in responses corresponding to this request.
        number : int
            Number of pairs that should be generated.
        remote_measurement_angles : dict
            Dictionary containing Euler angles the remote node should use for measuring after entanglement is generated.

        """

        message = Message([create_id, number, remote_measurement_angles, self.node.ID],
                          **{"type": self._TYPE_ASK, "forward": FORWARD_CHAIN})
        self._assign_header_to_message(message)

        self.node.ports[self._communication_port_name].tx_output(message)

    def create_and_keep(self, req):
        """Starts the process of generating an end-to-end entangled pair to be kept by sending message to other end node
        asking to start entanglement generation.

        Parameters
        ----------
        req : :class:`~qlink_interface.ReqCreateAndKeep`
            Request that needs to be handled by this method.

        """

        self._remote_node_id = req.remote_node_id
        self._should_keep = True
        create_id = self._get_create_id()
        # self._mem_pos = create_id
        self._number_pairs_to_generate = req.number
        self._send_ask_message_to_other_egp(create_id, self._number_pairs_to_generate)
        return create_id

    def measure_directly(self, req):
        """Starts the process of generating an end-to-end entangled pair to be immediately measured by sending message
        to other end node asking to start entanglement generation.

        Parameters
        ----------
        req : :class:`~qlink_interface.ReqMeasureDirectly`
            Request that needs to be handled by this method.

        """

        self._remote_node_id = req.remote_node_id
        self._should_keep = False
        self._local_measurement_angles = {"x_1": req.x_rotation_angle_local_1,
                                          "y": req.y_rotation_angle_local,
                                          "x_2": req.x_rotation_angle_local_2}
        remote_measurement_angles = {"x_1": req.x_rotation_angle_remote_1,
                                     "y": req.y_rotation_angle_remote,
                                     "x_2": req.x_rotation_angle_remote_2}
        if tuple(self._local_measurement_angles.values()) not in self._rotation_to_measbasis:
            raise ValueError("Link layer protocol only supports measurements in X, Y and Z bases.")
        create_id = self._get_create_id()
        self._number_pairs_to_generate = req.number
        # self._mem_pos = create_id
        self._send_ask_message_to_other_egp(create_id, self._number_pairs_to_generate, remote_measurement_angles)

        return create_id

    def remote_state_preparation(self, req):
        raise NotImplementedError

    def receive(self, req):
        """Allow entanglement generation with a remote EGP upon request by that EGP.

        Parameters
        ----------
        req : :object:`qlink_interface.ReqReceive`
            Request that needs to be handled by this method.

        """
        # TODO
        raise NotImplementedError

    def stop_receive(self, req):
        """Stop allowing entanglement generation with a remote EGP upon request by that EGP.

        Parameters
        ----------
        req : :object:`qlink_interface.ReqStopReceive`
            Request that needs to be handled by this method.

        """
        # TODO
        raise NotImplementedError

    @property
    def is_upstream(self):
        return self._is_upstream

    def send_repeater_activation_message(self, create_id, port_name):
        """Sends a message to all repeater nodes in the chain with a
        :class:`~nlblueprint.services.swap_asaprequest.ReqSwapASAP` request for them to start their local SWAP-ASAP
        protocols.

        Parameters
        ----------
        create_id : int
            Identifier that will also be used in responses corresponding to this request.
        port_name : str
            Name of the port where this message will be put.

        """
        request = ReqSwapASAP(request_id=create_id, num=0)

        message = Message([request], **{"type": TYPE_REQUEST, "service": SwapASAPService, "forward": FORWARD_CHAIN})
        self._assign_header_to_message(message)

        self.node.ports[self._communication_port_name].tx_output(message)

    def _perform_own_link_layer(self):
        """Places :class:`~nlblueprint.services.entanglement_service.ReqEntanglement` requests to the local
        entanglement generation protocol and awaits a response. If we get a failure response,
        :class:`~nlblueprint.services.entanglement_service.ResEntanglementError`, we try again until a success response,
        :class:`~nlblueprint.services.entanglement_service.ResEntanglementSuccess`, occurs.
        """
        request = ReqEntanglement(port_name=self._entanglement_port_name, mem_pos=self._mem_pos)
        local_entanglement_protocol = self.node.driver[EntanglementService]
        local_entanglement_protocol.put(request=request)

        while True:
            evt_entanglement_succ = self.await_signal(sender=local_entanglement_protocol,
                                                      signal_label=ResEntanglementSuccess.__name__)
            evt_entanglement_fail = self.await_signal(sender=local_entanglement_protocol,
                                                      signal_label=ResEntanglementError.__name__)
            evt_expr = evt_entanglement_succ | evt_entanglement_fail
            yield evt_expr

            if evt_expr.first_term.value:
                try:
                    if self.node.driver[QuantumMemoryManager].num_communication_qubits == 1 \
                            and self._should_keep and self.node.qmemory.num_positions > 2:
                        request = ReqMove(from_memory_position_id=self._mem_pos)
                        self.node.driver[QuantumMemoryManager].put(request=request)
                        yield self.await_signal(sender=self.node.driver[QuantumMemoryManager],
                                                signal_label=ResMoveSuccess.__name__)
                        response = self.node.driver[QuantumMemoryManager].get_signal_result(
                            label=ResMoveSuccess.__name__, receiver=self)
                        self._mem_pos = response.memory_position_id
                except AttributeError:
                    pass
                break
            else:
                req = ReqFreeMemory(memory_position_id=self._mem_pos)
                self.node.driver[QuantumMemoryManager].put(request=req)

    @property
    def _communication_port_name(self):
        [comm_port_name] = [port_name for port_name in self.node.ports if port_name in ["A", "B"]]
        return comm_port_name

    @property
    def _entanglement_port_name(self):
        [ent_port_name] = [port_name for port_name in self.node.ports if port_name in ["ENT_A", "ENT_B", "ENT"]]
        return ent_port_name

    @property
    def is_connected(self):
        """The service is only connected if the driver at the node has one communication port and the right services.

        Requires exactly one port with either name "A" or "B" for classical communication.
        At the node's :class:`Driver`, the following services must be registered:
        - :class:`nlblueprint.services.message_handler_protocol.MessageHandlerProtocol`,
        - :class:`nlblueprint.services.operation_services.MeasureService`,
        - :class:`nlblueprint.services.entanglement_service.EntanglementService`.

        """
        if MessageHandlerProtocol not in self.node.driver.services:
            return False
        if MeasureService not in self.node.driver.services:
            return False
        if EntanglementService not in self.node.driver.services:
            return False
        if EntanglementTrackerService not in self.node.driver.services:
            return False
        try:
            comm_port = self._communication_port_name
            if comm_port is None:
                return False
        except ValueError:
            return False
        return super().is_connected

    def _assign_header_to_message(self, message):
        """Checks if message to be sent has a header. If not, assigns it a unique one."""
        if not self._message_has_header(message):
            header = self._get_unique_header()
            message.meta["header"] = header

    @staticmethod
    def _message_has_header(message):
        """Checks if message to be sent has a header"""
        try:
            return message.meta["header"] is not None
        except KeyError:
            return False

    def _get_unique_header(self):
        """Generates unique header."""
        unique_header = self.node.name + '_' + str(self._unique_id)
        self._unique_id += 1
        return unique_header
