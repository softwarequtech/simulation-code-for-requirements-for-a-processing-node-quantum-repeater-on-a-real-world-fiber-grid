from netsquid import BellIndex

from netsquid_driver.EGP import EGPService

# TODO: no delay in measurement outcome


class MagicEGP(EGPService):
    """EGP service implementation which create entanglement magically.

    This protocol uses :class:`netsquid-magic.link_layer.MagicLinkLayerProtocol` under the hood,
    and only forwards requests and responses to and from this protocol.
    What kind of quantum states are magically delivered, and with what frequency,
    depends on the :class:`~netsquid-magic.magic_distributor.MagicDistributor` held my the magic link-layer protocol.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    magic_link_layer_protocol: :class:`~netsquid-magic.link_layer.MagicLinkLayerProtocolWithSignaling`
        Magic link-layer protocol with signalling to be used under the hood. This can also use another implementation
        of a :class:`netsquid-magic.magic.MagicProtocol` with the necessary signalling.
    name : str, optional
        The name of this protocol.

    """
    def __init__(self, node, magic_link_layer_protocol, name=None):
        super().__init__(node=node, name=name)
        self.add_subprotocol(magic_link_layer_protocol, "magic_link_layer_protocol")

    def run(self):
        """Listen for reactions :class:`~netsquid-magic.link_layer.MagicLinkLayerProtocol` and forward them as response.

        Whenever this protocol is running, it continuously listens for reactions from the
        :class:`~netsquid-magic.link_layer.MagicLinkLayerProtocol` that runs under the hood.
        All reactions are intercepted, but only those that are meant for this specific node are forwarded.

        Raises
        ------
        TypeError
            If the reaction message contains attribute "bell_state", and the value of the attribute is not of type
            :class:`~netsquid.qubits.ketstates.BellIndex`.

        """
        while True:
            yield self.await_signal(sender=self.subprotocols["magic_link_layer_protocol"],
                                    signal_label="react_to_{}".format(self.node.ID))
            result = self.subprotocols["magic_link_layer_protocol"].get_signal_result(
                label="react_to_{}".format(self.node.ID), receiver=self)
            if result.node_id == self.node.ID:
                try:
                    BellIndex(result.msg.bell_state)
                except AttributeError:
                    pass
                except ValueError:
                    raise TypeError(f"{result.msg.bell_state}, which was obtained from magic link layer protocol,"
                                    f"is not a :class:`netsquid.qubits.ketstates.BellIndex`.")
                self.send_response(response=result.msg)

    def create_and_keep(self, req):
        super().create_and_keep(req)
        return self.subprotocols["magic_link_layer_protocol"].put_from(self.node.ID, req)

    def measure_directly(self, req):
        super().measure_directly(req)
        return self.subprotocols["magic_link_layer_protocol"].put_from(self.node.ID, req)

    def remote_state_preparation(self, req):
        super().remote_state_preparation(req)
        return self.subprotocols["magic_link_layer_protocol"].put_from(self.node.ID, req)

    def receive(self, req):
        super().receive(req)
        self.subprotocols["magic_link_layer_protocol"].put_from(self.node.ID, req)

    def stop_receive(self, req):
        super().stop_receive(req)
        self.subprotocols["magic_link_layer_protocol"].put_from(self.node.ID, req)
