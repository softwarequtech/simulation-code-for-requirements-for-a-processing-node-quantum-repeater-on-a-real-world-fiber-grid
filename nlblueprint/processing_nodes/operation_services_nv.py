from netsquid.components import QuantumProgram, INSTR_MEASURE, INSTR_ROT_X, INSTR_ROT_Y, INSTR_INIT
from netsquid_nv.move_circuits import move_using_CXDirections
from netsquid_nv.rotated_bell_measurement import apply_rotated_bell_state_measurement,\
    convert_rotated_bell_state_circuit_outcomes_to_bell_index
from netsquid_entanglementtracker.entanglement_tracker_service import EntanglementTrackerService
import nlblueprint.processing_nodes.measurement_services_processing_nodes as operation_services_abstract
from nlblueprint.processing_nodes.entanglement_magic import SingleMemoryEntanglementMagic

ELECTRON_POSITION = 0


class NVMeasureProgram(QuantumProgram):
    """Implements arbitrary-basis single-qubit measurement circuit on an NV quantum processor.

    The measurement is done in any desired basis by first performing a rotation according to the
    specified Euler angles.
    """

    def program(self, x_rotation_angle_1, y_rotation_angle, x_rotation_angle_2):
        [qubit_to_meas] = self.get_qubit_indices(1)
        if qubit_to_meas != ELECTRON_POSITION:
            raise ValueError("Only the electron can be measured")

        self.apply(instruction=INSTR_ROT_X, qubit_indices=qubit_to_meas, angle=x_rotation_angle_1)
        self.apply(instruction=INSTR_ROT_Y, qubit_indices=qubit_to_meas, angle=y_rotation_angle)
        self.apply(instruction=INSTR_ROT_X, qubit_indices=qubit_to_meas, angle=x_rotation_angle_2)
        self.apply(instruction=INSTR_MEASURE, qubit_indices=qubit_to_meas, output_key="outcome")

        yield self.run()


class NVMeasureService(operation_services_abstract.ProcessingNodeMeasureService):
    """Service for performing a single-qubit measurement on an NV node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the measurement is registered at the entanglement tracker.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'NVMeasureService'.

    """
    def __init__(self, node, name="NVMeasureService"):
        super().__init__(node=node, name=name, meas_prog=NVMeasureProgram)


class NVSwapService(operation_services_abstract.ProcessingNodeSwapService):
    """Service for entanglement swapping (i.e. performing a Bell-state measurement) on an NV node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the swap is registered at the entanglement tracker.

    If a discard is registered at the local `EntanglementTrackerService` while the swap program is being executed,
    the swap is considered to have failed, and all participating qubits are discarded (if they weren't already).

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'NVSwapService'.

    """

    class NVSwapProgram(QuantumProgram):
        def program(self):
            apply_rotated_bell_state_measurement(self, electron_position=0, carbon_position=1, inplace=False)
            yield self.run()
            self.output["bell_index"] = convert_rotated_bell_state_circuit_outcomes_to_bell_index(self)
            yield self.run()

        @property
        def outcome_as_netsquid_bell_index(self):  # required by the way service is set up
            return self.output["bell_index"]

    def __init__(self, node):
        super().__init__(node=node, name="NVSwapService", swap_prog=self.NVSwapProgram)

    def _perform_swap(self):
        if self._mem_pos_1 != ELECTRON_POSITION:
            raise ValueError("For NV, the first qubit of the Bell-state measurement should be the electron spin")
        return super()._perform_swap()


class NVMoveProgram(QuantumProgram):

    def program(self):
        electron, carbon = self.get_qubit_indices(2)
        self.apply(INSTR_INIT, [carbon])
        move_using_CXDirections(qprogram=self,
                                control_position=electron,
                                target_position=carbon)
        yield self.run()


class NVSingleMemoryEntanglementMagic(SingleMemoryEntanglementMagic):
    """Subclass of :class:`~nlblueprint.service_implementations.entanglement_magic.SingleMemoryEntanglementMagic`
    which performs the 'electron->carbon move' operation in case the electron position is taken when
    new entanglement should be generated.

    Parameters
    ----------
    node: :obj:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    name : str, optional
        The name of this protocol.
    """

    def __init__(self, node, name=None):
        super().__init__(node, num_parallel_links=1, name=name)
        self._move_program = "default"

    @property
    def move_program(self):
        """The QuantumProgram that is used to perform the 'electron -> carbon move'
        operation.

        By convention, its first qubit is the electron, and the second the carbon.
        For example:

        >>> class MoveProgram(QuantumProgram):

        >>>     def program(self):
        >>>         electron, carbon = self.get_qubit_indices(2)
        >>>         # .... perform whichever operations on the electron and carbon spin
        >>>         # which maps the former onto the latter
        >>>         yield self.run()
        """
        if self._move_program == "default":
            return NVMoveProgram()
        else:
            return self._move_program

    @move_program.setter
    def move_program(self, value):
        if not isinstance(value, QuantumProgram):
            raise TypeError("{} is not a QuantumProgram".format(value))
        self._move_program = value

    def _perform_move(self, callback, **callback_args):
        """
        Perform a 'move' from the electron spin to an arbitrarily
        chosen free carbon spin.

        Parameters
        ----------
        callback: fn
            Callback function that is called when the 'move' operation
            has finished.
        **callback_args
            Arguments that are passed to the callback when it is called.
        """
        unused_carbon = self._get_arbitrary_unused_carbon_position()
        qubit_mapping = [ELECTRON_POSITION, unused_carbon]
        self.node.qmemory.execute_program(self.move_program, qubit_mapping=qubit_mapping)
        self.node.qmemory.set_program_done_callback(callback=callback, once=True, **callback_args)
        self.node.driver[EntanglementTrackerService].register_move(ELECTRON_POSITION, unused_carbon)

    def _execute_request(self, req):
        """Starts the generation of entanglement, possibly preceded by a 'move' operation
        electron->free carbon in case the electron position is in use.
        """
        assert(req.mem_pos == ELECTRON_POSITION)
        if self.get_in_use(mem_pos=ELECTRON_POSITION):
            self._perform_move(callback=super()._execute_request,
                               req=req)
        else:
            super()._execute_request(req=req)

    def _can_execute_request(self, req):
        """Checks whether request can be executed, which it cannot if:

        - if memory position is already in use;
        - if the relevant subprotocol on the port is already running;
        - all possible parallel attempt-paths are taken
        - the electron spins and all carbon spins are in use
        - the QuantumProcessor is busy doing other things

        Returns
        -------
        bool
        """
        if self._does_request_require_move(req) and self._are_all_carbons_in_use():
            return False
        elif self.node.qmemory.busy:
            return False
        else:
            subprot = self._get_subprot_by_port_name(req.port_name)
            agreement_is_possible = req not in self._requests_with_agreement_rejected
            return not subprot.is_running and not self._are_all_parallel_attempts_taken() and agreement_is_possible

    def _does_request_require_move(self, req):
        return self.get_in_use(mem_pos=req.mem_pos)

    def _get_arbitrary_unused_carbon_position(self):
        for pos in self.node.qmemory.carbon_positions:
            if not self.get_in_use(mem_pos=pos):
                return pos
        return None

    def _are_all_carbons_in_use(self):
        return (self._get_arbitrary_unused_carbon_position() is None)
