import numpy as np
from netsquid.nodes.node import Node
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.components.models.qerrormodels import DepolarNoiseModel
from netsquid.components import QuantumProgram, INSTR_SWAP
from netsquid_abstractmodel.abstract_node import AbstractNode
from netsquid_trappedions.ion_trap import IonTrap
from netsquid_nv.nv_center import NVQuantumProcessor
from netsquid_nv.nv_parameter_set import compute_product_tau_decay_delta_w_from_nodephasing_number
from nlblueprint.processing_nodes.measurement_services_processing_nodes import ProcessingNodeMeasureService, \
    ProcessingNodeSwapService
from nlblueprint.processing_nodes.operation_services_nv import NVSingleMemoryEntanglementMagic
from netsquid_driver.EGP import EGPService
from netsquid_driver.entanglement_agreement_service import EntanglementAgreementService
from nlblueprint.control_layer.initiative_based_agreement_service import \
    RespondingAgreementServiceWithClassicalConnections, InitiativeTakingAgreementServiceWithClassicalConnections
from netsquid_driver.message_handler_protocol import MessageHandlerProtocol
from nlblueprint.control_layer.swap_asap import SwapASAP, SwapASAPOneSequentialRepeater
from netsquid_driver.swap_asap_service import SwapASAPService
from nlblueprint.processing_nodes.operation_services_nv import NVMeasureProgram, NVSwapService, NVMoveProgram
from nlblueprint.control_layer.swapasap_egp import SwapAsapEndNodeLinkLayerProtocol
from netsquid_entanglementtracker.bell_state_tracker import BellStateTracker
from netsquid_entanglementtracker.cutoff_service import CutoffService
from netsquid_entanglementtracker.cutoff_timer import CutoffTimer
from netsquid_entanglementtracker.entanglement_tracker_service import EntanglementTrackerService
from nlblueprint.processing_nodes.entanglement_magic import SingleMemoryEntanglementMagic
from netsquid_driver.entanglement_service import EntanglementService
from netsquid_driver.measurement_services import MeasureService, SwapService
from netsquid_driver.driver import Driver
from nlblueprint.processing_nodes.operation_services_abstract import AbstractMeasureService, AbstractSwapService
from nlblueprint.processing_nodes.operation_services_ti import TIMeasureService, TISwapService, \
    TISingleMemoryEntanglementMagic
from netsquid_driver.memory_manager_service import QuantumMemoryManager
from nlblueprint.processing_nodes.memory_manager_implementations import MemoryManagerWithMoveProgram, \
    CommunicationMemoryManager

abstract_node_perfect_params = {
    "num_parallel_links": None,
    "num_communication_qubits": None,
    "swap_quality": 1,
    "T1": 0,
    "T2": 0,
    "emission_fidelity": 1,
    "initialization_duration": 0,
    "single_qubit_gate_duration": 0,
    "two_qubit_gate_duration": 0,
    "measurement_duration": 0,
    "emission_duration": 0,
}

abstract_node_standard_params = dict(abstract_node_perfect_params)

abstract_node_standard_params.update({
    "num_positions": 3,
    "cutoff_time": None,
    "port_names": None,
    "ID": None,
    "bright_state_param": None,
    "sequential_one_repeater": False,
})

ti_perfect_params = {
    "num_parallel_links": 1,
    "num_communication_qubits": None,
    "coherence_time": 0,
    "prob_error_0": 0,
    "prob_error_1": 0,
    "init_depolar_prob": 0,
    "rot_z_depolar_prob": 0,
    "multi_qubit_xy_depolar_prob": 0,
    "ms_depolar_prob": 0,
    "emission_fidelity": 1,
    "collection_efficiency": 1,
    "measurement_duration": 0,
    "initialization_duration": 0,
    "z_rotation_duration": 0,
    "multi_qubit_xy_rotation_duration": 0,
    "ms_pi_over_2_duration": 0,
    "emission_duration": 0,
}

ti_standard_params = dict(ti_perfect_params)

ti_standard_params.update({
    "num_positions": 2,
    "port_names": None,
    "cutoff_time": None,
    "sequential_one_repeater": False,
})

nv_perfect_params = {
    "num_communication_qubits": 1,
    "electron_init_depolar_prob": 0.,
    "electron_single_qubit_depolar_prob": 0.,
    "prob_error_0": 0.,
    "prob_error_1": 0.,
    "carbon_init_depolar_prob": 0.,
    "carbon_z_rot_depolar_prob": 0.,
    "ec_gate_depolar_prob": 0.,
    "electron_T1": 0.,
    "electron_T2": 0.,
    "carbon_T1": 0.,
    "carbon_T2": 0.,
    "coherent_phase": 0,
    "initial_nuclear_phase": 0.,
    "p_double_exc": 0.,
    "p_fail_class_corr": 0.,
    "photon_emission_delay": 0.,
    "n1e": 1.e+20,
    "std_electron_electron_phase_drift": 0.,
    "visibility": 1.,
    "carbon_init_duration": 0.,
    "carbon_z_rot_duration": 0.,
    "electron_init_duration": 0.,
    "electron_single_qubit_duration": 0.,
    "ec_two_qubit_gate_duration": 0.,
    "measure_duration": 0.,
    "magical_swap_gate_duration": 0.,
}

nv_standard_params = dict(nv_perfect_params)

nv_standard_params.update({
    "num_positions": 2,
    "port_names": None,
    "cutoff_time": None,
    "use_magical_swap": False,
    "bright_state_param": None,
    "sequential_one_repeater": False,
})


class AbstractMoveProgram(QuantumProgram):

    def program(self):
        move_from, move_to = self.get_qubit_indices(2)
        self.apply(INSTR_SWAP, [move_from, move_to])

        yield self.run()


class AbstractNodeWithDriver(AbstractNode):
    """:class:`netsquid_abstractmodel.abstract_node.AbstractNode` endowed with a
    :class:`nlblueprint.processing_node_API.driver.Driver`. The following protocol implementations are already loaded:

    - Message handler service: :class:`nlblueprint.control_layer.message_handler_protocol.MessageHandlerProtocol`
    - Entanglement-tracker service: :class:`nlblueprint.entanglement_tracker.bell_state_tracker.BellStateTracker`
    - Entanglement-agreement service:
        - :class:`nlblueprint.control_layer.initiative_based_agreement_service.InitiativeTakingAgreementService`
        if the node ID is odd,
        - :class:`nlblueprint.control_layer.initiative_based_agreement_service.RespondingAgreementService`
        if the node ID is even.
    - Entanglement service:
        :class:`nlblueprint.operation_services.entanglement_magic.SingleMemoryEntanglementMagic`
    - Measurement service: :class:`nlblueprint.processing_nodes.operation_services_abstract.MeasureServiceAbstract`
    - Swap service: :class:`nlblueprint.processing_nodes.operation_services_abstract.SwapServiceAbstract`

    If this node is a repeater node, the following service is also loaded in:

    - SWAPASAPService: :class:`nlblueprint.control_layer.swap_asap.SwapASAP`

    Parameters
    ----------
    name : str
        Name for this node.
    num_positions : int
        Number of qubits in the processor.
    end_node : bool
        True if this node is an end node, False if it is a repeater.
    num_parallel_links : int
        Number of elementary links on which this node can generate entanglement simultaneously.
    num_communication_qubits : int
        Number of a communication qubits the node has.
    swap_quality : float
        Parametrizes the depolarizing noise introduced by the node's processor during an entanglement swap.
    emission_fidelity : float
        Fidelity of light-matter emission.
    T1 : float
        Relaxation time of the node's qubits
    T2 : float
        Dephasing time of the node's qubits
    cutoff_time : int or None
        Time in nanoseconds after which all links should be discarded.
        If None, not cutoff protocol is used.
    initialization_duration : float
        Time in ns needed to initialise a qubit in the memory.
    single_qubit_gate_duration : float
        Time in ns needed to perform a single qubit gate in the memory.
    two_qubit_gate_duration : float
        Time in ns needed to perform a two qubit gate in the memory.
    measurement_duration : float
        Time in ns needed to measure a qubit in the memory.
    emission_duration : float
        Time in ns needed to emit qubit from memory.
    ID : int, optional
      ID for this node.
    port_names : list of str or None, optional
        Names of additional ports to add to this component.
    bright_state_param : float, dict or None (optional)
        Either dictionary with keys = port_names and values = corresponding bright state parameter, or a float which is
        the bright state parameter to use in all connections.
    sequential_one_repeater : bool, optional
        If True, we assume that the node will be used in a 3-node chain capable of generating entanglement on only
        one side at a time and adjust the protocols accordingly.

    """

    def __init__(self, name, num_positions, end_node, num_parallel_links, num_communication_qubits,
                 swap_quality, T1, T2, cutoff_time, initialization_duration, single_qubit_gate_duration,
                 two_qubit_gate_duration, measurement_duration, ID=None, port_names=None, bright_state_param=None,
                 sequential_one_repeater=False, **kwargs):
        if "params" in kwargs:
            raise ValueError("'params' keyword no longer supported")
        super().__init__(name=name,
                         num_positions=num_positions,
                         swap_quality=swap_quality,
                         T1=T1,
                         T2=T2,
                         ID=ID,
                         port_names=port_names,
                         initialization_duration=initialization_duration,
                         single_qubit_gate_duration=single_qubit_gate_duration,
                         two_qubit_gate_duration=two_qubit_gate_duration,
                         measurement_duration=measurement_duration)

        if bright_state_param is not None:
            self.add_property("bright_state_param", bright_state_param)

        abstract_driver = Driver(name="abstract_driver")
        abstract_driver.add_service(MessageHandlerProtocol, MessageHandlerProtocol(self))
        if self.ID % 2 == 0:
            agreement_service = RespondingAgreementServiceWithClassicalConnections(node=self)
        else:
            agreement_service = InitiativeTakingAgreementServiceWithClassicalConnections(node=self)
        abstract_driver.add_service(EntanglementAgreementService, agreement_service)
        abstract_driver.add_service(EntanglementTrackerService, BellStateTracker(self))
        abstract_driver.add_service(EntanglementService,
                                    SingleMemoryEntanglementMagic(node=self,
                                                                  num_parallel_links=num_parallel_links))
        abstract_driver.add_service(MeasureService, AbstractMeasureService(node=self,
                                                                           name="AbstractMeasureService"))
        abstract_driver.add_service(SwapService, AbstractSwapService(node=self,
                                                                     name="AbstractSwapService"))
        abstract_driver.add_service(QuantumMemoryManager,
                                    MemoryManagerWithMoveProgram(node=self,
                                                                 size=num_positions,
                                                                 move_program=AbstractMoveProgram()))
        if end_node:
            abstract_driver.add_service(EGPService, SwapAsapEndNodeLinkLayerProtocol(node=self))
        else:
            if sequential_one_repeater:
                abstract_driver.add_service(
                    SwapASAPService,
                    SwapASAPOneSequentialRepeater(self,
                                                  num_communication_qubits=num_communication_qubits))
            else:
                abstract_driver.add_service(
                    SwapASAPService,
                    SwapASAP(self, num_communication_qubits=num_communication_qubits))
            if cutoff_time is not None:
                abstract_driver.add_service(CutoffService, CutoffTimer(node=self, cutoff_time=cutoff_time))
        self.add_subcomponent(abstract_driver)
        self.driver = list(self.subcomponents.filter_by_type(Driver).values())[0]


class NVNodeWithDriver(Node):
    """:class:`netsquid.nodes.node.Node` with an NV processor and a
     :class:`nlblueprint.processing_node_API.driver.Driver`. The following protocol implementations are already loaded:

     - Message handler service: :class:`nlblueprint.control_layer.message_handler_protocol.MessageHandlerProtocol`
     - Entanglement-tracker service: :class:`nlblueprint.entanglement_tracker.bell_state_tracker.BellStateTracker`
     - Entanglement-agreement service:
         - :class:`nlblueprint.control_layer.initiative_based_agreement_service.InitiativeTakingAgreementService`
         if the node ID is odd,
         - :class:`nlblueprint.control_layer.initiative_based_agreement_service.RespondingAgreementService`
         if the node ID is even.
     - Entanglement service:
         :class:`nlblueprint.processing_nodes.operation_services_nv.NVSingleMemoryEntanglementMagic`
     - Measurement service: :class:`nlblueprint.processing_nodes.operation_services_nv.MeasureServiceNV`
     - Swap service: :class:`nlblueprint.processing_nodes.operation_services_nv.SwapServiceNV`

     If this node is a repeater node, the following service is also loaded in:

     - SWAPASAPService: :class:`nlblueprint.control_layer.swap_asap.SwapASAP`

    Parameters
    ----------
    name : str
        Name for this node.
    electron_init_depolar_prob : float
        Probability of depolarizing electron qubit on initialization.
    electron_single_qubit_depolar_prob : float
        Probability of depolarizing electron qubit on single-qubit gate.
    prob_error_0 : float
        Measurement error probability: probability that |0> gives outcome "1".
    prob_error_1 : float
        Measurement error probability: probability that |1> gives outcome "0".
    carbon_init_depolar_prob : float
        Probability of depolarizing carbon qubit on initialization.
    carbon_z_rot_depolar_prob : float
        Probability of depolarizing carbon qubit on Z rotation.
    ec_gate_depolar_prob : float
        Probability of depolarizing electron and carbon qubits on 2-qubit gate.
    electron_T1 : float
        Relaxation time of electron qubit. [ns]
    electron_T2 : float
        Dephasing time of electron qubit. [ns]
    carbon_T1 : float
        Relaxation time of carbon qubit. [ns]
    carbon_T2 : float
        Dephasing time of carbon qubit. [ns]
    coherent_phase : float
        Coherent phase between superposition states of delivered single-click electron state. [rad]
    initial_nuclear_phase : float
        Phase of carbon qubit on initialization. [rad]
    p_double_exc : float
        Probability of exciting two photons on emission.
    p_fail_class_corr : float
        Probability of failure of correlations between electron and emitted photon.
    photon_emission_delay : float
        Delay after which photon is emitted. [ns]
    n1e : float
        Number of tolerated entanglement generation attempts before state in carbon qubit dephases.
    std_electron_electron_phase_drift : float
        Standard deviation of the phase introduced on the electron-electron state by interferometric drift.
    visibility : float
        Photon indistinguishability.
    carbon_init_duration : float
        Time [ns] it takes to initialize a carbon qubit.
    carbon_z_rot_duration : float
        Time [ns] it takes to perform a Z rotation on a carbon qubit.
    electron_init_duration : float
        Time [ns] it takes to initialize an electron qubit.
    electron_single_qubit_duration : float
        Time [ns] it takes to perform a single-qubit gate on an electron.
    ec_two_qubit_gate_duration : float
        Time [ns] it takes to perform a controlled electron-carbon two-qubit gate.
    measure_duration : float
        Time [ns] it takes to measure the state of the electron qubit.
    magical_swap_gate_duration : float
        Time [ns] it takes to perform a native swap gate.
    use_magical_swap : bool
        If True, endow node with native swap operation.
    num_positions : int
     Number of qubits in the processor.
    end_node : bool
     True if this node is an end node, False if it is a repeater.
    num_communication_qubits : int
     Number of a communication qubits the node has.
    cutoff_time : int or None
     Time in nanoseconds after which all links should be discarded. If None, not cutoff protocol is used.
    port_names : list of str or None, optional
     Names of additional ports to add to this component.
    bright_state_param : float, dict or None (optional)
        Either dictionary with keys = port_names and values = corresponding bright state parameter, or a float which is
        the bright state parameter to use in all connections.
    sequential_one_repeater : bool, optional
        If True, we assume that the node will be used in a 3-node chain capable of generating entanglement on only
        one side at a time and adjust the protocols accordingly.
     """

    def __init__(self, name, end_node, electron_init_depolar_prob, electron_single_qubit_depolar_prob,
                 prob_error_0, prob_error_1, carbon_init_depolar_prob, carbon_z_rot_depolar_prob,
                 ec_gate_depolar_prob, electron_T1, electron_T2, carbon_T1, carbon_T2, coherent_phase,
                 initial_nuclear_phase, p_double_exc, p_fail_class_corr, photon_emission_delay, n1e,
                 std_electron_electron_phase_drift, visibility, carbon_init_duration,
                 carbon_z_rot_duration, electron_init_duration, electron_single_qubit_duration,
                 ec_two_qubit_gate_duration, measure_duration, magical_swap_gate_duration, num_positions, cutoff_time,
                 use_magical_swap=False, port_names=None, bright_state_param=None, num_communication_qubits=1,
                 sequential_one_repeater=False, **kwargs):
        if "params" in kwargs:
            raise ValueError("'params' keyword no longer supported")

        product_tau_decay_delta_w = compute_product_tau_decay_delta_w_from_nodephasing_number(n1e, alpha=0.5)
        nv_processor = NVQuantumProcessor(
            num_positions=num_positions,
            electron_init_depolar_prob=electron_init_depolar_prob,
            electron_single_qubit_depolar_prob=electron_single_qubit_depolar_prob,
            prob_error_0=prob_error_0,
            prob_error_1=prob_error_1,
            carbon_init_depolar_prob=carbon_init_depolar_prob,
            carbon_z_rot_depolar_prob=carbon_z_rot_depolar_prob,
            ec_gate_depolar_prob=ec_gate_depolar_prob,
            electron_T1=electron_T1,
            electron_T2=electron_T2,
            carbon_T1=carbon_T1,
            carbon_T2=carbon_T2,
            coherent_phase=coherent_phase,
            initial_nuclear_phase=initial_nuclear_phase,
            p_double_exc=p_double_exc,
            p_fail_class_corr=p_fail_class_corr,
            photon_emission_delay=photon_emission_delay,
            tau_decay=product_tau_decay_delta_w,
            delta_w=1.,
            std_electron_electron_phase_drift=std_electron_electron_phase_drift,
            visibility=visibility,
            carbon_init_duration=carbon_init_duration,
            carbon_z_rot_duration=carbon_z_rot_duration,
            electron_init_duration=electron_init_duration,
            electron_single_qubit_duration=electron_single_qubit_duration,
            ec_two_qubit_gate_duration=ec_two_qubit_gate_duration,
            measure_duration=measure_duration,
            magical_swap_gate_duration=magical_swap_gate_duration,
            use_magical_swap=use_magical_swap)
        super().__init__(name=name, qmemory=nv_processor, port_names=port_names)

        if bright_state_param is not None:
            self.add_property("bright_state_param", bright_state_param)

        nv_driver = Driver(name="nv_driver")
        nv_driver.add_service(MessageHandlerProtocol, MessageHandlerProtocol(self))
        nv_driver.add_service(EntanglementTrackerService, BellStateTracker(self))
        if self.ID % 2 == 0:
            agreement_service = RespondingAgreementServiceWithClassicalConnections(node=self)
        else:
            agreement_service = InitiativeTakingAgreementServiceWithClassicalConnections(node=self)
        nv_driver.add_service(EntanglementAgreementService, agreement_service)

        nv_driver.add_service(EntanglementService, NVSingleMemoryEntanglementMagic(node=self))
        nv_driver.add_service(MeasureService, ProcessingNodeMeasureService(node=self,
                                                                           name="NVMeasureService",
                                                                           meas_prog=NVMeasureProgram))
        nv_driver.add_service(SwapService, NVSwapService(node=self))
        nv_driver.add_service(QuantumMemoryManager,
                              CommunicationMemoryManager(node=self,
                                                         communication_qubits=[0],
                                                         size=num_positions,
                                                         move_program=NVMoveProgram()))

        if end_node:
            nv_driver.add_service(EGPService, SwapAsapEndNodeLinkLayerProtocol(node=self))
        else:
            if sequential_one_repeater:
                nv_driver.add_service(
                    SwapASAPService,
                    SwapASAPOneSequentialRepeater(self,
                                                  num_communication_qubits=1))
            else:
                nv_driver.add_service(
                    SwapASAPService,
                    SwapASAP(self, num_communication_qubits=1))
            if cutoff_time is not None:
                nv_driver.add_service(CutoffService, CutoffTimer(node=self, cutoff_time=cutoff_time))
        self.add_subcomponent(nv_driver)
        self.driver = list(self.subcomponents.filter_by_type(Driver).values())[0]


class TINodeWithDriver(Node):
    """Trapped-ion processing node.

    :class:`netsquid.nodes.node.Node` with an :class:`netsquid_trappedions.ion_trap.IonTrap` quantum processor
    and a :class:`nlblueprint.processing_nodes.driver.Driver`.

    The service implementations included in this driver are:
    - Message handler service: :class:`nlblueprint.control_layer.message_handler_protocol.MessageHandlerProtocol`,
    - Entanglement-tracker service: :class:`nlblueprint.entanglement_tracker.bell_state_tracker.BellStateTracker`,
    - Entanglement-agreement service:
        - :class:`nlblueprint.control_layer.initiative_based_agreement_service.InitiativeTakingAgreementService`
        if the node ID is odd,
        - :class:`nlblueprint.control_layer.initiative_based_agreement_service.RespondingAgreementService`
        if the node ID is even,
    - Entanglement service:
        :class:`nlblueprint.operation_services.entanglement_magic.SingleMemoryEntanglementMagic`.

    The driver is configured with :class:`nlblueprint.operation_services.measurement_service.MeasureService`
    if the number of positions is 1 and
    :class:`nlblueprint.operation_services.measurement_services.SwapService` if the number of positions is 2.
    The reason for this is that the X and Y rotations required to execute these services act on all ions in the
    ion trap simultaneously, and thus performing these rotations on ion traps with different number of ions
    would have unintended side effects.

    Parameters
    ----------
    name : str
      Name for this node.
    num_positions : int
        Number of qubits in the processor.
    end_node : bool
        True if this node is an end node, False if it is a repeater.
    num_parallel_links : int
        Number of elementary links on which this node can generate entanglement simultaneously.
    num_communication_qubits : int
        Number of a communication qubits the node has.
    coherence_time : float
        Coherence time of the qubits.
        Qubits in memory decohere according to a collective-dephasing channel characterized by this coherence time.
        This channel is both strongly correlated between all qubits in memory and non-Markovian.
    cutoff_time : float or None
        Time in nanoseconds after which all links should be discarded.
        If None, not cutoff protocol is used.
    prob_error_0 : float
        Measurement error probability: probability that |0> gives outcome "1".
    prob_error_1 : float
        Measurement error probability: probability that |1> gives outcome "0".
    init_depolar_prob : float
        Parameter characterizing depolarizing channel that is applied to a qubit when it is initialized.
    rot_z_depolar_prob : float
        Parameter characterizing depolarizing channel that is applied to a qubit
        when a single-qubit z rotation is performed.
    multi_qubit_xy_depolar_prob : float
        Parameter characterizing depolarizing channel that is applied to all qubits participating in a
        multi-qubit rotation around an axis in the XY plane of the Bloch sphere.
    ms_depolar_prob : float
        Parameter charactherizing depolarizing channel that is applied to all qubits participating in a
        multi-qubit Mølmer–Sørensen gate, which is able to entangle qubits
        (this gate is the main ingredient for a Bell-state measurement).
    emission_fidelity : float
        Fidelity of the ion-photon entangled state directly after emitting a photon.
    collection_efficiency: float
        Probability that an entangled photon is successfully emitted when attempted.
    measurement_duration : float
        Time [ns] it takes to perform a single-qubit computational-basis measurement.
    initialization_duration : float
        Time [ns] it takes to initialize a qubit.
    z_rotation_duration : float
        Time [ns] it takes to perform a single-qubit z rotation.
    multi_qubit_xy_rotation_duration : float
        Time [ns] it takes to perform a multi-qubit XY rotation.
    ms_pi_over_2_duration : float
        Time [ns] it takes to perform a Mølmer–Sørensen gate with angle pi / 2.
        Durations for MS gates with larger angles are derived from this number.
    emission_duration : float
        Time [ns] it takes to attempt emitting an entangled photon.
    port_names : list of str or None, optional
        Names of additional ports to add to this component.
    ms_optimization_angle : float, optional
        Angle of Mølmer–Sørensen gate for which the device has been optimized.
    sequential_one_repeater : bool, optional
        If True, we assume that the node will be used in a 3-node chain capable of generating entanglement on only
        one side at a time and adjust the protocols accordingly.

    """
    def __init__(self, name, num_positions, end_node, num_parallel_links, num_communication_qubits, coherence_time,
                 cutoff_time, prob_error_0, prob_error_1, init_depolar_prob, rot_z_depolar_prob,
                 multi_qubit_xy_depolar_prob, ms_depolar_prob, emission_fidelity, collection_efficiency,
                 measurement_duration, initialization_duration, z_rotation_duration, multi_qubit_xy_rotation_duration,
                 ms_pi_over_2_duration, emission_duration, port_names=None, ms_optimization_angle=np.pi / 2,
                 sequential_one_repeater=False, **kwargs):
        if "params" in kwargs:
            raise ValueError("'params' keyword no longer supported")

        ion_trap = IonTrap(num_positions=num_positions,
                           coherence_time=coherence_time,
                           prob_error_0=prob_error_0,
                           prob_error_1=prob_error_1,
                           init_depolar_prob=init_depolar_prob,
                           rot_z_depolar_prob=rot_z_depolar_prob,
                           multi_qubit_xy_rotation_depolar_prob=multi_qubit_xy_depolar_prob,
                           ms_depolar_prob=ms_depolar_prob,
                           emission_fidelity=emission_fidelity,
                           collection_efficiency=collection_efficiency,
                           measurement_duration=measurement_duration,
                           initialization_duration=initialization_duration,
                           z_rotation_duration=z_rotation_duration,
                           ms_pi_over_2_duration=ms_pi_over_2_duration,
                           multi_qubit_xy_rotation_duration=multi_qubit_xy_rotation_duration,
                           emission_duration=emission_duration,
                           ms_optimization_angle=ms_optimization_angle,
                           )

        super().__init__(name=name, qmemory=ion_trap, port_names=port_names)

        ti_driver = Driver(name="ti_driver")
        ti_driver.add_service(MessageHandlerProtocol, MessageHandlerProtocol(self))
        if self.ID % 2 == 0:
            agreement_service = RespondingAgreementServiceWithClassicalConnections(node=self)
        else:
            agreement_service = InitiativeTakingAgreementServiceWithClassicalConnections(node=self)
        ti_driver.add_service(EntanglementAgreementService, agreement_service)
        ti_driver.add_service(EntanglementTrackerService, BellStateTracker(self))
        ti_driver.add_service(EntanglementService,
                              TISingleMemoryEntanglementMagic(node=self,
                                                              num_parallel_links=num_parallel_links))
        ti_driver.add_service(QuantumMemoryManager,
                              MemoryManagerWithMoveProgram(node=self,
                                                           size=num_positions,
                                                           move_program=AbstractMoveProgram()))

        if num_positions == 1:
            ti_driver.add_service(MeasureService, TIMeasureService(node=self, name="TIMeasureService"))
        if num_positions == 2:
            ti_driver.add_service(SwapService, TISwapService(node=self, name="TISwapService"))
        if end_node:
            ti_driver.add_service(EGPService, SwapAsapEndNodeLinkLayerProtocol(node=self))
        else:
            if sequential_one_repeater:
                ti_driver.add_service(
                    SwapASAPService,
                    SwapASAPOneSequentialRepeater(self,
                                                  num_communication_qubits=num_communication_qubits))
            else:
                ti_driver.add_service(
                    SwapASAPService,
                    SwapASAP(self, num_communication_qubits=num_communication_qubits))
            if cutoff_time is not None:
                ti_driver.add_service(CutoffService, CutoffTimer(node=self, cutoff_time=cutoff_time))
        self.add_subcomponent(ti_driver)
        self.driver = ti_driver


class DepolarizingNodeWithDriver(Node):
    """Simple quantum node with only depolarizing noise."""

    DEFAULT_PARAMS = {"end_node": False,
                      "num_positions": 2,
                      "coherence_time": 0,
                      "noiseless": False,
                      "port_names": None,
                      "cutoff_time": None,
                      "num_parallel_links": 1,
                      "num_communication_qubits": 2,
                      "sequential_one_repeater": False,
                      }

    def __init__(self, name, end_node=False, num_positions=2, coherence_time=0, port_names=None,
                 cutoff_time=None, num_parallel_links=1, num_communication_qubits=2, sequential_one_repeater=False,
                 **kwargs):
        if "params" in kwargs:
            raise ValueError("'params' keyword no longer supported")

        if coherence_time == 0:
            depolar_rate = 0
        else:
            depolar_rate = 1 / coherence_time * 1E9
        qprocessor = QuantumProcessor(name=f"processor_of_{name}",
                                      num_positions=num_positions,
                                      mem_noise_models=DepolarNoiseModel(depolar_rate=depolar_rate),
                                      fallback_to_nonphysical=True)

        super().__init__(name=name, qmemory=qprocessor, port_names=port_names)

        driver = Driver(name="depolarizing_node_driver")
        driver.add_service(MessageHandlerProtocol, MessageHandlerProtocol(self))
        if self.ID % 2 == 0:
            agreement_service = RespondingAgreementServiceWithClassicalConnections(node=self)
        else:
            agreement_service = InitiativeTakingAgreementServiceWithClassicalConnections(node=self)
        driver.add_service(EntanglementAgreementService, agreement_service)
        driver.add_service(EntanglementTrackerService, BellStateTracker(self))
        driver.add_service(EntanglementService,
                           SingleMemoryEntanglementMagic(node=self,
                                                         num_parallel_links=num_parallel_links))
        driver.add_service(MeasureService, ProcessingNodeMeasureService(node=self,
                                                                        name="DepolarizingNOdeMeasureService"))
        driver.add_service(SwapService, ProcessingNodeSwapService(node=self,
                                                                  name="DepolarizingNodeSwapService"))

        driver.add_service(QuantumMemoryManager,
                           MemoryManagerWithMoveProgram(node=self,
                                                        size=num_positions,
                                                        move_program=AbstractMoveProgram()))
        if end_node:
            driver.add_service(EGPService, SwapAsapEndNodeLinkLayerProtocol(node=self))
        else:
            if sequential_one_repeater:
                driver.add_service(
                    SwapASAPService,
                    SwapASAPOneSequentialRepeater(self,
                                                  num_communication_qubits=num_communication_qubits))
            else:
                driver.add_service(
                    SwapASAPService,
                    SwapASAP(self, num_communication_qubits=num_communication_qubits))
            if cutoff_time is not None:
                driver.add_service(CutoffService, CutoffTimer(node=self, cutoff_time=cutoff_time))
        self.add_subcomponent(driver)
        self.driver = list(self.subcomponents.filter_by_type(Driver).values())[0]
