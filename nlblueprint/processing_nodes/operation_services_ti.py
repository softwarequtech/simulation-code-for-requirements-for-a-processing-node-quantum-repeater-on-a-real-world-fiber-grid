from netsquid.components import QuantumProgram, INSTR_MEASURE
from netsquid_trappedions.instructions import IonTrapMultiQubitRotation
from netsquid_trappedions.ion_trap import IonTrap
from netsquid_trappedions.programs import IonTrapSwapProgram
import numpy as np

from nlblueprint.processing_nodes.entanglement_magic import SingleMemoryEntanglementMagic
from nlblueprint.processing_nodes.measurement_services_processing_nodes import ProcessingNodeMeasureService, \
    ProcessingNodeSwapService


class TIMeasureProgram(QuantumProgram):
    """Implements arbitrary-basis single-qubit measurement circuit on a trapped-ion node.

    The measurement is done in any desired basis by first performing a rotation according to the
    specified Euler angles.
    This program only works on ion traps with a single ionic qubit, since X and Y rotations can only be performed
    on all ions simultaneously.

    """

    _rotation_instruction = IonTrapMultiQubitRotation(num_positions=1)  # only works for ion trap with one ion

    def program(self, x_rotation_angle_1, y_rotation_angle, x_rotation_angle_2):
        qubit_to_meas = self.get_qubit_indices(1)
        self.apply(instruction=self._rotation_instruction, qubit_indices=qubit_to_meas,
                   theta=x_rotation_angle_1, phi=0)
        self.apply(instruction=self._rotation_instruction, qubit_indices=qubit_to_meas,
                   theta=y_rotation_angle, phi=np.pi / 2)
        self.apply(instruction=self._rotation_instruction, qubit_indices=qubit_to_meas,
                   theta=x_rotation_angle_2, phi=0)
        self.apply(instruction=INSTR_MEASURE, qubit_indices=qubit_to_meas, output_key="outcome")
        yield self.run()


class TISwapProgram(IonTrapSwapProgram):

    @property
    def outcome_as_netsquid_bell_index(self):  # required by the way service is set up
        return self.get_outcome_as_bell_index


class TIMeasureService(ProcessingNodeMeasureService):
    """Service for performing a single-qubit measurement on a ion-trap node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the measurement is registered at the entanglement tracker.

    After measuring, :meth:`~netsquid_trappedions.ion_trap.IonTrap.resample()` is called on the node's ion trap.
    This is needed to reproduce correct statistics for memory decoherence.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'TIMeasureService'.

    Raises
    ------
    RunTimeError
        If the node's quantum memory is not a :class:`netsquid_trappedions.ion_trap.IonTrap`.

    """
    def __init__(self, node, name="TIMeasureService"):
        super().__init__(node=node, name=name, meas_prog=TIMeasureProgram)

    def start(self):
        if not isinstance(self.node.qmemory, IonTrap):
            raise RuntimeError("Cannot run TISwapService on a node that doesn't hold an IonTrap.")
        super().start()

    def _perform_measurement(self):
        yield from super()._perform_measurement()
        self.node.qmemory.resample()


class TISwapService(ProcessingNodeSwapService):
    """Service for entanglement swapping (i.e. performing a Bell-state measurement) on an ion-trap node.

    If the node this service runs on has a `driver.Driver` as `driver` attribute (i.e. `node.driver`),
    and this service has an `services.entanglement_tracker_service.EntanglementTrackerService` registered,
    the swap is registered at the entanglement tracker.

    If a discard is registered at the local `EntanglementTrackerService` while the swap program is being executed,
    the swap is considered to have failed, and all participating qubits are discarded (if they weren't already).

    After swapping, :meth:`~netsquid_trappedions.ion_trap.IonTrap.resample()` is called on the node's ion trap.
    This is needed to reproduce correct statistics for memory decoherence.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol runs on.
    name : str, optional.
        The name of this protocol. Default 'TISwapService'.

    Raises
    ------
    RunTimeError
        If the node's quantum memory is not a :class:`netsquid_trappedions.ion_trap.IonTrap`.

    """
    def __init__(self, node, name="TISwapService"):
        super().__init__(node=node, name=name, swap_prog=TISwapProgram)

    def start(self):
        if not isinstance(self.node.qmemory, IonTrap):
            raise RuntimeError("Cannot run TISwapService on a node that doesn't hold an IonTrap.")
        super().start()

    def _perform_swap(self):
        yield from super()._perform_swap()
        self.node.qmemory.resample()


class TISingleMemoryEntanglementMagic(SingleMemoryEntanglementMagic):
    """Version of :class:`SingleMemoryEntanglementMagic` that resamples the ion trap's rotation rate at state delivery.

    Exactly the same as :class:`SingleMemoryEntanglementMagic`, except for the fact that each time when a state
    is delivered :meth:`~netsquid_trappedions.ion_trap.IonTrap.resample()` is called.
    This is needed to reproduce correct statistics for memory decoherence.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    num_parallel_links : int or None (optional)
        Number of parallel entanglement-generation attempts supported by this protocol.
        Set to None if there if no limit. Default: None.
    name : str, optional

    """
    def _handle_state_delivery_factory(self, port_name, mem_pos, **kwargs):
        """Creates callback function for :class:`netsquid_magic.magic_distributor.MagicDistributor` which heralds the
        fact that the state is ready and calls :meth:`~netsquid_trappedions.ion_trap.IonTrap.resample()`.

        Parameters
        ----------
        port_name : str
            Name of the port that entanglement is generated over for which the callback function is tailored.
        mem_pos : int
            Memory position that entanglement is generated on for which the callback function is tailored.

        """
        handle_state_delivery = super()._handle_state_delivery_factory(port_name=port_name,
                                                                       mem_pos=mem_pos,
                                                                       **kwargs)

        def handle_state_delivery_and_resample(**handle_state_delivery_kwargs):
            self.node.qmemory.resample()
            return handle_state_delivery(**handle_state_delivery_kwargs)

        return handle_state_delivery_and_resample
