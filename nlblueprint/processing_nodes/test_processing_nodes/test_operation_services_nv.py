import netsquid as ns
from netsquid.qubits.ketstates import BellIndex
from netsquid.components import INSTR_INIT, INSTR_H, INSTR_CNOT, INSTR_I, INSTR_X, INSTR_Y, INSTR_Z
from nlblueprint.processing_nodes.nodes_with_drivers import NVNodeWithDriver, nv_standard_params
from nlblueprint.processing_nodes.test_operation_services.test_measurement_services_processing_nodes import (
    MockLinkLayerSwap,
    _test_measure_service)

nv_params = dict(nv_standard_params)
del nv_params["num_positions"]


def test_measure_service_nv():
    node = NVNodeWithDriver(name="TestNVNode", end_node=True, num_positions=1, **nv_params)
    _test_measure_service(node=node)


def test_swap_service_nv():
    """Test if a swap service performs as expected. NV is initialized in Bell state B00.
    Swap service is run and we check if the obtained Bell state is the expected.

    An extra Hadamard is applied to position 1, corresponding to the carbon, before the swap happens. This is because
    the NV swap program :class:`~nlblueprint.processing_nodes.operation_services_nv.NVSwapProgram` assumes that the
    carbons live in a basis rotated by H from the computational basis. Therefore, if this Hadamard is not applied, the
    wrong bell index will be obtained and the test fails.
    """

    # which rotation to apply from |00>+|11> to the
    # other Bell states
    rot_instr2bell_index = {INSTR_I: BellIndex.PHI_PLUS,
                            INSTR_X: BellIndex.PSI_PLUS,
                            INSTR_Z: BellIndex.PHI_MINUS,
                            INSTR_Y: BellIndex.PSI_MINUS}
    node = NVNodeWithDriver(name="TestNVNode", end_node=False, num_positions=2, **nv_params)
    driver = node.driver
    for instr, bell_index in rot_instr2bell_index.items():
        ns.sim_reset()

        INSTR_INIT(node.qmemory, positions=[0, 1])
        INSTR_H(node.qmemory, positions=[0])
        INSTR_CNOT(node.qmemory, positions=[0, 1])
        instr(node.qmemory, positions=[0])
        INSTR_H(node.qmemory, positions=[1])

        link_layer = MockLinkLayerSwap(driver=driver)
        link_layer.start()
        ns.sim_run()

        assert(link_layer.res_outc.outcome) == bell_index
        assert(link_layer.finished)
