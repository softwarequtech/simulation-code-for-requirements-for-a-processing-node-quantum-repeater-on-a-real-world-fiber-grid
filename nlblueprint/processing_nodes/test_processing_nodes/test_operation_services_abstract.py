from nlblueprint.processing_nodes.nodes_with_drivers import AbstractNodeWithDriver, abstract_node_standard_params
from nlblueprint.processing_nodes.test_operation_services.test_measurement_services_processing_nodes import \
    _test_measure_service, _test_swap_service

abstract_node_params = dict(abstract_node_standard_params)
del abstract_node_params["num_positions"]


def test_measure_service_abstract():
    node = AbstractNodeWithDriver(name="TestAbstractNode", num_positions=1, end_node=False, **abstract_node_params)
    _test_measure_service(node=node)


def test_swap_service_abstract():
    node = AbstractNodeWithDriver(name="TestAbstractNode", num_positions=2, end_node=False, **abstract_node_params)
    _test_swap_service(node=node)
