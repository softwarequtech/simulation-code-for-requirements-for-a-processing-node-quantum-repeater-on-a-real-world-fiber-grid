from nlblueprint.processing_nodes.nodes_with_drivers import AbstractNodeWithDriver, TINodeWithDriver, \
    DepolarizingNodeWithDriver, NVNodeWithDriver, abstract_node_standard_params, ti_standard_params, nv_standard_params
from netsquid_driver.driver import Driver


def test_abstract_node_with_driver():
    abstract_node_with_driver = AbstractNodeWithDriver(name="TestAbstractNode", end_node=False,
                                                       **abstract_node_standard_params)
    assert isinstance(abstract_node_with_driver, AbstractNodeWithDriver)
    assert isinstance(abstract_node_with_driver.driver, Driver)


def test_nv_node_with_driver():
    nv_node_with_driver = NVNodeWithDriver(name="TestNVNode", end_node=False, **nv_standard_params)
    assert isinstance(nv_node_with_driver, NVNodeWithDriver)
    assert isinstance(nv_node_with_driver.driver, Driver)


def test_abstract_node_with_driver_properties():
    abstract_node_params = dict(abstract_node_standard_params)
    del abstract_node_params["bright_state_param"]
    bright_state_param = {"ENT_A": 0.1, "ENT_B": 0.2}
    abstract_node_with_driver = AbstractNodeWithDriver(name="TestAbstractNode", end_node=False,
                                                       bright_state_param=bright_state_param,
                                                       **abstract_node_params)
    assert isinstance(abstract_node_with_driver, AbstractNodeWithDriver)
    assert abstract_node_with_driver.properties.get("bright_state_param") == bright_state_param


def test_nv_node_with_driver_properties():
    nv_node_params = dict(nv_standard_params)
    del nv_node_params["bright_state_param"]
    bright_state_param = {"ENT_A": 0.1, "ENT_B": 0.2}
    nv_node_with_driver = NVNodeWithDriver(name="TestNVNode", end_node=False,
                                           bright_state_param=bright_state_param, **nv_node_params)
    assert isinstance(nv_node_with_driver, NVNodeWithDriver)
    assert nv_node_with_driver.properties.get("bright_state_param") == bright_state_param


def test_ti_node_with_driver():
    ti_node_with_driver = TINodeWithDriver(name="TestTINode", end_node=False, **ti_standard_params)
    assert isinstance(ti_node_with_driver, TINodeWithDriver)
    assert isinstance(ti_node_with_driver.driver, Driver)


def test_depolarizing_node_with_driver():
    depolarizing_node_with_driver = DepolarizingNodeWithDriver(name="TestDepolarizingNode")
    assert isinstance(depolarizing_node_with_driver, DepolarizingNodeWithDriver)
    assert isinstance(depolarizing_node_with_driver.driver, Driver)
