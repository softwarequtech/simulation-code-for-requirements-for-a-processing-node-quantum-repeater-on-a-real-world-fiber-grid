from nlblueprint.processing_nodes.nodes_with_drivers import TINodeWithDriver, ti_standard_params
from nlblueprint.processing_nodes.test_operation_services.test_measurement_services_processing_nodes import \
    _test_measure_service, _test_swap_service

ti_params = dict(ti_standard_params)
del ti_params["num_positions"]


def test_measure_service_ti():
    node = TINodeWithDriver("TestTINode", end_node=False, num_positions=1, **ti_params)
    _test_measure_service(node)


def test_swap_service_ti():
    node = TINodeWithDriver("TestTINode", end_node=False, num_positions=2, **ti_params)
    _test_swap_service(node)
