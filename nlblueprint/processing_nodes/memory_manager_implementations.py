from netsquid_driver.memory_manager_service import QuantumMemoryManager
from netsquid.components import QuantumProgram


class MemoryManagerUsingInUseFlag(QuantumMemoryManager):
    """Implementation of a quantum memory manager using the `in_use` flag of
    :class:`~netsquid.components.qmemory.MemoryPosition` to mark memory positions as free or used.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on
    name : str, optional
        The name of this protocol
    size : int, optional
        Number of positions in this memory
    """

    def free_position(self, index):
        """Frees a position by setting its `in_use` flag to False. If the position was already free nothing happens.

        Parameters
        ----------
        index : int
            The memory position index to free.
        """
        super().free_position(index)
        self.node.qmemory.mem_positions[index].in_use = False


class MemoryManagerWithMoveProgram(MemoryManagerUsingInUseFlag):
    """Implementation of a quantum memory manager using a quantum program to perform moves.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on
    name : str, optional
        The name of this protocol
    move_program : :class:`netsquid.components.qprogram.QuantumProgram`
        Quantum program to be used to perform moves
    size : int, optional
        Number of positions in this memory
    """

    def __init__(self, node, move_program, size=None, name=None):
        super().__init__(node=node, size=size, name=name)
        self.move_program = move_program

    @property
    def move_program(self):
        """Program used to perform moves.
        """
        return self._move_program

    @move_program.setter
    def move_program(self, value):
        if not isinstance(value, QuantumProgram):
            raise TypeError("{} is not a QuantumProgram".format(value))
        self._move_program = value

    def move(self, from_memory_position_id, to_memory_position_id=None):
        """Moves a qubit from one position to the other using the defined quantum program.

        If the target position is `None`, moves to an arbitrary free position.

        Parameters
        ----------
        from_memory_position_id : int
            Index of memory position from where state should be moved.
        to_memory_position_id : int or None (optional)
            Index of memory position to where state should be moved. If None, an arbitrary free position will be chosen.

        """
        super().move(from_memory_position_id=from_memory_position_id, to_memory_position_id=to_memory_position_id)
        qubit_mapping = [from_memory_position_id, to_memory_position_id]
        self.node.qmemory.execute_program(self.move_program, qubit_mapping=qubit_mapping)


class CommunicationMemoryManager(MemoryManagerWithMoveProgram):
    """Implementation of a quantum memory manager tailor-made for devices with specialized communication qubits.
    Includes a quantum program used for moving states between the device's qubits.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    move_program : :class:`netsquid.components.qprogram.QuantumProgram`
        Quantum program to be used to perform moves.
    communication_qubits : list
        List containing indices of memory positions corresponding to communication qubits.
    size : int, optional
        Number of positions in this memory.
    name : str, optional
        The name of this protocol.

    """

    def __init__(self, node, move_program, communication_qubits, size=None, name=None):
        super().__init__(node=node, size=size, move_program=move_program, name=name)
        if isinstance(communication_qubits, int):
            communication_qubits = [communication_qubits]

        self.communication_qubits = communication_qubits
        self.memory_qubits = [x for x in range(self.size) if x not in self.communication_qubits]

    def move(self, from_memory_position_id, to_memory_position_id=None):
        """Moves a qubit from one position to the other using the defined quantum program.

        If the target position is `None`, moves to an arbitrary free position.

        Parameters
        ----------
        from_memory_position_id : int
            Index of memory position from where state should be moved.
        to_memory_position_id : int or None (optional)
            Index of memory position to where state should be moved. If None, an arbitrary free position will be chosen.

        """
        if to_memory_position_id is None:
            to_memory_position_id = self._get_free_memory_pos()
        if to_memory_position_id == -1:
            raise ValueError("No free position of type requested for move.")

        super().move(from_memory_position_id=from_memory_position_id, to_memory_position_id=to_memory_position_id)

    def _get_free_memory_pos(self):
        """Same functionality as method `_get_free_pos` of parent class, but returns the index of a position
        corresponding to a memory qubit specifically.
        """
        free_pos = [pos for pos in self.node.qmemory.mem_positions if not pos.in_use]
        free_pos_indices = [self._position_to_index(pos) for pos in free_pos]
        free_mem_pos = [pos for pos in free_pos_indices if pos in self.memory_qubits]
        try:
            pos_id = free_mem_pos[0]
            return pos_id

        except IndexError:
            return -1

    @property
    def num_communication_qubits(self):
        """
        Returns
        -------
        int
            Number of communication qubits in quantum memory.
        """
        return len(self.communication_qubits)

    @property
    def num_memory_qubits(self):
        """
        Returns
        -------
        int
            Number of memory qubits in quantum memory.
        """
        return len(self.memory_qubits)
