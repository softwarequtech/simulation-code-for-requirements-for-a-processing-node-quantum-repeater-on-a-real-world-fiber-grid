from netsquid_magic.magic_distributor import MagicDistributor
from netsquid_driver.entanglement_service import SingleMemoryHeraldedEntanglementBaseProtocol, \
    HeraldedEntanglementBaseProtocol, ResStateReady, EntanglementServiceWithConnections


class EntanglementMagic(HeraldedEntanglementBaseProtocol, EntanglementServiceWithConnections):
    """Magic implementation of :class:`EntanglementService` for heralded entanglement generation.

    For this protocol to work, gates over which entanglement is to be generated must each be connected to a
    :class:`netsquid.nodes.connections.Connection` that has the attribute `connection.magic_distributor` with as value a
    :class:`netsquid_magic.magic_distributor.MagicDistributor`.
    When entanglement is requested, the protocol will call
    :meth:`netsquid_magic.magic_distributor.MagicDistributor.add_pair_request`.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    num_parallel_links : int or None (optional)
        Number of parallel entanglement-generation attempts supported by this protocol.
        Set to None if there if no limit. Default: None.
    name : str, optional
        The name of this protocol.

    """

    def __init__(self, node, num_parallel_links=None, name=None):
        super().__init__(node=node, num_parallel_links=num_parallel_links, name=name)
        self._set_callbacks_md = []  # track for which (port_name, mem_pos) tuples a callback was set in magic

    def attempt_entanglement_generation(self, port_name, mem_pos):
        """Perform a single attempt at entanglement generation.

        Makes a single call to :meth:`netsquid_magic.magic_distributor.MagicDistributor.add_pair_request` of the
        :class:`netsquid_magic.magic_distributor.MagicDistributor` corresponding to the connection that is connected
        to the used port.

        Parameters
        ----------
        port_name : str
            Name of the port that should be used to attempt entanglement generation.
        mem_pos : int
            Memory position that should be used to attempt entanglement generation.

        """

        super().attempt_entanglement_generation(port_name, mem_pos)
        magic_distributor = self._get_magic_distributor(port_name)
        magic_distributor.add_pair_request(senderID=self.node.ID,
                                           remoteID=self.get_remote_node_id(port_name),
                                           pos_sender=mem_pos,
                                           port_sender=port_name)

        # If not done yet, register a callback to the magic distributor to handle state delivery.
        if (port_name, mem_pos) not in self._set_callbacks_md:
            magic_distributor.add_callback(self._handle_state_delivery_factory(port_name=port_name,
                                                                               mem_pos=mem_pos))
            self._set_callbacks_md.append((port_name, mem_pos))

    def abort(self, req):
        """Abort entanglement generation.

        This method ensures that the magic distributor associated to the port specified in the request aborts its
        deliveries.

        Parameters
        ----------
        req : :class:`ReqEntanglementAbort`
            Request that needs to be handled by this method.

        """

        super().abort(req)
        magic_distributor = self._get_magic_distributor(req.port_name)
        magic_distributor.abort_all_delivery()

    def _handle_state_delivery_factory(self, port_name, mem_pos, **kwargs):
        """Creates callback function for :class:`netsquid_magic.magic_distributor.MagicDistributor` which heralds the
        fact that the state is ready.

        Parameters
        ----------
        port_name : str
            Name of the port that entanglement is generated over for which the callback function is tailored.
        mem_pos : int
            Memory position that entanglement is generated on for which the callback function is tailored.

        Notes
        -----
        Callbacks cannot easily be unset in a :class:`netsquid_magic.magic_distributor.MagicDistributor`.
        Therefore, we don't try to unset callbacks after a request has been fulfilled successfully.
        Instead, the callback function checks each time it is called whether it should actually do something
        or not.

        """
        def handle_state_delivery(**handle_state_delivery_kwargs):
            subprot = self._get_subprot_by_port_name(port_name)  # subprotocol performing ent gen for this port
            if subprot.req.mem_pos != mem_pos:
                #  the protocol is currently not generating entanglement on port_name for mem_pos,
                #  so callback can be ignored
                return
            if not self.is_running or not subprot.is_running:
                #  protocol or subprotocol not running, so callback can be ignored
                return
            self.send_response(ResStateReady(port_name=port_name,
                                             mem_pos=mem_pos))
        return handle_state_delivery

    def _get_magic_distributor(self, port_name):
        """Retrieve magic distributor associated to the connection connected to specific port.

        Parameters
        ----------
        port_name : str
            Name of port that the magic distributor should be retrieved for.

        """

        connection = self._get_connection(port_name)
        try:
            magic_distributor = connection.magic_distributor
        except AttributeError:
            raise TypeError(f"Connection {connection} does not have an associated magic distributor.")
        if not isinstance(magic_distributor, MagicDistributor):
            raise TypeError(f"connection.magic_distributor, {magic_distributor}, is not a magic distributor.")
        return magic_distributor


class SingleMemoryEntanglementMagic(EntanglementMagic, SingleMemoryHeraldedEntanglementBaseProtocol):
    """Magic implementation of :class:`EntanglementService` for heralded entanglement generation with single memory.

    For this protocol to work, gates over which entanglement is to be generated must each be connected to a
    :class:`netsquid.nodes.connections.Connection` that has the attribute `connection.magic_distributor` with as value a
    :class:`netsquid_magic.magic_distributor.MagicDistributor`.
    When entanglement is requested, the protocol will call
    :meth:`netsquid_magic.magic_distributor.MagicDistributor.add_pair_request`.

    Parameters
    ----------
    node : :class:`~netsquid.nodes.node.Node`
        The node this protocol is running on.
    num_parallel_links : int or None (optional)
        Number of parallel entanglement-generation attempts supported by this protocol.
        Set to None if there if no limit. Default: None.
    name : str, optional
        The name of this protocol.

    """
    pass
