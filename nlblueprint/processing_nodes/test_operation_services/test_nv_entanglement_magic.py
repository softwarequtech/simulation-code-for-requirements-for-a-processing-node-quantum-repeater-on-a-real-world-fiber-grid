import netsquid as ns
from netsquid_magic.state_delivery_sampler import PerfectStateSamplerFactory
from netsquid.components.qprogram import QuantumProgram
from netsquid.components.instructions import INSTR_SWAP, INSTR_INIT
from netsquid.qubits.qubitapi import reduced_dm
from netsquid.qubits.ketstates import b00
import numpy as np
from netsquid.nodes.connections import Connection
from netsquid_driver.entanglement_service import ReqEntanglement, EntanglementService, \
    MessageHandlerProtocol
import logging
from nlblueprint.processing_nodes.test_operation_services.test_entanglement_magic import \
    MagicDistributorWithRightMeta, SimpleEntanglementAgreementService
from nlblueprint.processing_nodes.operation_services_nv import NVSingleMemoryEntanglementMagic
from nlblueprint.processing_nodes.nodes_with_drivers import NVNodeWithDriver, nv_standard_params
from netsquid_driver.entanglement_agreement_service import EntanglementAgreementService


ns.logger.setLevel(logging.DEBUG)

# durations of the 'electron->carbon move' and the total duration of entanglement generation
# (which we model here as deterministic, i.e. the attempt success probability is 1)
# all durations given in nanoseconds
MAGICAL_SWAP_DURATION = 1000010
INIT_DURATION = 310000
MOVE_DURATION = MAGICAL_SWAP_DURATION + INIT_DURATION
ENTGEN_DELAY = 20

# duration of the last (and thus successful) entanglement generation
# attempt.
_LABEL_DELAY = 3
_STATE_DELAY = ENTGEN_DELAY - _LABEL_DELAY
NUMBER_OF_MEMORY_POSITIONS = 10

nv_params = dict(nv_standard_params)
nv_params["magical_swap_gate_duration"] = MAGICAL_SWAP_DURATION
nv_params["carbon_init_duration"] = INIT_DURATION
nv_params["num_positions"] = NUMBER_OF_MEMORY_POSITIONS
nv_params["use_magical_swap"] = True
nv_params["magical_swap_gate_duration"] = MAGICAL_SWAP_DURATION
nv_params["carbon_init_duration"] = INIT_DURATION


class PerfectMoveProgram(QuantumProgram):

    def program(self):
        electron, carbon = self.get_qubit_indices(2)
        self.apply(INSTR_INIT, [carbon])
        self.apply(INSTR_SWAP, [electron, carbon])
        yield self.run()


def _get_prefab_NV_node_by_node_name(node_name):
    node = NVNodeWithDriver(name='test_nv_node_{}'.format(node_name), end_node=True, **nv_params)
    mock_entanglement_service = NVSingleMemoryEntanglementMagic(node=node, name=f"entanglement_magic_{node_name}")
    node.driver.add_service(EntanglementAgreementService, SimpleEntanglementAgreementService(node=node))

    mock_entanglement_service.move_program = PerfectMoveProgram()
    node.driver.add_service(EntanglementService,
                            mock_entanglement_service)
    return node


def add_magic_distributor_to_connection(connection, list_of_nodes):
    connection.magic_distributor = MagicDistributorWithRightMeta(delivery_sampler_factory=PerfectStateSamplerFactory(),
                                                                 nodes=list_of_nodes,
                                                                 cycle_time=100,
                                                                 state_delay=_STATE_DELAY,
                                                                 label_delay=_LABEL_DELAY)


def setup_twonode_network():
    node_names = ["alice", "bob"]
    nodes = {node_name: _get_prefab_NV_node_by_node_name(node_name=node_name) for node_name in node_names}

    connection = Connection("test_magic_entanglement_connection")
    nodes["alice"].connect_to(remote_node=nodes["bob"], connection=connection,
                              local_port_name="to_bob", remote_port_name="to_alice")
    for name in ["alice", "bob"]:
        nodes[name].driver.add_service(MessageHandlerProtocol, MessageHandlerProtocol(nodes[name]))
        nodes[name].driver[MessageHandlerProtocol].start()
    add_magic_distributor_to_connection(connection=connection, list_of_nodes=list(nodes.values()))
    return nodes


def setup_threenode_network():
    node_names = ["alice", "bob", "charlie"]
    nodes = {node_name: _get_prefab_NV_node_by_node_name(node_name=node_name) for node_name in node_names}

    # setup connection alice <-> bob
    connection_alice_and_bob = Connection("alice_and_bob")

    nodes["alice"].connect_to(remote_node=nodes["bob"], connection=connection_alice_and_bob,
                              local_port_name="to_bob", remote_port_name="to_alice")

    add_magic_distributor_to_connection(connection=connection_alice_and_bob,
                                        list_of_nodes=[nodes["alice"], nodes["bob"]])

    # setup connection bob <-> charlie
    connection_bob_and_charlie = Connection("bob_and_charlie")
    nodes["charlie"].connect_to(remote_node=nodes["bob"], connection=connection_bob_and_charlie,
                                local_port_name="to_bob", remote_port_name="to_charlie")
    add_magic_distributor_to_connection(connection=connection_bob_and_charlie,
                                        list_of_nodes=[nodes["charlie"], nodes["bob"]])

    for name in ["alice", "bob"]:
        nodes[name].driver.add_service(MessageHandlerProtocol, MessageHandlerProtocol(nodes[name]))
        nodes[name].driver[MessageHandlerProtocol].start()

    return nodes


def _test_arbitrary_number_of_entanglement_requests(number_of_requests):

    ns.sim_reset()

    nodes = setup_twonode_network()

    # ensure alice and bob will attempt to generation `number_of_requests` number of entangled pairs
    reqs_alice = [ReqEntanglement(port_name="to_bob", mem_pos=0)] * number_of_requests
    reqs_bob = [ReqEntanglement(port_name="to_alice", mem_pos=0)] * number_of_requests

    for req in reqs_alice:
        nodes["alice"].driver[EntanglementService].put(req)

    for req in reqs_bob:
        nodes["bob"].driver[EntanglementService].put(req)

    # run simulation
    ns.sim_run()

    # check whether all necessary operations were applied
    # by checking the time the operation should have taken
    expected_time = number_of_requests * ENTGEN_DELAY + (number_of_requests - 1) * MOVE_DURATION
    assert(ns.sim_time() == expected_time)

    # check whether the following entanglement is shared:
    # Alice     Bob
    # 0  <-------> 0
    # 1  <-------> 1
    # 2  <-------> 2
    # ...
    # and so on until `number_of_requests`
    qubit_positions_at_which_we_expect_entanglement = list(range(number_of_requests))
    qubits_by_node_name = {}
    for name, node in nodes.items():
        qubits_by_node_name[name] = node.qmemory.peek(positions=qubit_positions_at_which_we_expect_entanglement)
    for qubit_a, qubit_b in zip(qubits_by_node_name["alice"], qubits_by_node_name["bob"]):
        qubits = [qubit_a, qubit_b]
        assert np.isclose(reduced_dm(qubits), np.outer(b00, b00)).all()


def test_single_entanglement_request():
    _test_arbitrary_number_of_entanglement_requests(number_of_requests=1)


def test_two_entanglement_requests():
    _test_arbitrary_number_of_entanglement_requests(number_of_requests=2)


def test_three_entanglement_requests():
    _test_arbitrary_number_of_entanglement_requests(number_of_requests=3)


def test_repeaterlike_entanglement_request():
    # test the following situation:
    # 1. alice and bob generate entanglement, followed by
    # 2. bob and charlie generate entanglement

    ns.sim_reset()

    nodes = setup_threenode_network()

    # ask for single entangled pair, twice:
    # 1) alice <-> bob
    # 2) bob <-> charlie
    reqs_alice = [ReqEntanglement(port_name="to_bob", mem_pos=0)]
    reqs_bob = [ReqEntanglement(port_name="to_alice", mem_pos=0),
                ReqEntanglement(port_name="to_charlie", mem_pos=0)]
    reqs_charlie = [ReqEntanglement(port_name="to_bob", mem_pos=0)]

    for req in reqs_alice:
        nodes["alice"].driver[EntanglementService].put(req)

    for req in reqs_bob:
        nodes["bob"].driver[EntanglementService].put(req)

    for req in reqs_charlie:
        nodes["charlie"].driver[EntanglementService].put(req)

    # run simulation
    ns.sim_run()

    # check whether all necessary operations were applied
    # by checking the time the operation should have taken
    expected_time = 2 * ENTGEN_DELAY + MOVE_DURATION
    assert(ns.sim_time() == expected_time)

    # check whether the following entanglement is shared:
    #             Alice     Bob            Charlie
    # "Pair 1")              0 <-------------> 0
    # "Pair 2")   0  <-----> 1

    # check "Pair 1"
    [qubit_bob] = nodes["bob"].qmemory.peek(positions=[0])
    [qubit_charlie] = nodes["charlie"].qmemory.peek(positions=[0])
    qubits = [qubit_charlie, qubit_bob]
    assert np.isclose(reduced_dm(qubits), np.outer(b00, b00)).all()

    # check "Pair 2"
    [qubit_alice] = nodes["alice"].qmemory.peek(positions=[0])
    [qubit_bob] = nodes["bob"].qmemory.peek(positions=[1])
    qubits = [qubit_alice, qubit_bob]
    assert np.isclose(reduced_dm(qubits), np.outer(b00, b00)).all()
