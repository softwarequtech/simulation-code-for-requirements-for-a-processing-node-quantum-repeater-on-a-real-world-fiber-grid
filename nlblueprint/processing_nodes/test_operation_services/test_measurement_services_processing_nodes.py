import numpy as np
import netsquid as ns
from netsquid.qubits.ketstates import BellIndex
from netsquid.components.instructions import INSTR_MEASURE_BELL
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.components.qprogram import QuantumProgram
from netsquid.nodes.node import Node
from netsquid.protocols import Protocol
from netsquid.components import INSTR_INIT, INSTR_H, INSTR_CNOT, INSTR_I, INSTR_X, INSTR_Y, INSTR_Z, INSTR_ROT, \
    INSTR_MEASURE
from netsquid_driver.driver import Driver
from netsquid_entanglementtracker.local_link_tracker import LocalLinkTracker
from nlblueprint.processing_nodes.measurement_services_processing_nodes import ProcessingNodeSwapService, \
    ProcessingNodeMeasureService
from netsquid_entanglementtracker.entanglement_tracker_service import EntanglementTrackerService, LinkStatus
from netsquid_driver.measurement_services import MeasureService, ResMeasure, SwapService, ResSwap, \
    ReqSwap, ReqMeasure


class MockLinkLayerMeasure(Protocol):
    def __init__(self, driver):
        super().__init__(name="mock_link_layer_measure")
        self._driver = driver
        self._measure_service = self._driver[MeasureService]
        self.finished = False
        self.res_outc = None

    def start(self):
        super().start()
        self._measure_service.start()

    def stop(self):
        super().stop()
        self._measure_service.stop()

    def run(self):
        signal_label_meas = ResMeasure.__name__

        req = ReqMeasure(mem_pos=0, x_rotation_angle_1=0, y_rotation_angle=np.pi / 2, x_rotation_angle_2=np.pi)
        self._measure_service.put(req)

        yield self.await_signal(sender=self._measure_service, signal_label=signal_label_meas)
        self.res_outc = self._measure_service.get_signal_result(label=signal_label_meas, receiver=self)

        self.finished = True


class MockLinkLayerSwap(Protocol):
    def __init__(self, driver):
        super().__init__(name="mock_link_layer_swap")
        self._driver = driver
        self._swap_service = self._driver[SwapService]
        self._swap_service.stop()

    def start(self):
        super().start()
        self._swap_service.start()

    def stop(self):
        super().stop()
        self._swap_service.stop()

    def run(self):
        signal_label_swap = ResSwap.__name__

        req = ReqSwap(mem_pos_1=0, mem_pos_2=1)
        self._swap_service.put(req)

        yield self.await_signal(sender=self._swap_service, signal_label=signal_label_swap)
        self.res_outc = self._swap_service.get_signal_result(label=signal_label_swap, receiver=self)

        self.finished = True


def _test_measure_service(node):
    """Test if measurement service performs as expected. We initialize in the |+> state, measure in the
    Hadamard basis and check if the obtained result is as expected."""

    ns.sim_reset()
    INSTR_INIT(node.qmemory, positions=[0])
    INSTR_H(node.qmemory, positions=[0])

    driver = node.driver

    link_layer = MockLinkLayerMeasure(driver=driver)
    link_layer.start()
    ns.sim_run()

    assert(link_layer.finished)
    assert(link_layer.res_outc.outcome[0] == 0)


def test_processing_node_measure_service():
    qprocessor = QuantumProcessor(name="test_measure_service_processing_node_processor", num_positions=1,
                                  fallback_to_nonphysical=False)
    qprocessor.add_instruction(INSTR_ROT, duration=0)
    qprocessor.add_instruction(INSTR_MEASURE, duration=0)
    # qprocessor.add_instruction(INSTR_H)
    node = Node(name="test_measure_service_processing_node", qmemory=qprocessor)
    node.driver = Driver("test_measure_service_processing_node_driver")
    node.add_subcomponent(node.driver)
    node.driver.add_service(MeasureService, ProcessingNodeMeasureService(node=node))
    _test_measure_service(node=node)


def _test_swap_service(node=None):
    """Test if a swap service performs as expected. Node is initialized in Bell state B00.
    Swap service is run and we check if the obtained Bell state is the expected."""

    # which rotation to apply from |00>+|11> to the
    # other Bell states
    rot_instr2bell_index = {INSTR_I: BellIndex.PHI_PLUS,
                            INSTR_X: BellIndex.PSI_PLUS,
                            INSTR_Z: BellIndex.PHI_MINUS,
                            INSTR_Y: BellIndex.PSI_MINUS}
    driver = node.driver
    for instr, bell_index in rot_instr2bell_index.items():
        ns.sim_reset()

        INSTR_INIT(node.qmemory, positions=[0, 1])
        INSTR_H(node.qmemory, positions=[0])
        INSTR_CNOT(node.qmemory, positions=[0, 1])
        instr(node.qmemory, positions=[0])

        link_layer = MockLinkLayerSwap(driver=driver)
        link_layer.start()
        ns.sim_run()

        assert(link_layer.res_outc.outcome) == bell_index
        assert(link_layer.finished)


def test_processing_node_swap_service():
    qprocessor = QuantumProcessor(name="test_swap_service_processing_node_processor", num_positions=2,
                                  fallback_to_nonphysical=False)
    # qprocessor.add_instruction(INSTR_CNOT, duration=0)
    # qprocessor.add_instruction(INSTR_H, duration=0)
    # qprocessor.add_instruction(INSTR_MEASURE, duration=0)
    qprocessor.add_instruction(INSTR_MEASURE_BELL, duration=0)
    node = Node(name="test_swap_service_processing_node", qmemory=qprocessor)
    node.driver = Driver("test_swap_service_processing_node_driver")
    node.add_subcomponent(node.driver)
    node.driver.add_service(SwapService, ProcessingNodeSwapService(node=node))
    _test_swap_service(node=node)


def test_discard_during_swap():
    """If a qubit is discarded during swap, the service should discard the other qubit as well."""
    qprocessor = QuantumProcessor(name="test_discard_during_swap_processor", num_positions=2)
    qprocessor.add_instruction(instruction=INSTR_MEASURE_BELL, duration=100)
    node = Node(name="test_discard_during_swap_node", qmemory=qprocessor)
    driver = Driver("test_discard_during_swap_driver")
    node.add_subcomponent(driver)
    node.driver = driver
    tracker = LocalLinkTracker(node=node)
    driver.add_service(EntanglementTrackerService, tracker)

    class SimpleSwapProgram(QuantumProgram):

        def program(self):
            q1, q2 = self.get_qubit_indices(2)
            self.apply(INSTR_MEASURE_BELL, [q1, q2])
            yield self.run()

        @property
        def get_outcome_as_bell_index(self):
            return BellIndex.B00

    swap_service = ProcessingNodeSwapService(node=node, swap_prog=SimpleSwapProgram)
    driver.add_service(SwapService, swap_service)
    qprocessor.execute_instruction(INSTR_INIT, qubit_mapping=[0, 1], physical=False)
    ns.sim_run()
    tracker.register_new_local_link(remote_node_id=100, mem_pos=0)
    tracker.register_new_local_link(remote_node_id=101, mem_pos=1)
    link_identifier_0 = tracker.get_link(0)
    assert link_identifier_0 is not None
    link_identifier_1 = tracker.get_link(1)
    assert link_identifier_1 is not None
    req = ReqSwap(mem_pos_1=0, mem_pos_2=1)
    swap_service.start()
    swap_service.put(req)
    ns.sim_run(duration=50)  # should be halfway through swap now
    tracker.register_local_discard_mem_pos(mem_pos=0)
    ns.sim_run()
    assert tracker.get_status(link_identifier_0) is LinkStatus.DISCARDED
    assert tracker.get_status(link_identifier_1) is LinkStatus.DISCARDED
    assert swap_service.get_signal_result(ResSwap.__name__).outcome is None
