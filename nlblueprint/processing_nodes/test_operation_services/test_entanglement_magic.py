from netsquid import BellIndex
from netsquid.protocols import NodeProtocol
import netsquid as ns
from netsquid_magic.magic_distributor import MagicDistributor
from netsquid_magic.state_delivery_sampler import PerfectStateSamplerFactory
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.components import Message
from netsquid.qubits.qubitapi import reduced_dm
from netsquid.qubits.ketstates import b00
import numpy as np
from netsquid.nodes.node import Node
from netsquid.nodes.connections import Connection
from netsquid_physlayer.detectors import BSMOutcome

from netsquid_driver.entanglement_agreement_service import EntanglementAgreementService, \
    ResEntanglementAgreementReached
from netsquid_driver.driver import Driver
from nlblueprint.processing_nodes.entanglement_magic import EntanglementMagic, SingleMemoryEntanglementMagic
from netsquid_driver.entanglement_service import ReqEntanglement, ResStateReady, \
    ResEntanglementSuccess, EntanglementService, MessageHandlerProtocol
import logging


class EntanglementMagicTestProtocol(NodeProtocol):

    def __init__(self, node):
        super().__init__(node=node, name="entanglement_magic_test_protocol")
        self.finished = False

    def run(self):
        req = ReqEntanglement(port_name="to_bob", mem_pos=0)
        self.node.driver[EntanglementService].put(req)
        yield self.await_signal(sender=self.node.driver[EntanglementService],
                                signal_label=ResStateReady.__name__)
        # Bob only gets request after 1000, success is within first cycle, state delay = 10
        assert np.isclose(ns.sim_time(), 1010)
        yield self.await_signal(sender=self.node.driver[EntanglementService],
                                signal_label=ResEntanglementSuccess.__name__)
        # label delay = 1 so expect success heralding one time unit later
        assert np.isclose(ns.sim_time(), 1011)
        self.finished = True


class MagicDistributorWithRightMeta(MagicDistributor):
    """Needed because most magic distributors in netsquid_magic don't use the expected format."""

    @staticmethod
    def _create_label_message(label):
        return Message(items=BSMOutcome(success=True, bell_index=BellIndex.B00), id="qdetector")


class SimpleEntanglementAgreementService(EntanglementAgreementService):
    """Always automatic agreement."""
    def try_to_reach_agreement(self, req):
        self.send_response(ResEntanglementAgreementReached(remote_node_id=req.remote_node_id,
                                                           connection_id=req.connection_id))

    def abort(self, req):
        pass


def entanglement_magic_test(entanglement_magic_class):
    """Test entanglement magic services.

    Parameters
    ----------
    entanglement_magic_class : class
        Entanglement magic class that should be tested.

    """
    ns.sim_reset()
    ns.logger.setLevel(logging.DEBUG)
    nodes = {}
    for name in ["alice", "bob"]:
        nodes[name] = Node(name, qmemory=QuantumProcessor(f"processor_{name}"))
        driver = Driver(f"driver_{name}")
        nodes[name].add_subcomponent(driver)
        nodes[name].driver = driver
        nodes[name].driver.add_service(EntanglementService,
                                       entanglement_magic_class(node=nodes[name], name=f"entanglement_magic_{name}"))
        nodes[name].driver.add_service(EntanglementAgreementService,
                                       SimpleEntanglementAgreementService(node=nodes[name]))
    connection = Connection("test_magic_entanglement_connection")
    nodes["alice"].connect_to(remote_node=nodes["bob"], connection=connection,
                              local_port_name="to_bob", remote_port_name="to_alice")
    for name in ["alice", "bob"]:
        nodes[name].driver.add_service(MessageHandlerProtocol, MessageHandlerProtocol(nodes[name]))
        nodes[name].driver[MessageHandlerProtocol].start()
    connection.magic_distributor = MagicDistributorWithRightMeta(delivery_sampler_factory=PerfectStateSamplerFactory(),
                                                                 nodes=list(nodes.values()),
                                                                 cycle_time=100,
                                                                 state_delay=10,
                                                                 label_delay=1)
    test_protocol = EntanglementMagicTestProtocol(node=nodes["alice"])

    test_protocol.start()
    ns.sim_run(duration=1000)
    nodes["bob"].driver[EntanglementService].put(ReqEntanglement(port_name="to_alice", mem_pos=0))
    ns.sim_run()
    assert test_protocol.finished
    qubits = []
    for node in nodes.values():
        qubits.append(node.qmemory.peek(positions=[i for i in range(node.qmemory.num_positions)])[0])
    assert np.isclose(reduced_dm(qubits), np.outer(b00, b00)).all()


def test_entanglement_magic():

    entanglement_magic_test(EntanglementMagic)


def test_single_memory_entanglement_magic():

    entanglement_magic_test(SingleMemoryEntanglementMagic)

    # additionally, test if in_use system was inherited appropriately
    node = Node("test_node", qmemory=QuantumProcessor("test_processor"))
    single_memory_entanglement_magic = SingleMemoryEntanglementMagic(node)
    assert single_memory_entanglement_magic.get_in_use(0) is False
    single_memory_entanglement_magic.set_in_use(0, True)
    assert node.qmemory.mem_positions[0].in_use
    assert single_memory_entanglement_magic.get_in_use(0)
