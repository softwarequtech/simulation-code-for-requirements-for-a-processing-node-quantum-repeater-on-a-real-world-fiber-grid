import unittest
from netsquid.nodes.node import Node
from netsquid.components.qprocessor import QuantumProcessor
from netsquid_driver.driver import Driver
from netsquid_driver.memory_manager_service import ReqMove
from nlblueprint.processing_nodes.memory_manager_implementations import CommunicationMemoryManager
from nlblueprint.processing_nodes.test_memory_manager.test_memory_manager_with_move import AbstractMoveProgram


class TestCommunicationMemoryManager(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        qprocessor = QuantumProcessor(name="test_processor",
                                      num_positions=3,
                                      fallback_to_nonphysical=True)
        mock_driver = Driver(name="mock_driver")
        cls.node = Node(name="test_node",
                        qmemory=qprocessor)
        cls.node.add_subcomponent(mock_driver)
        cls.node.driver = list(cls.node.subcomponents.filter_by_type(Driver).values())[0]

    def setUp(self):
        self.size = 3
        self.memory_manager = CommunicationMemoryManager(node=self.node,
                                                         move_program=AbstractMoveProgram(),
                                                         communication_qubits=[0, 1],
                                                         size=self.size)

    def test_communication_and_memory_qubits(self):
        assert self.memory_manager.communication_qubits == [0, 1]
        assert self.memory_manager.memory_qubits == [2]

    def test_move_done_to_memory_position(self):
        # Check that move is done to memory position and not to communication position if no destination is specified
        req = ReqMove(from_memory_position_id=0)
        self.memory_manager.put(req)
        assert not self.node.qmemory.mem_positions[0].in_use
        assert not self.node.qmemory.mem_positions[1].in_use
        assert self.node.qmemory.mem_positions[2].in_use
