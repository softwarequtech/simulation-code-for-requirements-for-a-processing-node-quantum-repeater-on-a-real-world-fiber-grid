import numpy as np
import netsquid as ns
import unittest
from netsquid.nodes.node import Node
from netsquid.components import QuantumProgram, INSTR_SWAP, INSTR_INIT, INSTR_H
from netsquid.components.qprocessor import QuantumProcessor
from netsquid_driver.memory_manager_service import ReqMove, ReqFreeMemory
from nlblueprint.processing_nodes.memory_manager_implementations import MemoryManagerWithMoveProgram
from netsquid_driver.driver import Driver


class AbstractMoveProgram(QuantumProgram):

    def program(self):
        move_from, move_to = self.get_qubit_indices(2)
        self.apply(INSTR_SWAP, [move_from, move_to])

        yield self.run()


class TestMemoryManagerWithMoveProgram(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        qprocessor = QuantumProcessor(name="test_processor",
                                      num_positions=3,
                                      fallback_to_nonphysical=True)
        mock_driver = Driver(name="mock_driver")
        cls.node = Node(name="test_node",
                        qmemory=qprocessor)
        cls.node.add_subcomponent(mock_driver)
        cls.node.driver = list(cls.node.subcomponents.filter_by_type(Driver).values())[0]

    def setUp(self):
        self.size = 3
        self.memory_manager = MemoryManagerWithMoveProgram(node=self.node,
                                                           move_program=AbstractMoveProgram(),
                                                           size=self.size)

    def test_move(self):
        req = ReqMove(from_memory_position_id=0,
                      to_memory_position_id=2)
        self.memory_manager.put(req)
        assert not self.node.qmemory.mem_positions[0].in_use
        assert self.node.qmemory.mem_positions[2].in_use

    def test_move_to_position_out_of_bounds(self):
        req = ReqMove(from_memory_position_id=0,
                      to_memory_position_id=47)
        with self.assertRaises(IndexError):
            self.memory_manager.put(req)

    def test_move_from_position_out_of_bounds(self):
        req = ReqMove(from_memory_position_id=92,
                      to_memory_position_id=2)
        with self.assertRaises(IndexError):
            self.memory_manager.put(req)

    def test_move_and_check_states(self):
        """Initialize position 0 to |+>, position 1 to |0>, swap them and check that position 0 now holds state |0> and
        position 1 holds state |+>.
        """
        req = ReqFreeMemory()
        self.memory_manager.put(req)
        ns.sim_run()
        INSTR_INIT(self.node.qmemory, positions=[0])
        INSTR_INIT(self.node.qmemory, positions=[1])
        INSTR_H(self.node.qmemory, positions=[0])

        req = ReqMove(from_memory_position_id=0,
                      to_memory_position_id=1)
        self.memory_manager.put(req)
        ns.sim_run()

        plus_dm = np.array([[0.5, 0.5], [0.5, 0.5]])
        zero_dm = np.array([[1., 0.], [0., 0.]])
        qubits_post_move = self.node.qmemory.peek(positions=[0, 1])

        assert (np.isclose(qubits_post_move[0].qstate.qrepr.reduced_dm(), zero_dm)).all()
        assert (np.isclose(qubits_post_move[1].qstate.qrepr.reduced_dm(), plus_dm)).all()

    def test_free_specific_position(self):
        req = ReqFreeMemory(memory_position_id=1)
        self.memory_manager.put(req)
        assert not self.node.qmemory.mem_positions[1].in_use

    def test_free_all_positions(self):
        req = ReqFreeMemory()
        self.memory_manager.put(req)
        for pos in self.node.qmemory.mem_positions:
            assert not pos.in_use

    def test_free_out_of_bounds_position(self):
        req = ReqFreeMemory(memory_position_id=12)
        with self.assertRaises(IndexError):
            self.memory_manager.put(req)
