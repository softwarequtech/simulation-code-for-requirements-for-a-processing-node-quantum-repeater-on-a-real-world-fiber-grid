"Contains DataCollector classes to be used for gathering data from EGP as a service simulations."

import netsquid as ns
from netsquid.qubits.qubitapi import reduced_dm, operate
from qlink_interface import MeasurementBasis, ResMeasureDirectly, ResCreateAndKeep
from netsquid.qubits.operators import H
from netsquid_driver.memory_manager_service import QuantumMemoryManager


def measurement_basis_qlink_enum_to_str():
    # we should maybe refactor sim_tool to make this obsolete
    return {MeasurementBasis.X: "X",
            MeasurementBasis.Y: "Y",
            MeasurementBasis.Z: "Z"}


class EGPDataCollector:
    """DataCollector to pick up (QKD) data from EGP responses.

    Parameters
    ----------
    start_time: float (optional)
        Simulation time when the request was sent to the EGP service.
        This is then used to calculate the generation time as (time_response_arrived - start_time).

    """
    def __init__(self, start_time=ns.sim_time()):
        # probably this in not necessary
        self.sim_time = start_time

    def __call__(self, evexpr):
        """Picks up responses from the EGP when they are signalled by both Protocols.

        Note: This gets the EventExpression that triggers the DataCollection which as first term has Alice's event and
        as a second term Bob's.
        This is then used to get the right responses from the EGP Services.
        """
        generation_time = ns.sim_time() - self.sim_time
        self.sim_time = ns.sim_time()
        mb_to_str = measurement_basis_qlink_enum_to_str()

        response_alice = evexpr.first_term.triggered_events[0]. \
            source.get_signal_by_event(evexpr.first_term.triggered_events[0])
        response_bob = evexpr.second_term.triggered_events[0]. \
            source.get_signal_by_event(evexpr.second_term.triggered_events[0])

        assert evexpr.first_term.triggered_events[0].source != evexpr.second_term.triggered_events[0].source

        if isinstance(response_alice.result, ResMeasureDirectly) and \
                isinstance(response_bob.result, ResMeasureDirectly):
            data = {"basis_A": mb_to_str[response_alice.result.measurement_basis],
                    "basis_B": mb_to_str[response_bob.result.measurement_basis],
                    "outcome_A": response_alice.result.measurement_outcome,
                    "outcome_B": response_bob.result.measurement_outcome,
                    "generation_duration": generation_time * 1E-9,
                    "midpoint_outcome_0": response_alice.result.bell_state,
                    }
        else:
            raise NotImplementedError(f"Data collection not implemented for requests of type {type(response_alice)}")
        return data


class EGPDataCollectorState:
    """DataCollector to pick up state data from EGP responses.

    Parameters
    ----------
    start_time: float (optional)
        Simulation time when the request was sent to the EGP service.
        This is then used to calculate the generation time as (time_response_arrived - start_time).

    """
    def __init__(self, start_time=ns.sim_time()):
        # probably this in not necessary
        self.sim_time = start_time

    def __call__(self, evexpr):
        """Picks up responses from the EGP when they are signalled by both Protocols.

        Note: This gets the EventExpression that triggers the DataCollection which as first term has Alice's event and
        as a second term Bob's.
        This is then used to get the right responses from the EGP Services.
        """
        generation_time = ns.sim_time() - self.sim_time
        self.sim_time = ns.sim_time()

        assert evexpr.first_term.triggered_events[0].source != evexpr.second_term.triggered_events[0].source

        response_alice = evexpr.first_term.triggered_events[0]. \
            source.get_signal_by_event(evexpr.first_term.triggered_events[0])

        response_bob = evexpr.second_term.triggered_events[0]. \
            source.get_signal_by_event(evexpr.second_term.triggered_events[0])

        if isinstance(response_alice.result, ResCreateAndKeep) and \
                isinstance(response_bob.result, ResCreateAndKeep):
            alice_node = evexpr.first_term.triggered_events[0].source.node
            bob_node = evexpr.second_term.triggered_events[0].source.node
            [alice_qubit] = alice_node.qmemory.peek(positions=response_alice.result.logical_qubit_id, skip_noise=False)
            [bob_qubit] = bob_node.qmemory.peek(positions=response_bob.result.logical_qubit_id, skip_noise=False)

            # Required because move circuit for NV centers leaves carbon state in rotated basis
            # This is not a pretty solution, see https://ci.tno.nl/gitlab/QIA/nlblueprint/-/issues/610 for discussion
            for node, qubit in zip([alice_node, bob_node], [alice_qubit, bob_qubit]):
                try:
                    if node.driver[QuantumMemoryManager].num_communication_qubits == 1:
                        if node.qmemory.num_positions > 2:
                            operate(qubit, H)
                except AttributeError:
                    pass
            state = reduced_dm([alice_qubit, bob_qubit])

            data = {"state": state,
                    "generation_duration": generation_time * 1E-9,
                    "midpoint_outcome_0": response_alice.result.bell_state
                    }
        else:
            raise NotImplementedError(f"Data collection not implemented for requests of type {type(response_alice)}")
        return data
