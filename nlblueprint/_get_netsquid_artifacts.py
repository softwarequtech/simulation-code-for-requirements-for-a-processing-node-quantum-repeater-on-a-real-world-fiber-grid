########################################
# This file used used by the CI to extract the latest artifacts from the netsquid pipeline
#
# Author: Axel Dahlberg
########################################

import os
import json
from subprocess import Popen, PIPE, check_output


def get_python_version():
    output = check_output(["python3", "--version"])
    output = output.decode("utf-8")

    versions = ["3.{}.".format(i) for i in range(5, 9)]
    for version in versions:
        if version in output:
            return version.replace(".", "")
    raise RuntimeError("Could not find a known version in the output {}".format(output))


NETSQUID_PROJECT_ID = 890
_python_version = get_python_version()
JOB_NAME = "bdist:18.04-cp{}".format(_python_version)


def get_highest_job_id():
    secret_key = os.environ['SECRET_KEY']
    header = "PRIVATE-TOKEN:{}".format(secret_key)
    url = "https://ci.tno.nl/gitlab/api/v4/projects/{}/jobs/".format(NETSQUID_PROJECT_ID)
    p = Popen(["curl", "--header", header, url], stdout=PIPE)
    output = p.communicate()

    jobs = json.loads(output[0])

    highest_job_id = -1
    for job in jobs:
        job_id = job['id']
        if job_id > highest_job_id:
            name = job['name']
            if name == JOB_NAME:
                highest_job_id = job_id

    if highest_job_id == -1:
        raise RuntimeError("Could not find a job with name {}".format(JOB_NAME))

    return highest_job_id


def get_artifacts(job_id):
    secret_key = os.environ['SECRET_KEY']
    header = "PRIVATE-TOKEN:{}".format(secret_key)
    url = "https://ci.tno.nl/gitlab/api/v4/projects/{}/jobs/{}/artifacts".format(NETSQUID_PROJECT_ID, job_id)
    p = Popen(["curl", "--header", header, url, "-o", "netsquid_artifacts.zip"],
              stdout=PIPE)
    p.wait()


def main():
    job_id = get_highest_job_id()
    get_artifacts(job_id)


if __name__ == '__main__':
    main()
