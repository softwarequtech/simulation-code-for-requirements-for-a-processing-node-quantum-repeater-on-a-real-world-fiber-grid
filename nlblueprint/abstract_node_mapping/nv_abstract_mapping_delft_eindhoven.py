import yaml
from netsquid_netconf.netconf import Loader
from nlblueprint.abstract_node_mapping.abstract_model_delft_eindhoven import AbstractParameters


class NVParameters(AbstractParameters):
    """Performs mapping from NV center physical parameters to abstract parameters for abstract model.

    The abstract hardware parameters (which can be obtained as properties from the object) are:
        - emission fidelity
        - visibility
        - dark count probability
        - detector efficiency
        - swap quality
        - T1
        - T2

    This ParameterSet class further includes operation durations, which can be obtained as properties from the object as
    well:
        - measurement duration
        - initialization duration
        - single qubit gate duration
        - two qubit gate duration
        - emission duration

    Furthermore, we include also topology parameters:
        - num parallel links
        - num communication qubits

    And finally parameters that are not strictly required to define the abstract model, but that should nevertheless be
    present in the parameter file in order to run blueprint simulations:
        - speed of light
        - p loss length
        - num resolving
        - p loss init

    Parameters
    ----------
    param_file_name : str
        Name of yaml parameter file holding NV center physical parameters.

    The following parameters should be specified in the yaml file:

    prob_dark_count : float
        Probability of detector dark count.
    prob_detect_excl_transmission_no_conversion_no_cavities : float
        Total detection efficiency, i.e. the probability that an excitation in the NV results in a detector click,
        ignoring fiber loss.
    visibility : float
        Photon distinguishability.
    ec_gate_depolar_prob : float
        Depolarizing probability for noisy controlled electron-carbon gate.
    carbon_z_rot_depolar_prob: float
        Depolarizing probability for noisy carbon single-qubit gate.
    electron_single_qubit_depolar_prob : float
        Depolarizing probability for noisy electron single-qubit gate.
    electron_init_depolar_prob : float
        Depolarizing probability for noisy electron initialization.
    electron_measure_error_prob : float
        Bit-flip probability for noisy electron measurement.
    carbon_T1 : float
        T1 coherence time of carbon atom.
    carbon_T2 : float
        T2 coherence time of carbon atom.
    electron_measurement_duration : float
        Duration of electron measurement, in ns.
    carbon_init_duration : float
        Duration of carbon initialization, in ns.
    ec_gate_duration : float
        Duration of two-qubit electron-carbon gate, in ns.
    carbon_z_rot_duration : float
        Duration of Z rotation on carbon, in ns.
    photon_emission_delay : float
        Duration of emission operation, in ns.
    num_communication_qubits : int or None
        Number of qubits that can be used for outward communication.
    num_parallel_links : int or None
        Number of links that can be generated in parallel.
    cutoff_time : float or None
        Time after which qubits are discarded.
    speed_of_light : float
        Speed of light in fiber. Not part of model, required to run simulation.
    p_loss_length : float
        Attenuation in fiber. Not part of model, required to run simulation.
    p_loss_init : float
        Probability of losing photon on coupling to fiber. Not part of model, required to run simulation.
    num_resolving : bool
        Whether or not detector is num_resolving. Not part of model, required to run simulation.

    Notes
    -----

    An entanglement swap in an NV platform consists of one-qubit gates on both carbon and electron, two-qubit
    gates and measurement and initialization of the electron. The gates and initialization noises are modelled by
    depolarizing channels in the NV model, while the measurement error is modelled by a bit flip. We
    approximate it by a depolarizing channel and compute the swap quality as the parameter of the overall
    depolarizing channel.

    An NV center has different memory lifetimes for the carbon and the electron. This subtlety is lost in the
    abstract model. Assuming low success probabilities, the relevant decoherence processess should be the ones
    related to the carbon atom, not the electron, so we take T1 and T2 to be given by the carbon's T1 and T2.
    Other dephasing processes such as induced dephasing (see e.g. https://arxiv.org/abs/1802.05996) are likewise
    ignored.

    These parameters are input into the class using a yaml parameter file, `nv_center_param_file`. The class'
    `dump_to_yaml` method produces a yaml parameter file with the converted abstract parameters.

    This mapping is meant for usage with a single-click entanglement generation protocol. It does not take emission
    fidelity into account as that parameter does not play a role in the blueprint's single-click model.

    """

    def __init__(self, nv_center_param_file):
        self._param_file = nv_center_param_file
        with open(self._param_file, "r") as stream:
            sim_params = yaml.load(stream, Loader=Loader)

        self._visibility = sim_params["visibility"]
        self._dark_count_probability = sim_params["dark_count_probability"]
        try:
            self._detector_efficiency = \
                sim_params["prob_detect_excl_transmission_no_conversion_no_cavities"]
        except KeyError:
            self._detector_efficiency = sim_params["detector_efficiency"]
            if "collection_efficiency" in sim_params:
                self._detector_efficiency *= sim_params["collection_efficiency"]
            if "p_loss_init" in sim_params:
                self._detector_efficiency *= (1 - sim_params["p_loss_init"])
        self._T2 = sim_params["carbon_T2"]
        self._T1 = sim_params["carbon_T1"]
        electron_measure_error_prob = max(sim_params["prob_error_0"], sim_params["prob_error_1"])
        self._swap_quality = \
            self._compute_swap_quality(
                ec_gate_depolar_prob=sim_params["ec_gate_depolar_prob"],
                carbon_z_rot_depolar_prob=sim_params["carbon_z_rot_depolar_prob"],
                electron_single_qubit_depolar_prob=sim_params["electron_single_qubit_depolar_prob"],
                electron_init_depolar_prob=sim_params["electron_init_depolar_prob"],
                electron_measure_error_prob=electron_measure_error_prob)
        self._measurement_duration = sim_params["measure_duration"]
        self._initialization_duration = sim_params["electron_init_duration"]
        self._single_qubit_gate_duration = sim_params["carbon_z_rot_duration"]
        self._two_qubit_gate_duration = sim_params["ec_two_qubit_gate_duration"]
        self._emission_duration = sim_params["photon_emission_delay"]
        self._num_communication_qubits = None
        if sim_params["num_parallel_links"] is None:
            self._num_parallel_links = 1
        else:
            self._num_parallel_links = sim_params["num_parallel_links"]
        self._cutoff_time = sim_params["cutoff_time"]
        self._param_file_name = None
        self._speed_of_light = sim_params["speed_of_light"]
        self._p_loss_length = sim_params["p_loss_length"]
        self._num_resolving = sim_params["num_resolving"]

        super().__init__()

    @staticmethod
    def _compute_swap_quality(ec_gate_depolar_prob, carbon_z_rot_depolar_prob, electron_single_qubit_depolar_prob,
                              electron_init_depolar_prob, electron_measure_error_prob):
        magical_swap_gate_depol_prob = 1 - (1 - ec_gate_depolar_prob) ** 2
        swap_quality = (1 - carbon_z_rot_depolar_prob) * (1 - ec_gate_depolar_prob) * \
                       (1 - carbon_z_rot_depolar_prob) * (1 - electron_single_qubit_depolar_prob) * \
                       (1 - electron_measure_error_prob) * (1 - electron_init_depolar_prob) * \
                       (1 - magical_swap_gate_depol_prob) * (1 - electron_single_qubit_depolar_prob) * \
                       (1 - electron_measure_error_prob)

        return swap_quality

    @property
    def visibility(self):
        return float(self._visibility)

    @property
    def dark_count_probability(self):
        return float(self._dark_count_probability)

    @property
    def detector_efficiency(self):
        return float(self._detector_efficiency)

    @property
    def swap_quality(self):
        return float(self._swap_quality)

    @property
    def T1(self):
        return float(self._T1)

    @property
    def T2(self):
        return float(self._T2)

    @property
    def measurement_duration(self):
        return float(self._measurement_duration)

    @property
    def initialization_duration(self):
        return float(self._initialization_duration)

    @property
    def single_qubit_gate_duration(self):
        return float(self._single_qubit_gate_duration)

    @property
    def two_qubit_gate_duration(self):
        return float(self._two_qubit_gate_duration)

    @property
    def emission_duration(self):
        return float(self._emission_duration)

    @property
    def num_communication_qubits(self):
        return self._num_communication_qubits

    @property
    def num_parallel_links(self):
        return self._num_parallel_links

    @property
    def cutoff_time(self):
        return self._cutoff_time

    @property
    def param_file_name(self):
        return self._param_file_name

    @property
    def speed_of_light(self):
        return self._speed_of_light

    @property
    def p_loss_length(self):
        return self._p_loss_length

    @property
    def num_resolving(self):
        return self._num_resolving

    @property
    def param_file(self):
        return self._param_file


class NVParametersDoubleClick(NVParameters):
    """Extends parent class by including emission fidelity as a parameter. Meant for usage with double-click
    entanglement generation.
    """
    def __init__(self, nv_center_param_file):
        self._param_file = nv_center_param_file
        with open(self._param_file, "r") as stream:
            sim_params = yaml.load(stream, Loader=Loader)
        self._emission_fidelity = 1 - sim_params["p_fail_class_corr"]
        super().__init__(nv_center_param_file=nv_center_param_file)

    @property
    def emission_fidelity(self):
        return float(self._emission_fidelity)
