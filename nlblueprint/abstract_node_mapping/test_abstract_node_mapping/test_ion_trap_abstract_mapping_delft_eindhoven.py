from nlblueprint.abstract_node_mapping.ion_trap_abstract_mapping_delft_eindhoven import IonTrapParametersSingleClick


def test_perfect_hardware():
    ion_trap_perfect_param_file = \
        "nlblueprint/abstract_node_mapping/test_abstract_node_mapping/ti_perfect_parameters_for_conversion.yaml"

    ti_perfect_parameters = IonTrapParametersSingleClick(ion_trap_param_file=ion_trap_perfect_param_file)

    assert ti_perfect_parameters.emission_fidelity == 1.
    assert ti_perfect_parameters.visibility == 1.
    assert ti_perfect_parameters.dark_count_probability == 0.
    assert ti_perfect_parameters.detector_efficiency == 1.
    assert ti_perfect_parameters.swap_quality == 1.
    assert ti_perfect_parameters.T1 == 0.
    assert ti_perfect_parameters.T2 == 0.
    assert ti_perfect_parameters.measurement_duration == 3.
    assert ti_perfect_parameters.initialization_duration == 12.
    assert ti_perfect_parameters.single_qubit_gate_duration == 4.
    assert ti_perfect_parameters.two_qubit_gate_duration == 500.
    assert ti_perfect_parameters.emission_duration == 5.
    assert ti_perfect_parameters.num_communication_qubits == 1
    assert ti_perfect_parameters.num_parallel_links == 1
    assert ti_perfect_parameters.cutoff_time == 0.
    assert ti_perfect_parameters.speed_of_light == 20000000
    assert ti_perfect_parameters.p_loss_length == 0.
    assert ti_perfect_parameters.num_resolving is False
    assert ti_perfect_parameters.p_loss_init == 0.


def test_noisy_swap():
    ion_trap_noisy_swap_param_file = \
        "nlblueprint/abstract_node_mapping/test_abstract_node_mapping/ti_noisy_swap_parameters_for_conversion.yaml"

    expected_swap_quality = (1 - 0.05) * (1 - 0.1) * (1 - 0.1)
    ti_noisy_swap_parameters = IonTrapParametersSingleClick(ion_trap_param_file=ion_trap_noisy_swap_param_file)
    assert ti_noisy_swap_parameters.emission_fidelity == 1.
    assert ti_noisy_swap_parameters.visibility == 1.
    assert ti_noisy_swap_parameters.dark_count_probability == 0.
    assert ti_noisy_swap_parameters.detector_efficiency == 1.
    assert ti_noisy_swap_parameters.swap_quality == expected_swap_quality
    assert ti_noisy_swap_parameters.T1 == 0.
    assert ti_noisy_swap_parameters.T2 == 0.
    assert ti_noisy_swap_parameters.measurement_duration == 0.
    assert ti_noisy_swap_parameters.initialization_duration == 0.
    assert ti_noisy_swap_parameters.single_qubit_gate_duration == 0.
    assert ti_noisy_swap_parameters.two_qubit_gate_duration == 27.
    assert ti_noisy_swap_parameters.num_communication_qubits == 1
    assert ti_noisy_swap_parameters.num_parallel_links == 1
    assert ti_noisy_swap_parameters.cutoff_time == 0.
    assert ti_noisy_swap_parameters.speed_of_light == 20000000
    assert ti_noisy_swap_parameters.p_loss_length == 0.
    assert ti_noisy_swap_parameters.num_resolving is False
    assert ti_noisy_swap_parameters.p_loss_init == 0.
