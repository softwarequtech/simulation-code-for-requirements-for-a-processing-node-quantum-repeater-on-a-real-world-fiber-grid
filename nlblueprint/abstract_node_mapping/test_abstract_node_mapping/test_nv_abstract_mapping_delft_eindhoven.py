from nlblueprint.abstract_node_mapping.nv_abstract_mapping_delft_eindhoven import NVParameters


def test_perfect_hardware():
    nv_perfect_param_file = \
        "nlblueprint/abstract_node_mapping/test_abstract_node_mapping/nv_perfect_parameters_for_conversion.yaml"

    nv_perfect_parameters = NVParameters(nv_center_param_file=nv_perfect_param_file)

    assert nv_perfect_parameters.emission_fidelity == 1.
    assert nv_perfect_parameters.visibility == 1.
    assert nv_perfect_parameters.dark_count_probability == 0.
    assert nv_perfect_parameters.detector_efficiency == 1.
    assert nv_perfect_parameters.swap_quality == 1.
    assert nv_perfect_parameters.T1 == 0.
    assert nv_perfect_parameters.T2 == 0.
    assert nv_perfect_parameters.measurement_duration == 0.
    assert nv_perfect_parameters.initialization_duration == 0.
    assert nv_perfect_parameters.single_qubit_gate_duration == 0.
    assert nv_perfect_parameters.two_qubit_gate_duration == 0.
    assert nv_perfect_parameters.num_communication_qubits is None
    assert nv_perfect_parameters.num_parallel_links == 1
    assert nv_perfect_parameters.cutoff_time == 0.
    assert nv_perfect_parameters.speed_of_light == 20000000
    assert nv_perfect_parameters.p_loss_length == 0.
    assert nv_perfect_parameters.num_resolving is False
    assert nv_perfect_parameters.p_loss_init == 0.


def test_noisy_swap():
    nv_noisy_swap_param_file = \
        "nlblueprint/abstract_node_mapping/test_abstract_node_mapping/nv_noisy_swap_parameters_for_conversion.yaml"

    magical_swap_gate_depolar_prob = 1 - (1 - 0.01) ** 2
    expected_swap_quality = (1 - 0.02) * (1 - 0.01) * (1 - 0.02) * (1 - 0.03) * \
        (1 - magical_swap_gate_depolar_prob) * (1 - 0.03)
    nv_noisy_swap_parameters = NVParameters(nv_center_param_file=nv_noisy_swap_param_file)

    assert nv_noisy_swap_parameters.emission_fidelity == 1.
    assert nv_noisy_swap_parameters.visibility == 1.
    assert nv_noisy_swap_parameters.dark_count_probability == 0.
    assert nv_noisy_swap_parameters.detector_efficiency == 1.
    assert nv_noisy_swap_parameters.swap_quality == expected_swap_quality
    assert nv_noisy_swap_parameters.T1 == 0.
    assert nv_noisy_swap_parameters.T2 == 0.
    assert nv_noisy_swap_parameters.measurement_duration == 0.
    assert nv_noisy_swap_parameters.initialization_duration == 0.
    assert nv_noisy_swap_parameters.single_qubit_gate_duration == 0.
    assert nv_noisy_swap_parameters.two_qubit_gate_duration == 0.
    assert nv_noisy_swap_parameters.num_communication_qubits is None
    assert nv_noisy_swap_parameters.num_parallel_links == 1
    assert nv_noisy_swap_parameters.cutoff_time == 0.
    assert nv_noisy_swap_parameters.speed_of_light == 20000000
    assert nv_noisy_swap_parameters.p_loss_length == 0.
    assert nv_noisy_swap_parameters.num_resolving is False
    assert nv_noisy_swap_parameters.p_loss_init == 0.
