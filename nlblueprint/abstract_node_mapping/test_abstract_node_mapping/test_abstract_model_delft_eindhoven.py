from nlblueprint.abstract_node_mapping.abstract_model_delft_eindhoven import AbstractParameters


class TestAbstractParameters(AbstractParameters):
    """A simple parameter class for testing purposes."""
    def __init__(self, param_dic):

        self._emission_fidelity = param_dic["emission_fidelity"]
        self._visibility = param_dic["visibility"]
        self._dark_count_probability = param_dic["dark_count_probability"]
        self._detector_efficiency = (1 - param_dic["p_coupling_loss"]) * param_dic["p_good_emission"] * \
            param_dic["p_detection"]
        self._swap_quality = param_dic["swap_quality"]
        self._T1 = param_dic["T1"]
        self._T2 = param_dic["T2"]
        self._measurement_duration = param_dic["measurement_duration"]
        self._initialization_duration = param_dic["initialization_duration"]
        self._single_qubit_gate_duration = param_dic["single_qubit_gate_duration"]
        self._two_qubit_gate_duration = param_dic["two_qubit_gate_duration"]
        self._emission_duration = param_dic["emission_duration"]
        self._num_communication_qubits = param_dic["num_communication_qubits"]
        self._num_parallel_links = param_dic["num_parallel_links"]
        self._cutoff_time = param_dic["cutoff_time"]
        self._speed_of_light = param_dic["speed_of_light"]
        self._p_loss_length = param_dic["p_loss_length"]
        self._num_resolving = param_dic["num_resolving"]
        self._p_loss_init = param_dic["p_loss_init"]

        super().__init__()

    @property
    def emission_fidelity(self):
        return float(self._emission_fidelity)

    @property
    def p_loss_init(self):
        return float(self._p_loss_init)

    @property
    def visibility(self):
        return float(self._visibility)

    @property
    def dark_count_probability(self):
        return float(self._dark_count_probability)

    @property
    def detector_efficiency(self):
        return float(self._detector_efficiency)

    @property
    def swap_quality(self):
        return float(self._swap_quality)

    @property
    def T1(self):
        return float(self._T1)

    @property
    def T2(self):
        return float(self._T2)

    @property
    def measurement_duration(self):
        return float(self._measurement_duration)

    @property
    def initialization_duration(self):
        return float(self._initialization_duration)

    @property
    def single_qubit_gate_duration(self):
        return float(self._single_qubit_gate_duration)

    @property
    def two_qubit_gate_duration(self):
        return float(self._two_qubit_gate_duration)

    @property
    def emission_duration(self):
        return float(self._emission_duration)

    @property
    def num_communication_qubits(self):
        return self._num_communication_qubits

    @property
    def num_parallel_links(self):
        return self._num_parallel_links

    @property
    def cutoff_time(self):
        return self._cutoff_time

    @property
    def speed_of_light(self):
        return self._speed_of_light

    @property
    def p_loss_length(self):
        return self._p_loss_length

    @property
    def num_resolving(self):
        return self._num_resolving

    @property
    def param_file(self):
        return self._param_file


def test_simple_abstract_parameters():
    parameter_input_dic = {"emission_fidelity": 0.9,
                           "visibility": 0.92,
                           "dark_count_probability": 0.1,
                           "p_coupling_loss": 0.2,
                           "p_good_emission": 0.3,
                           "p_detection": 0.99,
                           "swap_quality": 0.87,
                           "T1": 100,
                           "T2": 10,
                           "measurement_duration": 10,
                           "initialization_duration": 5,
                           "single_qubit_gate_duration": 2,
                           "two_qubit_gate_duration": 20,
                           "emission_duration": 1,
                           "num_communication_qubits": 1,
                           "num_parallel_links": 1,
                           "cutoff_time": 15.,
                           "speed_of_light": 100000.,
                           "p_loss_length": 0.2,
                           "num_resolving": False,
                           "p_loss_init": 0.1
                           }

    parameter_set = TestAbstractParameters(parameter_input_dic)
    for key, val in parameter_input_dic.items():
        if key not in ["p_coupling_loss", "p_good_emission", "p_detection"]:
            assert getattr(parameter_set, key) == val
    assert parameter_set.detector_efficiency == (1 - parameter_input_dic["p_coupling_loss"]) * \
        parameter_input_dic["p_good_emission"] * parameter_input_dic["p_detection"]
