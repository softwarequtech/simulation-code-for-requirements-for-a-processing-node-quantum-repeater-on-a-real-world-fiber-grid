import abc
import yaml
import numpy as np
from netsquid_simulationtools.parameter_set import Parameter, ParameterSet


def _coherence_time_prob_fn(x):
    if x == np.inf:
        return 1.
    elif x == 0:
        return 0.
    else:
        return np.exp(-1. / x)


def _inverse_coherence_time_prob_fn(x):
    if x == 1.:
        return np.inf
    elif x == 0.:
        return 0
    else:
        return - 1. / np.log(x)


class AbstractParameters(abc.ABC, ParameterSet):
    """Abstract class for storing parameters for abstract model.

    Should be subclassed for different hardwares to define mapping from
    physical parameters to abstract parameters.

    Usage: subclass and overwrite the properties. `emission_fidelity` is set to 1 as it is expected that
    single-click implementions subclassing from this will not implement it. `p_loss_init` is a required parameter
    because it is required by `netsquid-physlayer`'s `MiddleHeraldedConnection`. It is recommended that its value of 0
    is not overwritten as it should not be part of the model.

    Note that all detection losses with the exception of fiber losses should be captured in `detector_efficiency`. This
    might be a misleading name, as it is really more of `photon_detection_probability_at_zero_distance`, but we use the
    name `detector_efficiency` as this allows us to straightforwardly make use of the connections in
    `netsquid-physlayer` without having to add an extra layer of translation in between.

    IMPORTANT NOTES:

    - the parent class `ParameterSet` checks if all parameters
      below have been set upon initialization. This implies that
      `super().__init__()` should only be called *at the very end* of the
      `__init__` method of any child class.

    """
    _REQUIRED_PARAMETERS = [

        Parameter(name='emission_fidelity',
                  units=None,
                  type=float,
                  perfect_value=1.,
                  convert_to_prob_fn=lambda x: x,
                  convert_from_prob_fn=lambda x: x),

        Parameter(name='visibility',
                  units=None,
                  type=float,
                  perfect_value=1.,
                  convert_to_prob_fn=lambda x: x,
                  convert_from_prob_fn=lambda x: x),

        Parameter(name='dark_count_probability',
                  units=None,
                  type=float,
                  perfect_value=0.,
                  convert_to_prob_fn=lambda x: 1 - x,
                  convert_from_prob_fn=lambda x: 1 - x),

        Parameter(name='detector_efficiency',
                  units=None,
                  type=float,
                  perfect_value=1.,
                  convert_to_prob_fn=lambda x: x,
                  convert_from_prob_fn=lambda x: x),

        Parameter(name='swap_quality',
                  units=None,
                  type=float,
                  perfect_value=1.,
                  convert_to_prob_fn=lambda x: x,
                  convert_from_prob_fn=lambda x: x),

        Parameter(name='T1',
                  units='ns',
                  type=float,
                  perfect_value=np.inf,
                  convert_to_prob_fn=_coherence_time_prob_fn,
                  convert_from_prob_fn=_inverse_coherence_time_prob_fn),

        Parameter(name='T2',
                  units='ns',
                  type=float,
                  perfect_value=np.inf,
                  convert_to_prob_fn=_coherence_time_prob_fn,
                  convert_from_prob_fn=_inverse_coherence_time_prob_fn),

        Parameter(name='measurement_duration',
                  units='ns',
                  type=float,
                  perfect_value=None),

        Parameter(name='initialization_duration',
                  units='ns',
                  type=float,
                  perfect_value=None),

        Parameter(name='single_qubit_gate_duration',
                  units='ns',
                  type=float,
                  perfect_value=None),

        Parameter(name='two_qubit_gate_duration',
                  units='ns',
                  type=float,
                  perfect_value=None),

        Parameter(name='emission_duration',
                  units='ns',
                  type=float,
                  perfect_value=None),

        Parameter(name='num_communication_qubits',
                  units=None,
                  type=(int, type(None)),
                  perfect_value=None),

        Parameter(name='num_parallel_links',
                  units=None,
                  type=(int, type(None)),
                  perfect_value=None),

        Parameter(name='cutoff_time',
                  units='ns',
                  type=(float, type(None)),
                  perfect_value=None),

        Parameter(name='speed_of_light',
                  units='m/s',
                  type=float,
                  perfect_value=None),

        Parameter(name='p_loss_length',
                  units=None,
                  type=float,
                  perfect_value=0.),

        Parameter(name='num_resolving',
                  units=None,
                  type=bool,
                  perfect_value=None),

        Parameter(name='p_loss_init',
                  units=None,
                  type=float,
                  perfect_value=0.)
    ]

    @property
    def emission_fidelity(self):
        return 1.

    @property
    def p_loss_init(self):
        return 0.

    @property
    @abc.abstractmethod
    def param_file(self):
        pass

    @property
    @abc.abstractmethod
    def visibility(self):
        pass

    @property
    @abc.abstractmethod
    def dark_count_probability(self):
        pass

    @property
    @abc.abstractmethod
    def detector_efficiency(self):
        pass

    @property
    @abc.abstractmethod
    def swap_quality(self):
        pass

    @property
    @abc.abstractmethod
    def T1(self):
        pass

    @property
    @abc.abstractmethod
    def T2(self):
        pass

    @property
    @abc.abstractmethod
    def measurement_duration(self):
        pass

    @property
    @abc.abstractmethod
    def initialization_duration(self):
        pass

    @property
    @abc.abstractmethod
    def single_qubit_gate_duration(self):
        pass

    @property
    @abc.abstractmethod
    def two_qubit_gate_duration(self):
        pass

    @property
    @abc.abstractmethod
    def emission_duration(self):
        pass

    @property
    @abc.abstractmethod
    def num_communication_qubits(self):
        pass

    @property
    @abc.abstractmethod
    def num_parallel_links(self):
        pass

    @property
    @abc.abstractmethod
    def cutoff_time(self):
        pass

    @property
    @abc.abstractmethod
    def speed_of_light(self):
        pass

    @property
    @abc.abstractmethod
    def p_loss_length(self):
        pass

    @property
    @abc.abstractmethod
    def num_resolving(self):
        pass

    @property
    def output_param_file(self):
        return self._output_param_file

    def dump_to_yaml(self, output_param_file_name=None):
        if output_param_file_name is None:
            output_param_file_name = self.param_file.split(".yaml")[0] + "_abstract.yaml"
        self._output_param_file = output_param_file_name
        with open(self.output_param_file, "w") as stream:
            yaml.dump(self.to_dict(), stream)
