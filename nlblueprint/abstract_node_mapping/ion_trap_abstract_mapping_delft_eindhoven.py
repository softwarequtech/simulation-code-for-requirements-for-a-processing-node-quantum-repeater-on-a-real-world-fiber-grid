import yaml
from netsquid_netconf.netconf import Loader
from nlblueprint.abstract_node_mapping.abstract_model_delft_eindhoven import AbstractParameters


class IonTrapParametersSingleClick(AbstractParameters):
    """Performs mapping from ion trap physical parameters to abstract model parameters.

    The abstract hardware parameters (which can be obtained as properties from the object) are:
        - emission fidelity
        - visibility
        - dark count probability
        - detector efficiency
        - swap quality
        - T1
        - T2

    This ParameterSet class further includes operation durations, which can be obtained as properties from the object as
    well:
        - measurement duration
        - initialization duration
        - single qubit gate duration
        - two qubit gate duration
        - emission duration

    Furthermore, we include also topology parameters:
        - num parallel links
        - num communication qubits

    And finally parameters that are not strictly required to define the abstract model, but that should nevertheless be
    present in the parameter file in order to run blueprint simulations:
        - speed of light
        - p loss length
        - num resolving
        - p loss init

    Parameters
    ----------
    param_file_name : str
        Name of yaml parameter file holding TI physical parameters.

    The following parameters should be specified in the yaml file:

    collection_efficiency : float
        Probability that an emitted photon is collected in the fiber.
    detector_efficiency : float
        Probability that a threshold detector clicks on arrival of a photon.
    dark_count_probability : float
        Probability that a threshold detector clicks per detection time window when no photons arrive.
    rot_z_depolar_prob : float
        Depolarizing probability used to model noisy Z rotations.
    ms_depolar_prob : float
        Depolarizing probability used to model noisy MS gate.
    prob_error_0 : float
        Probability that a 0 state is measured as a 1.
    prob_error_1 : float
        Probability that a 1 state is measured as a 0.
    coherence_time : float
        Coherence time for collective dephasing of the ions in the ion trap.
    visibility : float
        Photon distinguishability.
    measurement_duration : float
        Duration of measurement operation in ns.
    initialization_duration : float
        Duration of initialization operation in ns.
    ms_pi_over_2_duration : float
        Duration of Molmer-Sorensen gate in ns.
    z_rotation_duration : float
        Duration of single-qubit Z rotation in ns.
    emission_duration : float
        Duration of emission operation in ns.
    num_communication_qubits : int or None
        Number of qubits that can be used for outward communication.
    num_parallel_links : int or None
        Number of links that can be generated in parallel.
    cutoff_time : float or None
        Time after which qubits are discarded.
    speed_of_light : float
        Speed of light in fiber. Not part of model, required to run simulation.
    p_loss_length : float
        Attenuation in fiber. Not part of model, required to run simulation.
    p_loss_init : float
        Probability of losing photon on coupling to fiber. Not part of model, required to run simulation.
    num_resolving : bool
        Whether or not detector is num_resolving. Not part of model, required to run simulation.

    Notes
    -----
    T2 is set to coherence time of collective dephasing.
    The abstract model does not capture the collective or non-Markovian nature of memory decoherence in ion traps.

    T1 is set to zero since we don't model it in the main ion trap simulations (it is subleading).

    Entanglement swap is done by 1) Z rotation 2) MS gate 3) Z measurement.
    1) and 2) are modelled as perfect operation followed by depolarizing channels.
    3) is modelled using imperfect projectors, which we can approximate using a depolarizing channel.
    For the swap quality in the abstract model, we take the depolarizing parameter which describes the composition
    of all three depolarizing channels.

    There is one more ion trap physical parameter which does not enter here: multi-qubit rotation depolarization.
    It is needed for e.g. measurements in the X basis, but plays no role for the abstract parameters here considered.

    These parameters are input into the class using a yaml parameter file, `ion_trap_param_file`. The class'
    `dump_to_yaml` method produces a yaml parameter file with the converted abstract parameters.

    This mapping is meant for usage with a single-click entanglement generation protocol. It does not take emission
    fidelity into account as that parameter does not play a role in the blueprint's single-click model.
    """

    def __init__(self, ion_trap_param_file):
        self._param_file = ion_trap_param_file
        with open(self._param_file, "r") as stream:
            sim_params = yaml.load(stream, Loader=Loader)

        self._visibility = sim_params["visibility"]
        self._dark_count_probability = sim_params["dark_count_probability"]
        self._detector_efficiency = sim_params["collection_efficiency"]
        if "detector_efficiency" in sim_params:
            self._detector_efficiency *= sim_params["detector_efficiency"]
        if "p_loss_init" in sim_params:
            self._detector_efficiency *= (1 - sim_params["p_loss_init"])
        print(sim_params)
        if "coin_prob_ph_ph" in sim_params:
            print('coin prob')
            self._detector_efficiency *= sim_params["coin_prob_ph_ph"]
        self._T2 = sim_params["coherence_time"]
        meas_depolar_prob = sim_params["prob_error_0"] + sim_params["prob_error_1"]
        self._swap_quality = (1 - sim_params["ms_depolar_prob"]) * (1 - sim_params["rot_z_depolar_prob"])\
            * (1 - meas_depolar_prob)
        self._measurement_duration = sim_params["measurement_duration"]
        self._initialization_duration = sim_params["initialization_duration"]
        self._single_qubit_gate_duration = sim_params["z_rotation_duration"]
        self._two_qubit_gate_duration = sim_params["ms_pi_over_2_duration"]
        self._emission_duration = sim_params["emission_duration"]
        self._num_communication_qubits = sim_params["num_communication_qubits"]
        self._num_parallel_links = sim_params["num_parallel_links"]
        self._cutoff_time = sim_params["cutoff_time"]
        if "speed_of_light" in sim_params:
            self._speed_of_light = sim_params["speed_of_light"]
        else:
            self._speed_of_light = 2.e+5
        if "p_loss_length" in sim_params:
            self._p_loss_length = sim_params["p_loss_length"]
        else:
            self._p_loss_length = 0.2
        self._num_resolving = sim_params["num_resolving"]

        super().__init__()

    @property
    def visibility(self):
        return float(self._visibility)

    @property
    def dark_count_probability(self):
        return float(self._dark_count_probability)

    @property
    def detector_efficiency(self):
        return float(self._detector_efficiency)

    @property
    def swap_quality(self):
        return float(self._swap_quality)

    @property
    def T1(self):
        return 0.  # we do not model t1 for ion traps

    @property
    def T2(self):
        return float(self._T2)

    @property
    def measurement_duration(self):
        return float(self._measurement_duration)

    @property
    def initialization_duration(self):
        return float(self._initialization_duration)

    @property
    def single_qubit_gate_duration(self):
        return float(self._single_qubit_gate_duration)

    @property
    def two_qubit_gate_duration(self):
        return float(self._two_qubit_gate_duration)

    @property
    def emission_duration(self):
        return float(self._emission_duration)

    @property
    def num_communication_qubits(self):
        return self._num_communication_qubits

    @property
    def num_parallel_links(self):
        return self._num_parallel_links

    @property
    def cutoff_time(self):
        return self._cutoff_time

    @property
    def param_file_name(self):
        return self._param_file_name

    @property
    def speed_of_light(self):
        return self._speed_of_light

    @property
    def p_loss_length(self):
        return self._p_loss_length

    @property
    def num_resolving(self):
        return self._num_resolving

    @property
    def param_file(self):
        return self._param_file


class IonTrapParametersDoubleClick(IonTrapParametersSingleClick):
    """Extends parent class by including emission fidelity as a parameter. Meant for usage with double-click
    entanglement generation.
    """
    def __init__(self, ion_trap_param_file):
        self._param_file = ion_trap_param_file
        with open(self._param_file, "r") as stream:
            sim_params = yaml.load(stream, Loader=Loader)
        self._emission_fidelity = sim_params["emission_fidelity"]
        super().__init__(ion_trap_param_file=ion_trap_param_file)

    @property
    def emission_fidelity(self):
        return float(self._emission_fidelity)
