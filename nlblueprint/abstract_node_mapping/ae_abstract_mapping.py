import numpy as np
from nlblueprint.abstract_node_mapping.abstract_model import AbstractParameters


class AEParameters(AbstractParameters):
    """Maps from Atomic Ensemble physical parameters to abstract parameters
    following 'Rate-loss analysis of an efficient quantum repeater architecture', Guha et al., 2015 (arxiv:1404.7183).

    The parameters in the abstract model are:

        - Elementary link fidelity
        - Elementary link success probability
        - Swap quality
        - Swap qprobability
        - T1
        - T2

        Parameters
        ----------
        midpoint_det_time_window : float
            Midpoint detector detection time window in [ns].
        midpoint_det_dark_count_rate : float
            Midpoint detector dark count rate in [Hz].
        midpoint_det_efficiency : float
            Midpoint detector detection efficiency.
        swap_det_time_window : float
            Swap detector detection time window in [ns].
        swap_det_dark_count_rate : float
            Swap detector dark count rate in [Hz].
        swap_det_efficiency : float
            Swap detector detection efficiency.
        memory_efficiency : float
            Memory efficiency.
        attenuation : float
            Fiber attenuation in [db/km]
        internode_distance : float
         Length of the elementary link in [km]
        total_num_modes : int
            Total number of modes used (product of spectral , spatial, temporal, ... modes).


        Notes
        -----

        Atomic ensemble based quantum memories claim to have no noise only loss. We therefore choose T_1 and T_2 to be
        zero.

    """
    def __init__(self, midpoint_det_time_window, midpoint_det_dark_count_rate, midpoint_det_efficiency,
                 swap_det_time_window, swap_det_dark_count_rate, swap_det_efficiency,
                 memory_efficiency,
                 attenuation, internode_distance, total_num_modes):
        self._elementary_link_fidelity, self._elementary_link_succ_prob = \
            self._compute_elementary_link(mid_det_time_window=midpoint_det_time_window,
                                          mid_det_dark_count_rate=midpoint_det_dark_count_rate,
                                          mid_det_efficiency=midpoint_det_efficiency,
                                          attenuation=attenuation,
                                          internode_distance=internode_distance,
                                          total_num_modes=total_num_modes)

        self._swap_quality, self._swap_probability = \
            self._compute_swap(swap_det_time_window=swap_det_time_window,
                               swap_det_dark_count_rate=swap_det_dark_count_rate,
                               swap_det_efficiency=swap_det_efficiency,
                               memory_efficiency=memory_efficiency)

        # Atomic ensemble memories claim they have only loss
        self._t1 = 0
        self._t2 = 0
        super().__init__()

    @staticmethod
    def _compute_elementary_link(mid_det_time_window, mid_det_dark_count_rate, mid_det_efficiency,
                                 attenuation, internode_distance, total_num_modes):

        lambda_e = np.exp(- attenuation * internode_distance / 2)
        a_1, b_1, c_1 = _compute_coeffs(det_time_window=mid_det_time_window,
                                        det_dark_count_rate=mid_det_dark_count_rate,
                                        det_efficiency=mid_det_efficiency,
                                        lambda_em=lambda_e)
        s_1 = a_1 + b_1 + 2 * c_1

        # Fidelity of |M+> to rho_1
        elementary_link_fidelity = a_1 / s_1

        elementary_link_succ_prob = 1 - (1 - 4 * s_1) ** total_num_modes

        return elementary_link_fidelity, elementary_link_succ_prob

    @staticmethod
    def _compute_swap(swap_det_time_window, swap_det_dark_count_rate, swap_det_efficiency, memory_efficiency):

        a, b, c = _compute_coeffs(det_time_window=swap_det_time_window,
                                  det_dark_count_rate=swap_det_dark_count_rate,
                                  det_efficiency=swap_det_efficiency,
                                  lambda_em=memory_efficiency)

        # error probability of single swap Q = .5 * (1 - t_r)
        w_r = c / (a + b)
        t_r = (1 - 2 * w_r) / (1 + 2 * w_r)

        swap_quality = t_r

        swap_probability = 4 * (a + b + 2 * c)

        return swap_quality, swap_probability

    @property
    def elementary_link_fidelity(self):
        return self._elementary_link_fidelity

    @property
    def elementary_link_succ_prob(self):
        return self._elementary_link_succ_prob

    @property
    def swap_quality(self):
        return self._swap_quality

    @property
    def swap_probability(self):
        return self._swap_probability

    @property
    def t1(self):
        return self._t1

    @property
    def t2(self):
        return self._t2


def _compute_coeffs(det_time_window, det_dark_count_rate, det_efficiency, lambda_em):
    """Computes noise coefficients for swap and midpoint BSM.

    Parameters
    ----------
    det_time_window : float
        Detector time window in NANOseconds.
    det_dark_count_rate : float
        Detector dark count rate in Hz.
    det_efficiency : float
        Detection efficiency of the respective detector.
    lambda_em : float
        Either memory efficiency lambda_m (for swap BSM) or transmission efficiency of the elementary link fiber
        lambda_e = exp(- alpha * L / (2 * N)) (for midpoint BSM)

    Returns
    -------
    a : float
        Probability of getting the correct single clicks in both time windows.
    b : float
        Probability of getting a single click in both time windows, but in the wrong combination, such that
        the midpoint outcome and the corresponding Bell state do not coincide.
    c : float
        Probability of getting a single click in both time windows without there being actual entanglement.
    """
    # adapt naming convention of the paper
    P = 1 - np.exp(- det_time_window * det_dark_count_rate * 1e9)
    eta = det_efficiency

    A = eta * lambda_em + P * (1 - eta * lambda_em)
    B = 1 - (1 - P) * (1 - eta * lambda_em) ** 2

    a = (1 / 8) * (P ** 2 * (1 - A) ** 2 + A ** 2 * (1 - P) ** 2)
    b = (1 / 8) * (2 * A * P * (1 - A) * (1 - P))
    c = (1 / 8) * P * (1 - P) * (P * (1 - B) + B * (1 - P))

    return a, b, c
