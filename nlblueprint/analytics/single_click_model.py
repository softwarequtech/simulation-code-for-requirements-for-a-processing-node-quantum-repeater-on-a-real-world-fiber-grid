from sympy import symbols, pprint, simplify, sin, sqrt, trace
from sympy.matrices import Matrix, eye
from sympy.physics.quantum.tensorproduct import TensorProduct
from nlblueprint.analytics.elementary_link_modeling import post_measurement_state, amplitude_dampen, m01_dc, m10_dc,\
    m20_dc, m02_dc, swap, prob_loss_a, prob_loss_b, prettify_unnormalized_state

# single-click scheme matter-photon state right after emission
alpha = symbols("alpha", real=True, positive=True)  # bright-state parameter
input_state_one_side = Matrix([sqrt(1 - alpha), 0, 0, sqrt(alpha)])  # photon entangled with matter qubit
input_state_one_side = input_state_one_side * input_state_one_side.H  # make it a density matrix

# perform amplitude damping on photons to represent fiber/detector loss and other inefficiencies
input_state_side_a = amplitude_dampen(state=input_state_one_side, damp_qubit=2, loss_probability=prob_loss_a)
input_state_side_b = amplitude_dampen(state=input_state_one_side, damp_qubit=2, loss_probability=prob_loss_b)

# full input state of the midpoint measurement is tensor product of the states of both sides
input_state = TensorProduct(input_state_side_a, input_state_side_b)

# optional: express bright-state parameter as trigonometric function, sympy can simplify this better
theta = symbols("theta", positive=True)
input_state = input_state.subs(alpha, sin(theta) ** 2)

# matrix to reorder from
# matter qubit A - photon A - matter qubit B - photon B
# which was used to define the input state above, to
# matter qubit A - matter qubit B - photon A - photon B
# which is more suitable for the POVM
reorder_matrix = TensorProduct(eye(2), swap, eye(2))
input_state = reorder_matrix * input_state * reorder_matrix.T

# embed POVM in larger space, including the matter qubits
# note: these POVM elements also include the effect of dark counts
m10_embedded = TensorProduct(eye(4), m10_dc)
m01_embedded = TensorProduct(eye(4), m01_dc)
m20_embedded = TensorProduct(eye(4), m20_dc)
m02_embedded = TensorProduct(eye(4), m02_dc)

# post-measurement states (unnormalized) corresponding to specific POVM elements,
# after tracing out the photons
# for non-photon-number-resolving detectors, one and two clicks cannot be distinguished,
# giving effective POVM elements m01 + m02 and m10 + m20
state_click_no_click_number_resolving = post_measurement_state(m10_embedded, input_state)
state_no_click_click_number_resolving = post_measurement_state(m01_embedded, input_state)
state_click_no_click_not_number_resolving = post_measurement_state(m10_embedded + m20_embedded, input_state)
state_no_click_click_not_number_resolving = post_measurement_state(m01_embedded + m02_embedded, input_state)

# trace of the states is the probability of obtaining the states, this can be used to calculate success probabilities
succ_prob_number_resolving = trace(state_click_no_click_number_resolving) + trace(state_no_click_click_number_resolving)
succ_prob_number_resolving = simplify(succ_prob_number_resolving)
succ_prob_not_number_resolving = (trace(state_click_no_click_not_number_resolving) +
                                  trace(state_no_click_click_not_number_resolving))
succ_prob_not_number_resolving = simplify(succ_prob_not_number_resolving)

# find common factors in density matrices to take out (they are not normalized anyway)
state_click_no_click_number_resolving = prettify_unnormalized_state(state_click_no_click_number_resolving)
state_no_click_click_number_resolving = prettify_unnormalized_state(state_no_click_click_number_resolving)
state_click_no_click_not_number_resolving = prettify_unnormalized_state(state_click_no_click_not_number_resolving)
state_no_click_click_not_number_resolving = prettify_unnormalized_state(state_no_click_click_not_number_resolving)

if __name__ == "__main__":
    print("\n\nSINGLE-CLICK MODEL\n")
    print("\nPHOTON-NUMBER RESOLVING\n\n")
    print("success probability: \n")
    pprint(succ_prob_number_resolving)
    print("\nstates:\n")
    pprint(state_click_no_click_number_resolving)
    pprint(state_no_click_click_number_resolving)
    print("\n\nNON-PHOTON-NUMBER RESOLVING\n\n")
    print("success probability: \n")
    pprint(succ_prob_not_number_resolving)
    print("\nstates:\n")
    pprint(state_click_no_click_not_number_resolving)
    pprint(state_no_click_click_not_number_resolving)
