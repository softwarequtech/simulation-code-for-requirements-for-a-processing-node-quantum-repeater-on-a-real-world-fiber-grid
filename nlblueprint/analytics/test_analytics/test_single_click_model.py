from nlblueprint.analytics.elementary_link_modeling import mu, prob_loss_a, prob_loss_b, prob_dc
from sympy import cos, sin, diag, Matrix, sqrt, trace, refine, Q, conjugate, zeros, simplify
import pytest
import unittest


@pytest.mark.integtest
class TestSimplifiedModel(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        """only import the model when the test is actually run;
        when it is imported, the calculation is also performed, which takes some time """
        from nlblueprint.analytics.single_click_model import theta, \
            succ_prob_number_resolving, succ_prob_not_number_resolving, \
            state_click_no_click_number_resolving, state_no_click_click_number_resolving
        cls.state_click_no_click_number_resolving = state_click_no_click_number_resolving
        cls.state_no_click_click_number_resolving = state_no_click_click_number_resolving
        cls.succ_prob_number_resolving = succ_prob_number_resolving
        cls.succ_prob_not_number_resolving = succ_prob_not_number_resolving
        cls.theta = theta

    def test_only_nonunit_visibility(self):
        """Test model obtained here against model from the literature under simplifying assumptions.

        Paper:
        Entanglement Distillation Between Solid-State Quantum Network Nodes, N. Kalb et al., 10.1126/science.aan0070

        Specifically, the model can be found in eq. 4 (bottom of page 5) in the Supplementary Material:
        http://www.sciencemag.org/content/356/6341/928/suppl/DC1
        (explanation of symbols used in table S3, page 18).

        Our simplifying assumptions are:
        - no dark counts
        - no photon loss
        - no phase picked up on fiber (phi = 0)
        - no dephasing
        - no failure probability for classical spin-photon correlations

        NOTE
        ----
        The model uses parameter V = |<psi_a|psi_b>|^2, i.e. V = |mu|^2.

        """
        p01 = sin(self.theta) ** 2 * cos(self.theta) ** 2
        p10 = p01
        expected_plus_state = diag(0, Matrix([[p01, sqrt(mu ** 2 * p01 * p10)], [sqrt(mu ** 2 * p01 * p10), p10]]), 0)
        expected_plus_state = expected_plus_state / trace(expected_plus_state)
        expected_min_state = diag(0, Matrix([[p01, -sqrt(mu ** 2 * p01 * p10)], [-sqrt(mu ** 2 * p01 * p10), p10]]), 0)
        expected_min_state = expected_min_state / trace(expected_min_state)

        # state produced by model under simplifying assumptions
        state_click_no_click = self.state_click_no_click_number_resolving \
            .subs([(conjugate(mu), mu), (prob_loss_a, 0), (prob_loss_b, 0), (prob_dc, 0)])
        state_no_click_click = self.state_no_click_click_number_resolving \
            .subs([(conjugate(mu), mu), (prob_loss_a, 0), (prob_loss_b, 0), (prob_dc, 0)])

        # normalize: the states produced by the models are not normalized
        state_click_no_click = state_click_no_click / trace(state_click_no_click)
        state_no_click_click = state_no_click_click / trace(state_no_click_click)

        assert simplify(refine(expected_plus_state - state_click_no_click, Q.positive(mu))) == zeros(4)
        assert simplify(refine(expected_min_state - state_no_click_click, Q.positive(mu))) == zeros(4)

    def test_photons_always_lost(self):
        """Assert that when photons always get lost, the success probability is zero in absence of dark counts."""
        succ_prob_number_resolving = self.succ_prob_number_resolving\
            .subs([(prob_loss_a, 1), (prob_loss_b, 1), (prob_dc, 0)])
        assert succ_prob_number_resolving == 0
        succ_prob_not_number_resolving = self.succ_prob_not_number_resolving \
            .subs([(prob_loss_a, 1), (prob_loss_b, 1), (prob_dc, 0)])
        assert succ_prob_not_number_resolving == 0

    def test_always_dark_counts(self):
        """Assert that the success probability is zero if there are always dark counts."""
        succ_prob_number_resolving = self.succ_prob_number_resolving.subs(prob_dc, 1)
        assert succ_prob_number_resolving == 0
        succ_prob_not_number_resolving = self.succ_prob_not_number_resolving.subs(prob_dc, 1)
        assert succ_prob_not_number_resolving == 0
