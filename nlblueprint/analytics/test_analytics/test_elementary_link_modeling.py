from nlblueprint.analytics.elementary_link_modeling import post_measurement_state, partial_trace_last_qubits, \
    amplitude_dampen, prettify_unnormalized_state
from sympy import Matrix, sqrt, eye, symbols, diag, trace
from sympy.physics.quantum.tensorproduct import TensorProduct

PHI_PLUS = Matrix([1 / sqrt(2), 0, 0, 1 / sqrt(2)])
PHI_PLUS = PHI_PLUS * PHI_PLUS.H
zero_ket = Matrix([1, 0])


def test_post_measurement_state():
    """Check that post-measurement after tracing out is as expected.

    Measuring third qubit of |0> (|00> + |11>) |0>, and tracing out the last two qubits,
    should leave the first two qubits in |00>.

    """
    projector_on_0 = zero_ket * zero_ket.H
    output = post_measurement_state(povm_element=TensorProduct(eye(4), projector_on_0, eye(2)),
                                    input_state=TensorProduct(projector_on_0, PHI_PLUS, projector_on_0))
    assert trace(output) == 1 / 2  # probability of measurement outcome
    output = output * 2  # normalize post-measurement state
    expected_output = TensorProduct(projector_on_0, projector_on_0)
    assert output == expected_output


def test_partial_trace_last_qubits():
    """Assert that tracing qubits has expected effect for both a product state and an entangled state."""
    # product state should be unaffected by tracing out
    product_state = TensorProduct(PHI_PLUS, zero_ket, zero_ket)
    product_state = product_state * product_state.H  # convert to density matrix
    assert partial_trace_last_qubits(product_state) == PHI_PLUS * PHI_PLUS.H

    # maximally entangled state should become maximally mixed by tracing out
    entangled_state = TensorProduct(zero_ket, PHI_PLUS, zero_ket)
    entangled_state = entangled_state * entangled_state.H
    assert partial_trace_last_qubits(entangled_state) == TensorProduct(zero_ket * zero_ket.H, eye(2) / 2)


def test_amplitude_dampen():
    """Check that amplitude damping behaves as expected.

    Input state: |111>, three photons that are present.
    Apply amplitude damping to the middle one.
    Then, the state of the middle one should be (1 - loss_probability) |1><1| + loss_probability |0><0|.
    The other two qubits should be unaffected.

    """
    one_ket = Matrix([0, 1])
    initial_state = TensorProduct(one_ket, one_ket, one_ket)  # one photon in each of three modes
    initial_state = initial_state * initial_state.H
    gamma = symbols("gamma", positive=True)
    output_state = amplitude_dampen(initial_state, damp_qubit=2, loss_probability=gamma)
    expected_state_dampened_qubit = diag(gamma, 1 - gamma)
    expected_output_state = TensorProduct(one_ket * one_ket.H, expected_state_dampened_qubit, one_ket * one_ket.H)
    assert output_state == expected_output_state


def test_prettify_unnormalized_state():
    state = diag(10, 20, 30)
    assert prettify_unnormalized_state(state) == diag(1, 2, 3)
