import numpy as np
from nlblueprint.analytics.effect_of_time_windows import decay_param_from_half_life, half_life_from_decay_param, \
    detection_in_time_window_probability, detection_probability_density_function, coincidence_probability,\
    coincidence_probability_two_dark_counts, coincidence_probability_dark_count_and_photon, visibility


def test_decay_param_from_half_life(half_life=100, time=70):
    assert np.isclose(np.exp(- decay_param_from_half_life(half_life) * time), 2 ** (- time / half_life))


def test_half_life_from_decay_param(decay_param=0.1, time=70):
    assert np.isclose(np.exp(- decay_param * time), 2 ** (- time / half_life_from_decay_param(decay_param)))


def test_detection_in_time_window_probability(emission_time_decay_param=0.01, wave_function_decay_param=0.1):
    assert detection_in_time_window_probability(emission_time_decay_param=emission_time_decay_param,
                                                wave_function_decay_param=wave_function_decay_param,
                                                t_win=None) == 1
    assert detection_in_time_window_probability(emission_time_decay_param=emission_time_decay_param,
                                                wave_function_decay_param=wave_function_decay_param,
                                                t_win=0) == 0
    det_prob = detection_in_time_window_probability(emission_time_decay_param=emission_time_decay_param,
                                                    wave_function_decay_param=wave_function_decay_param,
                                                    t_win=20)
    assert not np.isclose(det_prob, 0)
    assert not np.isclose(det_prob, 1)


def test_detection_prob(emission_time_decay_param=0.01, wave_function_decay_param=0.1, start_time=10):
    def det_prob(time, t_win, renormalize):
        return detection_probability_density_function(time=time, t_win=t_win, renormalize=renormalize,
                                                      start_time=start_time,
                                                      emission_time_decay_param=emission_time_decay_param,
                                                      wave_function_decay_param=wave_function_decay_param)
    assert det_prob(0, 100, False) == 0.
    assert det_prob(8, 14, True) == 0.

    # check that it becomes zero after start_time + t_win
    assert det_prob(51, 50, False) != 0.
    assert det_prob(61, 50, False) == 0.

    assert np.isclose(det_prob(1E100, None, False), 0.)  # should go to zero at large times
    assert det_prob(20, 30, False) < det_prob(20, 30, True)  # renormalizing should increase probability


def coin_prob_tester(coin_prob, one_click_is_dark_count, clicks_always_in_t_win, **kwargs):
    """Test a function that takes coincidence and detection time windows and returns a coincidence probability.

    It is expected that the coincidence probability becomes 1 if the coincidence window exceeds the time window.

    """
    # assert coincidence prob is zero for zero coincidence window
    assert np.isclose(coin_prob(t_coin=0, t_win=10, **kwargs), 0.)
    assert np.isclose(coin_prob(t_coin=0, t_win=None, **kwargs), 0.)
    assert 0. < coin_prob(t_coin=5, t_win=10, **kwargs) < 1.

    if clicks_always_in_t_win:
        # assert coincidence prob is (almost) 1 when coincidence window is above or close to the detection window
        # note that if clicks can happen outside of the detection time window, this may not be true
        assert np.isclose(coin_prob(t_coin=10, t_win=10, **kwargs), 1.)
        assert np.isclose(coin_prob(t_coin=10 - 1E-5, t_win=10, **kwargs), 1.)
        assert np.isclose(coin_prob(t_coin=11, t_win=10, **kwargs), 1.)

    if one_click_is_dark_count:
        # assert coincidence prob is zero for infinite time window,
        # as the probability of the dark count appearing in any finite interval becomes zero
        assert np.isclose(coin_prob(t_coin=7, t_win=None, **kwargs), 0.)


def test_coincidence_prob(emission_time_decay_param=0.01, wave_function_decay_param=0.1):
    def coin_prob(t_coin, t_win, condition_on_within_time_window):
        return coincidence_probability(t_coin=t_coin, emission_time_decay_param=emission_time_decay_param,
                                       wave_function_decay_param=wave_function_decay_param, t_win=t_win,
                                       condition_on_within_time_window=condition_on_within_time_window)

    coin_prob_tester(coin_prob=coin_prob, one_click_is_dark_count=False, clicks_always_in_t_win=True,
                     condition_on_within_time_window=True)
    coin_prob_tester(coin_prob=coin_prob, one_click_is_dark_count=False, clicks_always_in_t_win=False,
                     condition_on_within_time_window=False)

    # conditioning on both clicks being within the time window should always increase the coincidence probability
    assert coin_prob(50, 100, False) < coin_prob(50, 100, True)

    # if t_coin >= t_win without conditioning on within window, coincidence prob = prob both photons in time window
    both_photons_within_time_window_prob = detection_in_time_window_probability(
        emission_time_decay_param=emission_time_decay_param,
        wave_function_decay_param=wave_function_decay_param,
        t_win=100) ** 2
    assert np.isclose(coin_prob(100, 100, False), both_photons_within_time_window_prob)
    assert np.isclose(coin_prob(50, 100, False), coin_prob(50, 100, True) * both_photons_within_time_window_prob)


def test_coincidence_prob_two_dark_counts():
    coin_prob = coincidence_probability_two_dark_counts
    return coin_prob_tester(coin_prob, one_click_is_dark_count=True, clicks_always_in_t_win=True)


def test_coincidence_prob_dark_count_and_photon(emission_time_decay_param=0.01, wave_function_decay_param=0.1):
    def coin_prob(t_coin, t_win):
        return coincidence_probability_dark_count_and_photon(t_coin=t_coin, t_win=t_win,
                                                             emission_time_decay_param=emission_time_decay_param,
                                                             wave_function_decay_param=wave_function_decay_param)
    coin_prob_tester(coin_prob, one_click_is_dark_count=True, clicks_always_in_t_win=False)

    # if decay of photon is very fast, the photon should effectively always be in the time window
    def coin_prob_very_fast_decay(t_coin, t_win):
        return coincidence_probability_dark_count_and_photon(t_coin=t_coin, t_win=t_win,
                                                             emission_time_decay_param=10,
                                                             wave_function_decay_param=10)
    # fun = coin_prob_very_fast_decay
    coin_prob_tester(coin_prob_very_fast_decay, one_click_is_dark_count=True, clicks_always_in_t_win=True)


def test_visibility(emission_time_decay_param=0.01, wave_function_decay_param=0.1):
    def vis(t_coin, t_win):
        return visibility(t_coin=t_coin, emission_time_decay_param=emission_time_decay_param,
                          wave_function_decay_param=wave_function_decay_param, t_win=t_win)
    for t_win in [None, 10]:
        assert np.isclose(vis(t_coin=0, t_win=t_win), 1.)
        assert np.isclose(vis(t_coin=1E-5, t_win=t_win), 1.)
        assert vis(t_coin=6, t_win=t_win) < vis(t_coin=5, t_win=t_win)  # should be strictly decreasing in det window

    assert vis(t_coin=16, t_win=10) == vis(t_coin=16, t_win=t_win)  # should be constant outside det window
    assert vis(t_coin=10, t_win=15) > vis(t_coin=10, t_win=16)  # should decrease when det window increases
    # going to from finite to infinite det window is also an increase, and therefore visibility should decrease
    assert vis(t_coin=10, t_win=15) > vis(t_coin=10, t_win=None)

    assert 0. < vis(t_coin=1, t_win=10) < 1.
    assert 0. < vis(t_coin=9, t_win=10) < 1.
