from nlblueprint.analytics.elementary_link_modeling import m01_dc, m10_dc, m02_dc, m20_dc, swap, \
    post_measurement_state, prob_loss_a, prob_loss_b, amplitude_dampen, prettify_unnormalized_state
from sympy import pprint, simplify, sqrt, symbols
from sympy.matrices import Matrix, zeros, eye, diag, trace
from sympy.physics.quantum.tensorproduct import TensorProduct
import itertools

# matter qubit - photon state right after photon emission (no noise)
# state is phi+ = (|010> + |101>) / sqrt(2), where qubit order is matter qubit - photon mode 1 - photon mode 2
# (e.g. photon mode 1 is horizontal, and photon mode 2 is vertical polarization, giving |0H> + |1V>)
perfect_state = Matrix([0, 0, 1, 0, 0, 1, 0, 0]) / sqrt(2)
perfect_state = perfect_state * perfect_state.H

# we model emission of a non-perfectly entangled photon as depolarization;
# that is, the photon is either emitted in the perfectly entangled state |010> + |101>,
# or both the matter qubit and photon are in a maximally mixed state
# that is, the photon is emitted in one of the two modes uniformly at random
# such that the mode it is in is not not correlated to the state of the matter qubit
depolarized_state_matter_qubit = eye(2) / 2
depolarized_state_photons = diag(0, 1, 1, 0) / 2  # |H><H| + |V><V| in case of polarization encoding
depolarized_state = TensorProduct(depolarized_state_matter_qubit, depolarized_state_photons)

# fidelity of state p phi+ + (1 - p) I / 4 is F_em = p + (1 -p) / 4 = (3 p + 1) / 4
# thus, the depolarizing probability at emission is p = (4 F - 1) / 3
emission_fidelity_a = symbols("F_emA")
emission_fidelity_b = symbols("F_emB")
emission_depolar_prob_a = (4 * emission_fidelity_a - 1) / 3
emission_depolar_prob_b = (4 * emission_fidelity_b - 1) / 3
input_state_side_a = emission_depolar_prob_a * perfect_state + (1 - emission_depolar_prob_a) * depolarized_state
input_state_side_b = emission_depolar_prob_b * perfect_state + (1 - emission_depolar_prob_b) * depolarized_state

# perform amplitude damping on photons to represent fiber/detector loss and other inefficiencies
# note: damping must be performed on the two separate photon modes
input_state_side_a = amplitude_dampen(state=input_state_side_a, damp_qubit=2, loss_probability=prob_loss_a)
input_state_side_a = amplitude_dampen(state=input_state_side_a, damp_qubit=3, loss_probability=prob_loss_a)
input_state_side_b = amplitude_dampen(state=input_state_side_b, damp_qubit=2, loss_probability=prob_loss_b)
input_state_side_b = amplitude_dampen(state=input_state_side_b, damp_qubit=3, loss_probability=prob_loss_b)

# take tensor product to turn it into a single six-qubit state; qubit order now is
# matter A, horizontal mode A, vertical mode A, matter B, horizontal mode B, vertical mode B
input_state = TensorProduct(input_state_side_a, input_state_side_b)

# to perform POVM, which acts on two qubits in the same mode,
# it is convenient to reorder the state such that the new order is
# matter A, matter B, horizontal mode A, horizontal mode B, vertical mode A, vertical mode B
# the easiest way of changing the orders, is to interchange two "neighbouring" qubits,
# since this is realized by a swap operation tensored with identity for all other dimensions
# ("easy" in the sense that the matrix is easy to write down in sympy using only small-dimensional explicit matrices)

# first interchange: vertical mode A <-> matter B, i.e. 3d and 4th qubits
interchange_3_4 = TensorProduct(eye(2 ** 2), swap, eye(2 ** 2))
# order is now
# matter A, horizontal mode A, matter B, vertical mode A, horizontal mode B, vertical mode B
# second interchange: horizontal mode A <-> matter B, i.e. 2nd and 3rd qubits
interchange_2_3 = TensorProduct(eye(2 ** 1), swap, eye(2 ** 3))
# order is now
# matter A, matter B, horizontal mode A, vertical mode A, horizontal mode B, vertical mode B
# third and final interchange: vertical mode A <-> horizontal mode B, i.e. 4th and 5th qubits
interchange_4_5 = TensorProduct(eye(2 ** 3), swap, eye(2 ** 1))
# combining gives finally
reorder_matrix = interchange_4_5 * interchange_2_3 * interchange_3_4
input_state = reorder_matrix * input_state * reorder_matrix.T


# photon-number-resolving

# POVMs corresponding to same click and different click
# (0101, 1010 are grouped, and 0110, 1001 are grouped, because they are expected to produce the same state)
same_click_number_resolving = (TensorProduct(eye(2 ** 2), m01_dc, m01_dc) +
                               TensorProduct(eye(2 ** 2), m10_dc, m10_dc))
different_click_number_resolving = (TensorProduct(eye(2 ** 2), m01_dc, m10_dc) +
                                    TensorProduct(eye(2 ** 2), m10_dc, m01_dc))

# post-measurement states (not normalized)
state_same_click_number_resolving = post_measurement_state(same_click_number_resolving, input_state)
state_different_click_number_resolving = post_measurement_state(different_click_number_resolving, input_state)

# probabilities of same click and different click
prob_same_click_number_resolving = trace(state_same_click_number_resolving)
prob_different_click_number_resolving = trace(state_different_click_number_resolving)

# success probability
succ_prob_number_resolving = simplify(prob_same_click_number_resolving + prob_different_click_number_resolving)

# prettify post-measurement states by taking out common factors (they are not normalized anyway)
state_same_click_number_resolving = prettify_unnormalized_state(state_same_click_number_resolving)
state_different_click_number_resolving = prettify_unnormalized_state(state_different_click_number_resolving)


# non-photon-number resolving

# POVMs

# "same click" POVM elements
# (all combinations for which, in both Hong-Ou-Mandel setups (one for each photon bin),
# there is a click in the same side)
combinations_same_click_not_number_resolving = itertools.chain(itertools.product([m01_dc, m02_dc], repeat=2),
                                                               itertools.product([m10_dc, m20_dc], repeat=2))
povm_elements_same_click_not_number_resolving = [TensorProduct(eye(2 ** 2), m1, m2)
                                                 for m1, m2 in combinations_same_click_not_number_resolving]

# total POVM element corresponding to "same click"
same_click_not_number_resolving = zeros(2 ** 6)
for povm_element in povm_elements_same_click_not_number_resolving:
    same_click_not_number_resolving += povm_element

# "different click" POVM elements
combinations_different_click_not_number_resolving = itertools.chain(itertools.product([m01_dc, m02_dc],
                                                                                      [m10_dc, m20_dc]),
                                                                    itertools.product([m10_dc, m20_dc],
                                                                                      [m01_dc, m02_dc]))
povm_elements_different_click_not_number_resolving = [TensorProduct(eye(2 ** 2), m1, m2)
                                                      for m1, m2 in combinations_different_click_not_number_resolving]

# total POVM element corresponding to "different click"
different_click_not_number_resolving = zeros(2 ** 6)
for povm_element in povm_elements_different_click_not_number_resolving:
    different_click_not_number_resolving += povm_element

# post-measurement states (unnormalized)
state_same_click_not_number_resolving = post_measurement_state(same_click_not_number_resolving, input_state)
state_different_click_not_number_resolving = post_measurement_state(different_click_not_number_resolving, input_state)

# probabilities of same click and different click
prob_same_click_not_number_resolving = trace(state_same_click_not_number_resolving)
prob_different_click_not_number_resolving = trace(state_different_click_not_number_resolving)

# success probability
succ_prob_not_number_resolving = simplify(prob_same_click_not_number_resolving +
                                          prob_different_click_not_number_resolving)

# find common factors in density matrices to take out (they are not normalized anyway)
state_same_click_not_number_resolving = prettify_unnormalized_state(state_same_click_not_number_resolving)
state_different_click_not_number_resolving = prettify_unnormalized_state(state_different_click_not_number_resolving)

if __name__ == "__main__":
    print("\n\nDOUBLE-CLICK MODEL\n")
    print("\nPHOTON-NUMBER RESOLVING\n\n")
    print("success probability: \n")
    pprint(succ_prob_number_resolving)
    print("\nstates: \n")
    pprint(state_same_click_number_resolving)
    pprint(state_different_click_number_resolving)
    print("\n\nNON-PHOTON-NUMBER RESOLVING\n\n")
    print("success probability: \n")
    pprint(succ_prob_not_number_resolving)
    print("\nstates: \n")
    pprint(state_same_click_not_number_resolving)
    pprint(state_different_click_not_number_resolving)
