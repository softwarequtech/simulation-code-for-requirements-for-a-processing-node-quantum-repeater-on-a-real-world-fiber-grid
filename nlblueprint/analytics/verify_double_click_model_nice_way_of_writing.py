from nlblueprint.analytics.double_click_model import emission_fidelity_a, emission_fidelity_b, \
    succ_prob_not_number_resolving, succ_prob_number_resolving, state_same_click_not_number_resolving,\
    state_same_click_number_resolving, \
    state_different_click_not_number_resolving, state_different_click_number_resolving
from nlblueprint.analytics.elementary_link_modeling import prob_loss_a, prob_loss_b, prob_dc, mu
from sympy import Matrix, sqrt, diag, eye, zeros, trace, simplify, conjugate
"""In nlblueprint.analytics.double_click_model, the success probabilities and post-measurement states
of the double-click scheme are determined analytically from the different quantum channels and midpoint POVMs.
However, the result that is returned by sympy is very messy and hard to interpret.
It is possible to also derive expressions for the success probabilities and post-measurement states
based on more intuitive arguments.
These results are more structured and much easier to interpret.
In this file, we verify that this "intuitive" model is in fact equivalent to the "formal" model derived using sympy.
For a full explanation of the model, see the "Double-Click Model" section in the nlblueprint-docs repository.
"""

# Define parameters
V = abs(mu) ** 2
p_A = 1 - prob_loss_a
p_B = 1 - prob_loss_b

# Define different constants showing up in expressions for success probability and density matrix
# Note: NR means photon-number resolving, NNR means non-photon-number resolving

# probability that both nodes emit an entangled state instead of a maximally mixed state
q_em = (4 * emission_fidelity_a - 1) * (4 * emission_fidelity_b - 1) / 9

# probability of "true" success
p_TNR = p_A * p_B * V * (1 - prob_dc) ** 4 / 2
p_TNNR = p_A * p_B * V * (1 - prob_dc) ** 2 / 2

# probability of detecting photons in different modes,
# but only getting classical anti correlations because the photons are not indistinguishable
p_F1NR = p_A * p_B * (1 - V) * (1 - prob_dc) ** 4 / 2
p_F1NNR = p_A * p_B * (1 - V) * (1 - prob_dc) ** 2 / 2

# probability of two photons being detected in the exact same detector
# (if they are in the same mode, this is due to bunching with probability V or random with probability (1 - V) / 2)
# and a dark count making it a heralded success, leasing to classical correlation
p_F2NR = 0
p_F2NNR = p_A * p_B * (1 + V) * prob_dc * (1 - prob_dc) ** 2 / 2

# probability of only one photon arriving and a dark count making it a heralded success
p_F3NR = 2 * (p_A * (1 - p_B) + (1 - p_A) * p_B) * prob_dc * (1 - prob_dc) ** 3
p_F3NNR = 2 * (p_A * (1 - p_B) + (1 - p_A) * p_B) * prob_dc * (1 - prob_dc) ** 2

# probability of no photons arriving and dark counts making it a heralded success
p_F4 = 4 * (1 - p_A) * (1 - p_B) * prob_dc ** 2 * (1 - prob_dc) ** 2

# Defining different matrices showing up in expression for density matrix
psi_plus = Matrix([0, 1, 1, 0]) / sqrt(2)
psi_plus = psi_plus * psi_plus.H  # Bell state Psi+
psi_min = Matrix([0, 1, -1, 0]) / sqrt(2)
psi_min = psi_min * psi_min.H  # Bell state Psi-
cl_anti_cor = diag(0, 1, 1, 0) / 2  # classical anti correlation
cl_cor = diag(1, 0, 0, 1) / 2  # classical correlation
max_mixed = eye(4) / 4  # maximally mixed state

# calculate success probability and states for number resolving
succ_prob_alternative_writing_NR = p_TNR + p_F1NR + p_F2NR + p_F3NR + p_F4
state_alternative_writing_NR_plus = (q_em * (p_TNR * psi_plus +
                                             p_F1NR * cl_anti_cor +
                                             p_F2NR * cl_cor) +
                                     ((1 - q_em) * (p_TNR + p_F1NR + p_F2NR) + p_F3NR + p_F4) * max_mixed)
state_alternative_writing_NR_min = (q_em * (p_TNR * psi_min +
                                            p_F1NR * cl_anti_cor +
                                            p_F2NR * cl_cor) +
                                    ((1 - q_em) * (p_TNR + p_F1NR + p_F2NR) + p_F3NR + p_F4) * max_mixed)

# assert success probability number resolving is correct
assert simplify(succ_prob_number_resolving - succ_prob_alternative_writing_NR) == 0

# assert state for same click number resolving is correct
state_alternative_writing_NR_plus = state_alternative_writing_NR_plus / succ_prob_alternative_writing_NR
state_same_click_number_resolving = state_same_click_number_resolving / trace(state_same_click_number_resolving)
state_same_click_number_resolving = state_same_click_number_resolving.subs(conjugate(mu) * mu, abs(mu) ** 2)
assert simplify(state_same_click_number_resolving - state_alternative_writing_NR_plus) == zeros(4)

# assert state for different click number resolving is correct
state_alternative_writing_NR_min = state_alternative_writing_NR_min / succ_prob_alternative_writing_NR
state_different_click_number_resolving = (state_different_click_number_resolving /
                                          trace(state_different_click_number_resolving))
state_different_click_number_resolving = state_different_click_number_resolving.subs(conjugate(mu) * mu, abs(mu) ** 2)
assert simplify(state_different_click_number_resolving - state_alternative_writing_NR_min) == zeros(4)

# calculate success probability and states for not number resolving
succ_prob_alternative_writing_NNR = p_TNNR + p_F1NNR + p_F2NNR + p_F3NNR + p_F4
state_alternative_writing_NNR_plus = (q_em * (p_TNNR * psi_plus +
                                              p_F1NNR * cl_anti_cor +
                                              p_F2NNR * cl_cor) +
                                      ((1 - q_em) * (p_TNNR + p_F1NNR + p_F2NNR) + p_F3NNR + p_F4) * max_mixed)
state_alternative_writing_NNR_min = (q_em * (p_TNNR * psi_min +
                                             p_F1NNR * cl_anti_cor +
                                             p_F2NNR * cl_cor) +
                                     ((1 - q_em) * (p_TNNR + p_F1NNR + p_F2NNR) + p_F3NNR + p_F4) * max_mixed)

# assert success probability not number resolving is correct
assert simplify(succ_prob_not_number_resolving - succ_prob_alternative_writing_NNR) == 0

# assert state for same click not number resolving is correct
state_alternative_writing_NNR_plus = state_alternative_writing_NNR_plus / succ_prob_alternative_writing_NNR
state_same_click_not_number_resolving = (state_same_click_not_number_resolving /
                                         trace(state_same_click_not_number_resolving))
state_same_click_not_number_resolving = state_same_click_not_number_resolving.subs(mu * conjugate(mu), abs(mu) ** 2)
assert simplify(state_same_click_not_number_resolving - state_alternative_writing_NNR_plus) == zeros(4)

# assert state for different click number not resolving is correct
state_alternative_writing_NNR_min = state_alternative_writing_NNR_min / succ_prob_alternative_writing_NNR
state_different_click_not_number_resolving = (state_different_click_not_number_resolving /
                                              trace(state_different_click_not_number_resolving))
state_different_click_not_number_resolving = state_different_click_not_number_resolving.subs(mu * conjugate(mu),
                                                                                             abs(mu) ** 2)
assert simplify(state_different_click_not_number_resolving - state_alternative_writing_NNR_min) == zeros(4)

assert(simplify(trace(state_alternative_writing_NR_plus))) == 1
assert(simplify(trace(state_alternative_writing_NR_min))) == 1
assert(simplify(trace(state_alternative_writing_NNR_plus))) == 1
assert(simplify(trace(state_alternative_writing_NNR_min))) == 1

print("Alternative writing verified! :)")
