PYTHON        = python3
PIP           = pip3
SOURCEDIR     = nlblueprint
SIMDIR        = simulations
TESTS         = nlblueprint/tests
EXAMPLES      = examples
SIMULATIONS   = simulations
RUN_EXAMPLES  = $(EXAMPLES)/_test_examples.py
PIP_FLAGS     = --extra-index-url=https://${NETSQUIDPYPI_USER}:${NETSQUIDPYPI_PWD}@pypi.netsquid.org --extra-index-url=https://${NETSQUIDPYPI_USER}:${NETSQUIDPYPI_PWD}@pypiqutech.netsquid.org   
MINCOV       = 50


clean:
	@find . -name '*.pyc' -delete

lint:
	@$(PYTHON) -m flake8 $(SOURCEDIR) $(EXAMPLES) $(SIMULATIONS)

python-deps:
	@cat requirements.txt | xargs -n 1 $(PYTHON) -m pip install ${PIP_FLAGS}

tests:
	@$(PYTHON) -m pytest --cov=${SOURCEDIR} --cov=${SIMDIR} --cov-fail-under=${MINCOV} -m "not integtest" $(SOURCEDIR)

_netsquid_artifacts:
	@${PYTHON} ${SOURCEDIR}/_get_netsquid_artifacts.py

tests_full:
	@$(PYTHON) -m pytest --cov=${SOURCEDIR} --cov=${SIMDIR} --cov-fail-under=${MINCOV} $(SOURCEDIR)

open-cov-report:
	@$(PYTHON) -m pytest --cov=${SOURCEDIR} --cov=${SIMDIR} --cov-report html && xdg-open htmlcov/index.html

examples:
	@$(PYTHON) -m unittest $(RUN_EXAMPLES)

verify: clean python-deps lint tests_full examples

.PHONY: clean examples lint python-deps tests tests_full verify
